# inputify

Simple and advanced inputs creation made easy.

For the full documentation, installation instructions... check [inputify documentation page](https://squeak.eauchat.org/libs/inputify/).
