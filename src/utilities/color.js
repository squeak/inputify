var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: figure out to what those styles should apply
  ARGUMENTS: (
    inputified,
    selector <string> «
      if "&": selects the input
      if "&&": selects the input container
      if starts with "&&": find the matching selector in the input container
      else: find the matching selector in the input
    »,
  )
  RETURN: <yquerjObject>
*/
function getColorTarget (inputified, selector) {
  if (selector === "&") return inputified.$input
  else if (selector === "&&") return inputified.$inputContainer
  else if (selector.match(/^\&\&/)) return inputified.$inputContainer.find(selector.replace(/^\&\&/, ""))
  else return inputified.$input.find(selector);
};

/**
  DESCRIPTION: iterate all elements to color and do an action on them
  ARGUMENTS: (
    inputified,
    key <string>,
    callback: <function(cssProperty)>
  )
  RETURN: void
*/
function doOnColorTargets (inputified, key, callback) {
  _.each(inputified.colorsClasses[key], function (selectorsList, cssProperty) {
    _.each(selectorsList, function (selector) {
      var $el = getColorTarget(inputified, selector);
      callback($el, cssProperty);
    });
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  COLOR DEFINERS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var colorsCorrespondances = {
  base: ["base"],
  baseContrast: ["baseContrast"],
  primary: ["clickable", "selected"],
  primaryContrast: ["clickableContrast", "selectedContrast"],
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: setup which elements should receive coloring
    ARGUMENTS: (
      ?options <{
        ["base"|"baseContrast"|"primary"|"primaryContrast"|"hover"|"hoverContrast"]: <{
          [any css property]: <string[]> « list of classes »
        }>
      }> « NOTE: if initializeColor has already been ran, you can omit passing options »
    )
    RETURN: void
    NOTE: for internal usage
  */
  initializeColor: function (options) {

    var inputified = this;

    if (options) inputified.colorsClasses = options;

    var colorDefiners = ["base", "baseContrast", "primary", "primaryContrast", "hover", "hoverContrast"];

    _.each(colorDefiners, function (colorDefiner) {
      if (!_.isUndefined(inputified.options.color[colorDefiner])) inputified.setColor(colorDefiner, inputified.options.color[colorDefiner]);
    });

  },

  /**
    DESCRIPTION: set input's color
    ARGUMENTS: (
      !colorDefiner <"base"|"baseContrast"|"primary"|"primaryContrast"|"hover"|"hoverContrast">,
      !value <colorString|"">,
    )
    RETURN: void
  */
  setColor: function (colorDefiner, value) {

    var inputified = this;
    inputified.options.color[colorDefiner] = value;

    doOnColorTargets(
      inputified,
      colorDefiner,
      function ($el, cssProperty) {

        // ADD HOVER FOR CLICKABLES
        if (colorDefiner == "hover") $el.hover(
          function () { $(this).css(cssProperty, inputified.options.color.hover); },
          function () {
            $el.each(function () {
              if ($(this).hasClass("inputify-selected")) var beforeHover = inputified.options.color[(inputified.colorsClasses.selectedColor || "primary")]
              else var beforeHover = inputified.options.color[(inputified.colorsClasses.unselectedColor || "base")];
              $(this).css(cssProperty, beforeHover || "");
            });
          }
        )
        else if (colorDefiner == "hoverContrast") $el.hover(
          function () { $(this).css(cssProperty, inputified.options.color.hoverContrast); },
          function () {
            $el.each(function () {
              if ($(this).hasClass("inputify-selected")) var beforeHover = inputified.options.color[(inputified.colorsClasses.selectedColor || "primary") +"Contrast"]
              else var beforeHover = inputified.options.color[(inputified.colorsClasses.unselectedColor || "base") +"Contrast"];
              $(this).css(cssProperty, beforeHover || "");
            });
          }
        );

        // IF NOT HOVER
        else $el.css(cssProperty, value);

      }
    );

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
