var _ = require("underscore");
var $$ = require("squeak");
var tippy = require("tippy.js");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function shouldSilenceGetErrors (options, opts) {
  return opts.getIsForChanged && options.unvalidatedOnChangedSilent; // && !(opts.customSetContext == "defaultValue" || opts.customSetContext == "prepareValue");
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  RETURN VALUE AS OBJECT OR JUST VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: return the result value with or without input name
    ARGUMENTS: (
      !value <any>,
      ?objectResult <boolean>@default=inputified.options.objectResult,
    )
    RETURN: <value<any> | { name: <string>, value: <any>, }>
  */
  returnValue: function (value, objectResult) {

    // JUST GET THE VALUE, OR GET IT AS OBJECT WITH INPUT NAME
    objectResult = _.isUndefined(objectResult) ? this.options.objectResult : objectResult;

    // RETURN VALUE AND NAME
    if (objectResult) return {
      name: this.options.name,
      value: value,
    }
    // RETURN VALUE
    else return value;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VERIFY MANDATORY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  verifyMandatoryInputIsDefined: function (processedValueVerified) {
    var inputified = this;
    // remove mandatory missing styling if ok
    if (_.isUndefined(processedValueVerified)) {
      inputified.$inputContainer.addClass("mandatory_missing");
      inputified.$input.addClass("mandatory_missing");
    }
    // add mandatory missing styling
    else {
      inputified.$inputContainer.removeClass("mandatory_missing");
      inputified.$input.removeClass("mandatory_missing");
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VERIFY VALUE OUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  verifyValueOut: function (processedValue, opts) {
    var inputified = this;

    // TRY TO GET THE VALUE
    try {
      var processedValueVerified = inputified.systemValueOutVerify ? inputified.systemValueOutVerify(processedValue) : processedValue;
      processedValueVerified = inputified.options.systemValueOutVerify ? inputified.options.systemValueOutVerify.call(inputified, processedValueVerified) : processedValueVerified;
      processedValueVerified = inputified.options.valueOutVerify ? inputified.options.valueOutVerify.call(inputified, processedValueVerified) : processedValueVerified;
      // remove red color to input (in case it ever has been set), and remove tooltip if there was one
      inputified.$inputContainer.removeClass("invalid");
      inputified.$input.removeClass("invalid");
      if (inputified.tip) { inputified.tip.destroy(); delete inputified.tip; };
      // return processedValue verified (maybe modified)
      return processedValueVerified;
    }

    // IF THERE IS AN ERROR
    catch (e) {
      // raise an alert and return undefined value
      if (inputified.options.unvalidatedAlertError && !shouldSilenceGetErrors(inputified.options, opts)) {
        $$.log.detailedError(
          "inputify/utilities/value:verifyValueOut",
          "The value you inputed doesn't pass verification.",
          "inputed value:",           processedValue,
          "Returned error message:",  e,
          "inputified.options:",      inputified.options,
          "verifyValueOut options:",  opts
        );
        alert(inputified.deepKey +"\n"+ e);
      };
      // if asked, throw an error
      if (inputified.options.unvalidatedThrowError && !shouldSilenceGetErrors(inputified.options, opts)) throw e;
      // color input red to display error, and display tooltip
      inputified.$inputContainer.addClass("invalid");
      inputified.$input.addClass("invalid");
      if (inputified.tip) inputified.tip.setContent(e)
      else inputified.tip = tippy(inputified.$inputContainer[0], {
        content: e,
        placement: "bottom",
        theme: "error",
        trigger: "click",
      });
      inputified.tip.show();
      // return undefined
      return undefined;
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  VERIFY VALUE IN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  verifyValueIn: function (value) {
    var inputified = this;

    // TRY TO SET THE VALUE
    try {
      var verifiedValue = inputified.systemValueInVerify ? inputified.systemValueInVerify(value) : value;
      verifiedValue = inputified.options.systemValueInVerify ? inputified.options.systemValueInVerify.call(inputified, verifiedValue) : verifiedValue;
      verifiedValue = inputified.options.valueInVerify ? inputified.options.valueInVerify.call(inputified, verifiedValue) : verifiedValue;
      // return value verified (maybe modified)
      return verifiedValue;
    }

    // IF THERE IS AN ERROR
    catch (e) {
      // raise an alert and cancel setting the value
      if (inputified.options.unvalidatedAlertError) $$.alert.detailedError(
        "inputify/utilities/value:verifyValueIn",
        "The value you try to set doesn't pass verification.",
        "value:",                   value,
        "Returned error message:",  e,
        "Inputify options:",        inputified.options
      );
      // if asked, throw an error
      if (inputified.options.unvalidatedThrowError) throw e;
      // return undefined
      return undefined;
    };


  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROCESS VALUE IN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: process the inputed value for getting it
    ARGUMENTS: ( !processedValue <any>, )
    RETURN: value <any>
  */
  processValueOut: function (processedValue) {
    var inputified = this;
    var value = inputified.options.systemValueOutProcess ? inputified.options.systemValueOutProcess.call(inputified, processedValue) : processedValue;
    value = inputified.options.valueOutProcess ? inputified.options.valueOutProcess.call(inputified, value) : value;
    return value;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  PROCESS VALUE OUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: process the given value for setting it to input
    ARGUMENTS: ( !value <any> )
    RETURN: processedValue <any>
  */
  processValueIn: function (value) {
    var inputified = this;
    var processedValue = inputified.options.systemValueInProcess ? inputified.options.systemValueInProcess.call(inputified, value) : value;
    processedValue = inputified.options.valueInProcess ? inputified.options.valueInProcess.call(inputified, processedValue) : processedValue;
    return processedValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
