var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  show: function () { this.$inputContainer.removeClass("inputify-hidden"); },
  hide: function () { this.$inputContainer.addClass("inputify-hidden"); },

  /**
    DESCRIPTION: check display condition in options, reevaluate it and hide/show input consequently
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  recalculateDisplayCondition: function () {
    var inputified = this;
    // if input should be hidden
    if (
      !_.isUndefined(inputified.options.condition) &&
      !$$.result.call(inputified, inputified.options.condition)
    ) {
      // if input has a value, show it in any case (unless specified otherwise)
      if (!_.isUndefined(inputified.internalGet()) && !inputified.options.conditionIsImperative) inputified.show()
      // else hide it
      else inputified.hide();
    }
    // else input should be displayed
    else inputified.show();
  },

  /**
    DESCRIPTION: check lock state and set it on input consequently
    ARGUMENTS: ( ø )
    RETURN: <void>
  */
  recalculateLockState: function () {
    var inputified = this;
    if (!_.isUndefined(inputified.options.locked) && $$.result.call(inputified, inputified.options.locked)) inputified.lock()
    else inputified.unlock();
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
