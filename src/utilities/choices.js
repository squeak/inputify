var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeChoices (inputified, choices) {
  var choicesList = choices || [];
  return _.map(choicesList, inputified.makeChoice);
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/*
  TYPES:
    inputify·choiceObject: { !label: <string>, !value: <any>, ?search: <any>, },
*/
module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: make initial list of choices
    ARGUMENTS: ( !callback <function(choices<inputify·choiceObject[]>)> )
    RETURN: <void>
  */
  interpretChoices: function (callback) {
    var inputified = this;
    // make choices from function
    if (_.isFunction(inputified.options.choices)) inputified.options.choices.call(inputified, function (choices) {
      callback(makeChoices(inputified, choices));
    })
    // make choices from list
    else callback(makeChoices(inputified, inputified.options.choices));
  },

  /**
    DESCRIPTION: convert a value to a choice item
    ARGUMENTS: ( value <any> )
    RETURN: <inputify·choiceObject>
  */
  makeChoice: function (value) {
    var inputified = this;
    // make choices result object
    if (_.isObject(value) && value._isChoice) var result = value
    else var result = { _isChoice: true, value: value, };
    // make choice's label
    if (_.isUndefined(result.label)) {
      var valueToLabelFunction = $$.getValue(inputified.suggestOptions, "valueToLabel") || $$.getValue(inputified.options, "radioTickbox.valueToLabel");
      result.label = valueToLabelFunction ? valueToLabelFunction.call(inputified, value) : value;
    };
    // return choice object
    return result;
  },

  /**
    DESCRIPTION: apply a function to the value, or for an input with mulitple items, to each item
    ARGUMENTS: (
      val <any|any[]>,
      callback <function(val<any>)> « this function will be applied to val or to each entry of val »,
    )
    RETURN: <any|any[]>
  */
  doOnVal: function (val, callback) {
    var inputified = this;
    if (inputified.hasSingleItem()) return callback(val)
    else return _.map(val, callback);
  },

  /**
    DESCRIPTION: return true if this input accepts a single value (not an array of values) (e.g. radio, simple, select inputs)
    ARGUMENTS: ( ø )
    RETURN: <boolean>
  */
  hasSingleItem: function () {
    return $$.getValue(this.suggestOptions, "maxItemCount") === 1 || this.options.type == "radio";
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET/MAKE CHOICES FROM VALUES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: find, in the given list of choices, the choice matching the given value, or return undefined
    ARGUMENTS: (
      !val <any>,
      !listOfChoices <choice[]>,
    )
    RETURN: <choice|undefined>
    TYPES:
      choice: { value: <any>, ... }
  */
  findChoiceForThisVal: function (val, listOfChoices) {
    return _.find(listOfChoices, function (item) {
      return _.isEqual(item.value, val);
    });
  },

  /**
    DESCRIPTION: from a value (or list of values), get/make the according choice (or list of choices)
    ARGUMENTS: (
      ?value <any>,
      listOfChoices <choice[]>,
    )
    RETURN: <choice|choice[]|undefined>
    TYPES:
      choice: { value: <any>, ... }
  */
  findOrMakeChoicesForThisValue: function (value, listOfChoices) {
    var inputified = this;
    if (!_.isUndefined(value)) return inputified.doOnVal(value, function (val) {
      var choiceForThisVal = inputified.findChoiceForThisVal(val, listOfChoices);
      return choiceForThisVal || inputified.makeChoice(val);
    });
  },

  /**
    DESCRIPTION: from a value (or list of values), remove the one(s) that are not in the passed list of choices (and alert removal)
    ARGUMENTS: (
      !value <choice|choice[]>,
      !listOfChoices <choice[]>,
    )
    RETURN: <any|any[]> « value (or list of values) that have been accepted »
    TYPES:
      choice: { value: <any>, ... }
  */
  getAcceptedValues: function (value, listOfChoices) {
    var inputified = this;

    var acceptableValues = [];
    var unacceptableValues = [];
    if (!_.isUndefined(value)) inputified.doOnVal(value, function (val) {
      // if there's a choice add val to list of accepted values
      if (inputified.findChoiceForThisVal(val, listOfChoices)) acceptableValues.push(val)
      // if not add val to unaceptable values
      else unacceptableValues.push(val);
    });

    // alert some choices where not acceptable
    if (
      unacceptableValues.length &&
      (inputified.suggestOptions.invalidChoiceError == "alert" || inputified.suggestOptions.invalidChoiceError == "log")
    ) $$[inputified.suggestOptions.invalidChoiceError].detailedError(
      "inputify/utilities/choices:getAcceptedChoices",
      "Could not set some items for input '"+ (inputified.options.name || "unnamed") +"' because they are not an available choice.\nSee console for details.",
      "The following value(s) haven't been set:",
      unacceptableValues,
      "This is the list of available choices:",
      listOfChoices,
      "Inputified object: ",
      inputified
    );

    // return acceptable value(s)
    return inputified.hasSingleItem() ? acceptableValues[0] : acceptableValues;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
