var _ = require("underscore");
var $$ = require("squeak");

var allMethods = {};
var methodsGroups = [
  require("./general"),
  require("./makers"),
  require("./value"),
  require("./choices"),
  require("./color"),
  require("./display"),
];

_.each(methodsGroups, function (methodGroup) {
  _.each(methodGroup, function (method, methodName) {
    allMethods[methodName] = method;
  });
});

module.exports = allMethods;
