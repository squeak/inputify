var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get value from UI, verify it, process it, and return it
    ARGUMENTS: ( ?opts<{
      ?getIsForChanged: <boolean>@default=false « is get fired by changed function? »,
      ?objectResult: <boolean> « if set, will use this value instead of the default one in options »,
    }> )
    RETURN: <value<any> | { name: <string>, value: <any>, }>
  */
  get: function (opts) {
    var inputified = this;
    if (!opts) opts = {};

    // get value already processed (useful if value is stored before processing)
    if (inputified.uiGetRaw) var value = inputified.uiGetRaw()

    // get processed value and process it back to output
    else {
      var processedValue = inputified.uiGet();
      var processedValueVerified = inputified.verifyValueOut(processedValue, opts);
      var value = inputified.processValueOut(processedValueVerified, opts);
    };

    // if value is same as prepareValue, return undefined instead of the value
    if (!_.isUndefined(inputified.options.prepareValue) && _.isEqual(value, inputified.options.prepareValue)) value = undefined;

    // verify mandatory requirement is met
    if (inputified.isMandatory()) inputified.verifyMandatoryInputIsDefined(value);

    // set display undefined or normal
    if (_.isUndefined(value)) inputified.$inputContainer.addClass("not_defined")
    else inputified.$inputContainer.removeClass("not_defined");

    // return the value alone or in an object with input name
    return inputified.returnValue(value, opts.objectResult);

  },

  /**
    DESCRIPTION: same as previous inputified.get function, but with presets so in order to get the value (without name), and to not throw/alert errors
    ARGUMENTS: ( <inputified.get·options> )
    RETURN: <any>
  */
  internalGet: function (opts) {
    var inputified = this;
    var getOptions = $$.defaults({
      getIsForChanged: true,
      objectResult: false,
    }, opts);
    return inputified.get(getOptions);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: verify, process and set value to UI
    ARGUMENTS: (
      !value <any|undefined>,
      ?opts <{
        ?customSetContext <"prepareValue"|"defaultValue">,
        ?objectResult: <boolean> « if set, will use this value instead of the default one in options »,
        ... any custom option that this input's uiSet method understands?
      }>
    )
    RETURN: <value<any> | { name: <string>, value: <any>, }>
  */
  set: function (value, opts) {
    var inputified = this;

    // process value in
    var valueVerified = inputified.verifyValueIn(value);
    var processedValue = inputified.processValueIn(valueVerified);

    // make sure processedValue can be set // TODO maybe some day see if it's possible to do without this, would be a bit simpler
    try {
      if (inputified.valueSetVerify) inputified.valueSetVerify(processedValue);
    }
    catch (e) {
      // raise an alert and cancel setting the value
      if (inputified.options.unvalidatedAlertError) $$.alert.detailedError(
        "inputify/utilities/value:verifyValueIn",
        "The value you try to set doesn't pass verification, undefined set instead.",
        "value:",                   valueVerified,
        "processedValue:",          processedValue,
        "Returned error message:",  e,
        "Inputify options:",        inputified.options
      );
      // if asked, throw an error
      if (inputified.options.unvalidatedThrowError) throw e;
      valueVerified = undefined;
      processedValue = undefined;
    };

    // execute ui set callback
    var uiSetReturn = inputified.uiSet(processedValue, valueVerified, opts);

    // run set and modified callback and return value in wrapping object or not
    inputified.setCallback();
    return inputified.internalGet(opts);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHANGED AND SETCALLBACK EVENTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  changed: function () {
    var inputified = this;
    // check changes (so invalid class is set if needed)
    var val = inputified.internalGet();
    // run changed and modified function if they are set
    if (inputified.options.modified) inputified.options.modified.call(inputified, val); // NOTE: do not change, keep modified executed before changed!!! (otherwise the modified function of a subinputify will be executed after firing the changed of it's parent)
    if (inputified.options.changed) inputified.options.changed.call(inputified, val);
    // make sure input is shown if it's defined (even if it's an advanced input)
    if (!_.isUndefined(val) && !inputified.options.hideAdvancedEvenIfDefined) inputified.$inputContainer.addClass("advanced_input-hidden-force_visibility");
    // execute subscriptions
    _.each(inputified.subscriptions.modified, function (callback) { callback.call(inputified); });
    _.each(inputified.subscriptions.changed, function (callback) { callback.call(inputified); });
  },

  setCallback: function () {
    var inputified = this;
    // run setCallback and modified function if they are set
    if (inputified.options.modified) inputified.options.modified.call(inputified, inputified.internalGet()); // NOTE: do not change, keep modified executed before setCallback! (otherwise the modified function of a subinputify will be executed after firing the changed of it's parent)
    if (inputified.options.setCallback) inputified.options.setCallback.call(inputified, inputified.internalGet());
    // make sure input is shown if it's defined (even if it's an advanced input)
    if (!_.isUndefined(inputified.internalGet()) && !inputified.options.hideAdvancedEvenIfDefined) inputified.$inputContainer.removeClass("advanced_input-hidden-force_visibility");
    // execute subscriptions
    _.each(inputified.subscriptions.modified, function (callback) { callback.call(inputified); });
    _.each(inputified.subscriptions.setCallback, function (callback) { callback.call(inputified); });
  },

  // OTHER EVENTS
  removed: function () {
    var inputified = this;
    _.each(inputified.subscriptions.removed, function (callback) { callback.call(inputified); });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SUBSCRIPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: subscribe to changes on this input
    ARGUMENTS: (
      !eventName <"changed"|"setCallback"|"modified"|"removed"> « event to subscribe to »,
      !callback <function(ø){@this=inputified}> « what to execute when this event is triggered »,
    )
    RETURN: <void>
  */
  subscribe: function (eventName, callback) {
    var inputified = this;
    inputified.subscriptions[eventName].push(callback);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  OPTIONS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get options specific to an input type
    ARGUMENTS: (
      !key <string> « the key where are stored options in inputified.options object »,
      ?customProcessing <
        |function(specificOptions):specificOptions « any custom processing you want to apply to get options, this function must return the final options »,
        |object « this object will be used as default options for those specific options »
      >
      ?mandatoryOptions <object> « list of mandatory key/values to verify validity »,
    )
    RETURN: specificOptions <any>
    NOTE: for internal usage
  */
  getSpecificOptions: function (key, customProcessing, mandatoryOptions) {

    var inputified = this;
    var specificOptions = $$.clone(inputified.options[key], 99) || {};

    // CUSTOM PROCESS OPTIONS AND SET IT TO inputified.options
    if (_.isFunction(customProcessing)) inputified.options[key] = customProcessing(specificOptions)
    else if (_.isObject(customProcessing)) inputified.options[key] = $$.defaults(customProcessing, specificOptions)
    else inputified.options[key] = specificOptions;

    // VERIFY MANDATORY KEYS
    if (mandatoryOptions) $$.mandatory(specificOptions, mandatoryOptions);

    // RETURN OPTIONS
    return inputified.options[key];

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHECK IF INPUT IS MANDATORY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  isMandatory: function () {
    var inputified = this;
    return $$.result.call(inputified, inputified.options.mandatory);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LOCK/UNLOCK
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  lock: function () {
    var inputified = this;
    inputified.$inputContainer.addClass("locked");
    inputified.$input.on("keydown", function (e) {
      // only allow tab key to be used inside container element (so the user can still use keyboard to escape input)
      if (e.keyCode != 9) {
        e.returnValue = false;
        return false;
      };
    });
    if (inputified.labelTip) inputified.labelTip.setProps({ theme: "locked", });
  },

  unlock: function () {
    var inputified = this;
    inputified.$inputContainer.removeClass("locked");
    inputified.$input.off("keydown");
    if (inputified.labelTip) inputified.labelTip.setProps({ theme: "default", });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
