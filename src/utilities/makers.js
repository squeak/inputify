var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/array");
require("squeak/extension/dom");
var $ = require("yquerj");
var uify = require("uify");
var tippy = require("tippy.js");
var async = require("async");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET/SET/REMOVE/HAS IN STORAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: do something to storage or recursiveStorage
  ARGUMENTS: (
    !whichStorage <options.storage|options.storageRecursive>,
    ?deepKey <string> « if not defined, will return the whole storage »,
    ?customAction <"setValue"|"hasValue"|"removeValue"|"getValueOrResult">,
    ?otherArguments <any[]> « additional arguments that will be passed to the called method »,
  )
  RETURN: <any>
*/
function doToStorage (whichStorage, deepKey, customAction, otherArguments) {

  // asked to perform a custom action on the storage
  if (customAction) {
    if (_.indexOf(["setValue", "hasValue", "removeValue", "getValueOrResult"], customAction)) {
      var args = _.flatten([[whichStorage, deepKey], otherArguments], true)
      return $$[customAction].apply(whichStorage, args);
    }
    else $$.log.detailedError(
      "inputified.storage|inputified.storageRecursive",
      "The customAction you asked is unsupported (the value at deepKey has been returned instead).",
      "Custom action you asked: ",
      customAction
    );
  }

  // return the whole  storage
  if (_.isUndefined(deepKey)) return whichStorage
  // just get value from storage
  else return $$.getValue(whichStorage, deepKey);

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMAKE INPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    see documentation
    NOTE: a solution to the warning in the readme would be to set a type specific options as prototype of the options object, but for some reason using the prototype doesn't seem to work in some cases
  */
  remake: function (callback) {
    var inputified = this;

    // clean up existant
    inputified.$inputContainer.empty();
    inputified.$inputContainer.removeClass();

    // remake full options and reattach them
    var options = inputified.inputify.makeFullOptions(inputified.options);
    inputified.attachOptionsToInputified(options);

    // remake input
    inputified.initialize(callback);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ATTACH OPTIONS TO INPUTIFIED
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  attachOptionsToInputified: function (options) {
    var inputified = this;

    // ATTACH ALL OPTIONS AND NAME
    inputified.options = options;
    inputified.name = options.name;

    // ATTACH STORAGE AND STORAGERECURSIVE
    inputified.storage = function (deepKey, customAction) { return doToStorage(options.storage, deepKey, customAction, _.rest(arguments, 2)); };
    inputified.storageRecursive = function (deepKey, customAction) { return doToStorage(options.storageRecursive, deepKey, customAction, _.rest(arguments, 2)); };

    // INITIALIZE SUBSCRIPTIONS OBJECT
    inputified.subscriptions = { changed: [], setCallback: [], modified: [], removed: [], };

    // CREATE DEEPKEY
    var deepKeyArray = [];
    // if there is a parent and it had a deepKey add it
    if (inputified.storage("parentInputified.deepKey")) deepKeyArray.push(inputified.storage("parentInputified.deepKey"));
    if (!_.isUndefined(options.name) && !options.ignoreInDeepKeyChain) deepKeyArray.push(options.name); // entry inputs depend on ignoring an inputified in deepKey if it's name is undefined
    inputified.deepKey = deepKeyArray.join(".");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INITIALIZING FUNCTION
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  initialize: function (initializedCallback) {
    var inputified = this;

    // o            >            +            #            °            ·            ^            :            |
    //                                           ATTACH CUSTOM METHODS

    if (inputified.options.inputMethods) _.each(inputified.options.inputMethods, function (methodFunc, methodName) {
      if (!inputified[methodName] || inputified.options.allowOverridingExistingMethods) inputified[methodName] = _.bind(methodFunc, inputified);
    });

    // o            >            +            #            °            ·            ^            :            |
    //                                           MAKE DOM

    inputified.setupInputContainer();
    inputified.makeLabel();
    inputified.makeInput();
    inputified.makeHelpTip();

    // if label is hidden, allow to toggle it via ctrl+clicking
    if (inputified.options.labelLayout == "hidden") {
      function toggleLableDisplay (event) {
        if (event.ctrlKey) { // event.which === 3 // < would test if rightclicked
          inputified.$label.toggle();
          event.stopPropagation(); // prevent toggling also parents inputs labels
        };
      };
      // apply on input and label (better than applying on $inputContainer, because otherwise should deactivate the click event at every refresh of input, otherwise it's ran multiple time)
      inputified.$input.click(toggleLableDisplay);
      inputified.$label.click(toggleLableDisplay);
    };


    // o            >            +            #            °            ·            ^            :            |
    //                                           MAKE INPUT SPECIFICS FOR THIS TYPE

    // inputified.makeSyncOrAsync(async.ensureAsync(function () {
    inputified.makeSyncOrAsync(function () {

      //
      //                              IF INPUT IS MANDATORY APPLY MANDATORY STYLE BY DEFAULT (DO THIS BEFORE EVENTUALLY SETTING A DEFAULT VALUE)

      if (inputified.isMandatory()) {
        inputified.$inputContainer.addClass("mandatory_missing");
        inputified.$input.addClass("mandatory_missing");
      };

      //
      //                              SET INITIAL VALUE (OR DEFAULT ONE IF THERE'S ONE)

      // VALUE
      if (!_.isUndefined(inputified.options.value)) inputified.set($$.result.call(inputified, inputified.options.value))
      // DEFAULT VALUE
      else if (
        !_.isUndefined(inputified.options.defaultValue) &&
        (!inputified.options.defaultOnlyForNew || $$.getValue(inputified.options, "storageRecursive.isNewEntry"))
      ) inputified.set($$.result.call(inputified, inputified.options.defaultValue), { customSetContext: "defaultValue" })
      // PREPARE VALUE
      else if (!_.isUndefined(inputified.options.prepareValue)) inputified.set($$.result.call(inputified, inputified.options.prepareValue), { customSetContext: "prepareValue" });

      //
      //                              SET DISPLAY STATE

      // CHECK IF INPUT SHOULD BE LOCKED
      inputified.recalculateLockState();

      // FIGURE OUT IF INPUT SHOULD BE DISPLAYED
      inputified.recalculateDisplayCondition();

      //
      //                              EXECUTED INITIALIZED CALLBACK IF THERE IS ONE SET

      if (initializedCallback) initializedCallback();

      //                              ¬
      //

    });
    // }));

    // o            >            +            #            °            ·            ^            :            |
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CALL SYNC OR ASYNC MAKE METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeSyncOrAsync: function (callback) {
    var inputified = this;
    if (inputified.makeIsAsync) inputified.make(callback)
    else {
      inputified.make()
      callback();
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE INPUT CONTAINER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  setupInputContainer: function () {
    var inputified = this;

    inputified.$inputContainer.addClass("inputify_container inputify_container-"+ inputified.options.type +" not_defined");
    if (inputified.options.containerClass) inputified.$inputContainer.addClass(inputified.options.containerClass);
    if (inputified.options.labelLayout == "stacked" || inputified.options.labelLayout == "intertitle") inputified.$inputContainer.addClass("stacked-label");
    inputified.$inputContainer.attr("inputify-type", inputified.options.type);
    inputified.$inputContainer.attr("inputify-name", inputified.options.name);
    if (inputified.options.cssContainer) inputified.$inputContainer.css(inputified.options.cssContainer);

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE LABEL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeLabel: function () {
    var inputified = this;

    var $label = inputified.$inputContainer.label();
    $label.div({
      htmlSanitized: !_.isUndefined(inputified.options.label) ? inputified.options.label : inputified.options.name,
      class: "label-text",
    });
    if (inputified.options.labelLayout == "hidden") $label.hide()
    else if (inputified.options.labelLayout == "stacked") $label.addClass("stacked");
    else if (inputified.options.labelLayout == "intertitle") $label.addClass("intertitle");
    else $label.addClass("normal");
    if (inputified.options.cssLabel) $label.css(inputified.options.cssLabel);

    // buttons
    var allLabelBttons = $$.array.merge(inputified.options.labelAdditionalButtons, inputified.options.allLabelButtons);
    _.each(allLabelBttons, function (buttonOptions) {
      uify.button($$.defaults({
        $container: $label,
        context: inputified,
        class: "label-button",
      }, buttonOptions));
    });

    inputified.$label = $label;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE HELP TIP
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeHelpTip: function () {
    var inputified = this;

    if (inputified.options.help || inputified.isMandatory()) {
      var help = _.flatten([$$.result.call(inputified, inputified.options.help)]);
      if (inputified.isMandatory()) help.unshift("—MANDATORY—");
      if (inputified.options.helpPlacement) var tipPlacement = inputified.options.helpPlacement
      else if ($$.isTouchDevice() || $(window).width() < 700) var tipPlacement = inputified.options.labelLayout == "intertitle" ? "top" : "bottom";
      else var tipPlacement = "left";
      inputified.labelTip = tippy(
        // if touch device, just on label, else on container (because on touch device, otherwise they are showing all the time and it's annoying)
        $$.isTouchDevice() ? inputified.$label[0] : inputified.$inputContainer[0],
        {
          content: _.compact(help).join("<br>").replace(/\n/g, "<br>"),
          allowHTML: true,
          placement: tipPlacement,
          // trigger: "mouseenter focus click",
        }
      );
      // $label.attr("title", _.compact(help).join("\n"));
      inputified.$label.addClass("has_help");
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE INPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeInput: function () {
    var inputified = this;

    //
    //                              CREATE INPUT

    var $input = inputified.$inputContainer[inputified.options.tag]({ class: "inputify inputify-"+ inputified.options.type, });
    if (inputified.options.inputClass) $input.addClass(inputified.options.inputClass);
    if (inputified.options.attributes) _.each(inputified.options.attributes, function (attributeValue, attributeName) {
      $input.attr(attributeName, attributeValue);
    });

    // $input.attr("title", inputified.options.name);
    $input.attr("inputify-type", inputified.options.type);
    $input.attr("inputify-name", inputified.options.name);

    inputified.$input = $input;
    // custom css to apply to input
    if (inputified.options.cssInput) $input.css(inputified.options.cssInput);
    // monospace styling
    if (inputified.options.monospace) inputified.$input.addClass("monospace");


    //
    //                              IS ADVANCED

    if (inputified.options.isAdvanced) {
      inputified.$inputContainer.addClass("advanced_input");
      if (inputified.options.advancedInputDisplay == "hidden") inputified.$inputContainer.addClass("advanced_input-hidden")
      else if (inputified.options.advancedInputDisplay == "decoration") inputified.$inputContainer.addClass("advanced_input-decoration");
    };

    //
    //                              HAS BANNER

    var bannerText = $$.result.call(inputified, inputified.options.protectionBanner);
    if (bannerText) {
      inputified.$input.css("pointer-events", "none");
      inputified.$label.find(".label-button").css("pointer-events", "none");
      var $isAdvancedMessage = inputified.$inputContainer.div({
        class: "advanced_input-message",
        title: "double click to enable edit mode on this advanced input",
        htmlSanitized: bannerText,
      }).on("dblclick", function () {
        $$.dom.clearSelection();
        inputified.$input.css("pointer-events", "auto");
        inputified.$label.find(".label-button").css("pointer-events", "auto");
        $isAdvancedMessage.addClass("spinBounceOut");
        $input.css("opacity", "");
      });
      $input.css("opacity", 0.5);
    };

    //
    //                              FOCUS INPUT IF ASKED

    if (inputified.options.focus) $$.dom.focus(inputified.$input);

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
