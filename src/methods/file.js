var _ = require("underscore");
var $$ = require("squeak");
var async = require("async");
var uify = require("uify");
require("uify/extension/preview");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET ROOT ENTRY WHERE ATTACHMENT SHOULD BE SAVED
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getRootEntry (inputified) {
  return inputified.storageRecursive("rootInputified.objectValue");
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SAVE FILE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: save blob to inputified and preview it on the side of the input
  ARGUMENTS: (
    !inputified,
    ?blob <Blob|File|"remove"> «
      the file to save (it will be previewed if it's an image)
      to remove an existing file, pass "remove" ;)
    »,
    ?silent <boolean> « if true, no changed event fired »,
  )
  RETURN: <void>
*/
function saveFile (inputified, blob, silent) {

  // preview file
  previewBlob(inputified, blob == "remove" ? "no file" : blob);

  // save blob to inputified
  inputified.file.blob = blob;

  // remove attachment
  if (inputified.file.blob == "remove") {
    inputified.$fileTitle.val(inputified.file.title || "");
    inputified.$filePreview.empty();
    deleteAttachment(inputified, getRootEntry(inputified));
    delete inputified.file.title;
    delete inputified.fileValue;
  }
  // save attachment
  else if (inputified.file.blob) inputified.fileValue = saveAttachment(inputified, getRootEntry(inputified));

  // run changed callback
  if (!silent) inputified.changed();

};

/**
  DESCRIPTION: delete an attachment from entry or wherever
  ARGUMENTS: (
    inputified,
    entry « root entry »
  )
  RETURN: <void>
*/
function deleteAttachment (inputified, entry) {

  // custom delete function
  if (_.isFunction(inputified.options.file.attachmentDelete)) inputified.options.file.attachmentDelete.call(inputified, inputified.file, entry)

  // couch delete function
  else if (inputified.options.file.attachmentDelete == "couch") {
    if (!entry._attachments) entry._attachments = {};
    delete entry._attachments[inputified.file.attachmentId];
  };

};

/**
  DESCRIPTION: save an attachment to entry or wherever
  ARGUMENTS: (
    inputified,
    entry « root entry »
  )
  RETURN: <any> « the return of this will be returned by inputified.get method »
*/
function saveAttachment (inputified, entry) {

  // custom save function
  if (_.isFunction(inputified.options.file.attachmentSave)) inputified.options.file.attachmentSave.call(inputified, inputified.file, entry)

  // couch save function
  else if (inputified.options.file.attachmentSave == "couch") {
    if (!entry._attachments) entry._attachments = {};
    entry._attachments[inputified.file.attachmentId] = {
      content_type: inputified.file.blob.type,
      data: inputified.file.blob,
    };
    return makeFileValueFromFile(inputified);
  }

  // default, do nothing and just return a file object
  else return inputified.file; // TODO: probably this is missing additional inputs, no?

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE inputified.fileValue FROM inputified.file
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeFileValueFromFile (inputified) {

  inputified.fileValue = {
    attachmentId: inputified.file.attachmentId,
    title: inputified.file.title,
  };

  // set additional inputs values
  _.each(inputified.additionalInputifieds, function (additionalInputified) {
    var key = additionalInputified.name;
    if (inputified.file[key]) additionalInputified.set(inputified.file[key]);
  });

  return inputified.fileValue;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE HUMAN READABLE FILE SIZE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var units = ["bytes", "kB", "MB", "GB", "TB"];
function makeHumanReadableFileSize (fileSize) {
  var unitIndex = 0;
  while (unitIndex < units.length && fileSize > 600) {
    unitIndex++;
    fileSize = fileSize/1000; // thought should be divided by 1024 but gives different results than file browser
  }
  return $$.round(fileSize, 1) + units[unitIndex];
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  PREVIEW FILE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function previewFile (inputified) {

  if (_.isFunction(inputified.options.file.preview)) inputified.options.file.preview.call(inputified, getRootEntry(inputified), previewBlob)
  else if (inputified.options.file.preview == "couch") couchPreviewFromEntry.call(inputified, getRootEntry(inputified), previewBlob);

};

function previewBlob (inputified, blob) {

  // make preview
  if (inputified.options.file.preview) {

    // empty file preview container
    inputified.$filePreview.empty();

    // no file selected
    if (!blob) inputified.$filePreview.text("No file selected.")

    // blob is message
    else if (_.isString(blob)) inputified.$filePreview.text(blob)

    // display preview
    else uify.preview({
      content: blob,
      $clickableThumbnailContainer: inputified.$filePreview,
      displayOnlyIconThumbnail: true,
    });

  };

  if (inputified.options.file.showFileInfos && $$.isBlob(blob)) {

    inputified.$fileInfos.htmlSanitized(
      "<b>File selected:</b><br>"+
      "<i>type:</i> "+ blob.type +"<br>"+
      "<i>size:</i> "+ makeHumanReadableFileSize(blob.size)
    );

  };

};

function couchPreviewFromEntry (entry, previewFunc) {
  var inputified = this;

  if (entry && entry._attachments) {

    var file = entry._attachments[inputified.file.attachmentId];
    if (file) {
      // data is base64 string (was saved)
      if (_.isString(file.data)) fetch($$.url.makeBase64(file.data, file.content_type)).then(function (res) {
        return res.blob();
      }).then(function (blob) {
        previewFunc(inputified, blob);
      })
      // data is blob (not yet saved)
      else if ($$.isBlob(file.data)) previewFunc(inputified, file.data)
      else $$.log.error("[inputify] Failed to generate couch preview frome entry");
    };

  }

  else previewFunc(inputified, "there is a file, but something's wrong");

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SUBSRIBE TO ALL PARENTS SET ACTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    - ok, let's say your attachment is registered in entry.sub.obj.images and stored in entry._attachments.s0meLONgCoMpliCATEdUuID
    if entry.sub is removed, the real attachment stored in entry._attachments.s0meLONgCoMpliCATEdUuID should also be removed
    That's what this function is about!
    - the way this is done, is going up to the parents recursively, and subscribing to their "changed" event
  ARGUMENTS: ( inputified )
  RETURN: <void>
*/
function subscribeToAllParentChange (inputified) {

  var rootEntry = getRootEntry(inputified);
  recurseParentToSubscribeToRemovedEvent(inputified, function () {

    // if there is a file in the input, but nothing at this entry's key
    // then update so that file can be removed from entry
    if (!inputified || !inputified.file) return
    else saveFile(inputified, "remove", true);

  });

};

function recurseParentToSubscribeToRemovedEvent (inputifiedToSubscribeParentOf, callback) {
  var parentInputified = inputifiedToSubscribeParentOf.storage("parentInputified");
  if (parentInputified) {

    // subscribe to change in parent
    parentInputified.subscribe("removed", callback);

    // investigate parent to check it if has a parent as well
    subscribeToAllParentChange(parentInputified, callback);

  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  EXTENSIONS PRESETS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var presetExtensions = {
  image: ["jpg", "jpeg", "png", "svg", "gif"],
  video: ["mp4", "avi", "mkv", "flv", "mov", "m4v"],
  audio: ["mp3", "m4a", "ogg", "wav", "aif", "aiff", "flac", "wma", "mov"],
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  make: function () {
    var inputified = this;

    //
    //                              GET FILE-SPECIFIC OPTIONS

    var fileOptions = inputified.getSpecificOptions("file", {
      title: false,
      showFileInfos: true,
    });

    // couchAttachment shorthand
    if (fileOptions.couchAttachment) {
      fileOptions.preview = "couch"
      fileOptions.attachmentSave = "couch"
      fileOptions.attachmentDelete = "couch"
    };

    //
    //                              CREATE FILE OBJECT

    inputified.file = {
      isFile: true,
      attachmentId: $$.uuid(),
    };

    //
    //                              MAKE FILE PICKER SIDE

    var $filePicker = inputified.$input.div({
      class: "file_picker",
    });

    // make list of all allowed extensions
    var allowedExtensions = _.chain(fileOptions.fileTypes)
      .map(function (fileType) { return presetExtensions[fileType] || fileType; })
      .flatten()
      .compact()
      .uniq()
      .value()
    ;

    // MAKE FILE PICKER HIDDEN INPUT
    var $filePickerInput = $filePicker.input({
      class: "file_input",
      type: "file",
      accept: allowedExtensions ? _.map(allowedExtensions, function (ext) { return "."+ ext }).join(",") : undefined,
    }).on("input", function () {
      var input = this;
      // if filed selected, process and preview
      if (input.files && input.files[0]) processSelectedFile(input.files[0])
      // if no file selected, hide preview
      else saveFile(inputified);
    });

    // FILE PROCESSING FUNCTION
    function processSelectedFile (selectedFile) {

      // optional processing of the file
      if (_.isArray(fileOptions.process)) async.reduce(fileOptions.process, selectedFile, function (currentBlob, processFunc, next) {
        processFunc.call(inputified, currentBlob, function (modifiedBlob) { next(null, modifiedBlob); });
      }, function (err, resultBlob) {
        if (err) $$.log.detailedError(
          "inputify/file",
          "Error processing file.",
          err
        )
        else saveFile(inputified, resultBlob);
      })
      else saveFile(inputified, selectedFile);

    };

    // FILE CHOOSING BUTTONS
    inputified.$chooseFileButtons = $filePicker.div({ class: "choose_file_buttons", });

    // FILE PICKER BUTTON
    inputified.$chooseFromFileButton = inputified.$chooseFileButtons.div({
      class: "choose_file_button",
      text: "choose from file",
    }).click(function () { $filePickerInput.click(); });

    // URL PICKER BUTTON
    inputified.$chooseFromUrlButton = inputified.$chooseFileButtons.div({
      class: "choose_file_button",
      text: "fetch from url",
    }).click(function () {

      // ask url of file to import
      var imageUrl = prompt("Url of the file to import:");
      if (!imageUrl) return
      else if (!imageUrl.match(/^https?\:\/\//)) return alert("You didn't enter a valid url.");

      // make loading spinner
      var spinner = uify.spinner({
        text: "Downloading image...",
        overlay: true,
      });

      // fetch file
      try {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", imageUrl);
        xhr.responseType = "blob";
        xhr.onerror = function (err) {
          spinner.destroy();
          uify.alert.warning("Network error trying to fetch file.");
          $$.log.error("Network error trying to fetch file.", err);
        };
        xhr.onload = function () {
          spinner.destroy();
          // process fetched file and preview it if possible
          if (xhr.status === 200) processSelectedFile(xhr.response)
          else uify.alert.warning("Error fetching file:" + xhr.statusText);
        };
        xhr.send();
      }
      catch (err) {
        uify.alert.warning("Unknown error while trying to fetch file.");
        $$.log.error("Unknown error while trying to fetch file.", err.message);
      };

    });

    // FILE TITLE
    inputified.$fileTitle = $filePicker.input({
      class: "file_title_input",
      placeholder: "file title",
    }).on("input", function () {
      inputified.file.title = $(this).val();
      if (!inputified.fileValue) inputified.fileValue = {};
      inputified.fileValue.title = inputified.file.title;
      inputified.changed();
    });
    if (!fileOptions.title) inputified.$fileTitle.hide();

    // ADDITIONAL INPUTS
    if (fileOptions.additionalInputs) {
      inputified.$additionalInputs = $filePicker.div({ class: "addtional-inputs", });
      inputified.additionalInputifieds = _.map(fileOptions.additionalInputs, function (additionalInput) {
        return inputified.inputify($$.defaults(additionalInput, {
          $container: inputified.$additionalInputs,
          changed: function (val) { inputified.changed(); },
          modified: function (val) {
            inputified.file[additionalInput.key || additionalInput.name] = val;
            inputified.fileValue[additionalInput.key || additionalInput.name] = val;
          },
        }));
      });
    };

    // FILE INFOS
    inputified.$fileInfos = $filePicker.div({
      class: "file_infos",
    });
    if (!fileOptions.showFileInfos) inputified.$fileInfos.hide();

    //
    //                              IF ANY PARENT IS MODIFIED, SHOULD MAKE SURE ATTACHMENTS ARE REMOVED IF NECESSARY

    if (!inputified.options.file.doNotSubscribeToParentChangedEvent) subscribeToAllParentChange(inputified);

    //
    //                              MAKE FILE PREVIEW SIDE

    inputified.$filePreview = inputified.$input.div({ class: "file_preview", });

    //
    //                              COLOR

    inputified.initializeColor({
      base: { backgroundColor: [".file_title_input"], },
      baseContrast: { color: [".file_title_input"], },
      primary: { background: [".choose_file_button"], },
      primaryContrast: { color: [".choose_file_button"], },
      hover: { background: [".choose_file_button"], },
      hoverContrast: { color: [".choose_file_button"], },
    });

    //
    //                              RETURN INPUT

    return inputified.$input;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    var inputified = this;
    return inputified.fileValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiSet: function (processedValue) {
    var inputified = this;
    if (processedValue) {

      // make sure inputified.file is an object (in other words, ignore the given value if it doesn't seem a valid file object)
      if (!_.isObject(processedValue)) inputified.file = {
        isFile: true,
        attachmentId: $$.uuid(),
      }
      else inputified.file = processedValue;

      // set file value object
      makeFileValueFromFile(inputified);

      if (inputified.file.title) inputified.$fileTitle.val(inputified.file.title);

      previewFile(inputified);

    }
    else {
      delete inputified.file.title;
      saveFile(inputified, "remove", true);
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
