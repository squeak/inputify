var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var uify = require("uify");
var async = require("async");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE NEW INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function keyIsAlreadyUsed (inputified, key) {
  var inputifiedForThisKey = _.findWhere(inputified.structure, { name: key, });
  return inputifiedForThisKey ? true : false;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SORT SUBENTRIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function sortSubEntries (inputified) {
  var sorterInputified;

  uify.dialog({

    title: "Sort "+ inputified.name +" entries",

    content: function ($container) {
      sorterInputified = inputified.inputify({
        $container: $container,
        labelLayout: "hidden",
        type: "sortableList",
        sortableList: {
          display: $$.getValue(inputified, "options.object.sortDisplay"),
        },
        value: _.clone(inputified.objectValue),
      });
    },

    ok: function () {
      inputified.set(sorterInputified.get());
    },

  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  KEY PROMPTER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: prompt the suer to define a the key in an object
  ARGUMENTS: (
    !inputified « the object input in which to add key »,
    !callback <function(newKeyValue<string>)>,
    ?currentKeyValue <string> « if there is already a key defined »,
  )
  RETURN: <void>
*/
function keyPrompter (inputified, callback, currentKeyValue) {
  var keyPrompterInputified;

  function valueVerification (newKey) {

    // if childKey is not properly defined refuse it
    if (_.isUndefined(newKey) || _.isNull(newKey)) throw "invalid value"
    // same if this key is already used
    else if (keyIsAlreadyUsed(inputified, newKey)) throw "this key is already used"
    // if is a newKeyConstraint function execute it so it can throw some errors if this should be interrupted
    else if (inputified.options.object && _.isFunction(inputified.options.object.newKeyConstraint)) inputified.options.object.newKeyConstraint.call(inputified, newKey);

    // all good
    return newKey;

  };

  uify.dialog({

    title: "Choose/Modify a child key in "+ inputified.name +" input",

    content: function ($container) {
      // make all input options
      var inputOptions = {
        $container: $container,
        labelLayout: "hidden",
        type: inputified.objectOptionsCache.newKeyChoices ? "simple" : "normal",
        value: currentKeyValue,
      };
      if (inputified.objectOptionsCache.newKeyChoices) {
        inputOptions.suggest = { labelToValue: valueVerification, };
        inputOptions.choices = _.difference(
          $$.result.call(inputified, inputified.objectOptionsCache.newKeyChoices),
          _.keys(inputified.objectValue)
        );
      }
      else inputOptions.valueOutVerify = valueVerification;
      // create input
      keyPrompterInputified = inputified.inputify(inputOptions);
    },

    ok: function () {
      var chosenKey = keyPrompterInputified.get();
      if (chosenKey) callback(chosenKey)
      else return false;
    },

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFINE CHILD INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function defineNewChildInput (inputified, childKey, callback) {

  var subinputifyOptions = $$.defaults(inputified.objectOptionsCache.model, { name: childKey });
  inputified.addSubKey(subinputifyOptions, function (childInputified) {
    inputified.objectValue[childKey] = childInputified.get({ getIsForChanged: true, objectResult: false, });
    // run optional callback
    if (callback) callback(childInputified);
    // trigger change event on new key creation
    inputified.changed();
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RESET OBJECT VALUE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function defaultObjectValue (inputified) {
  return $$.getValue(inputified.options, "object.shape") == "array" ? [] : {};
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SET SUBINPUTS OF AN UNSTRUCTURED OBJECT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: (re)set values of subinputs
  ARGUMENTS: ( inputified, processedValue, opts )
  RETURN: <void>
*/
function resetStructuredSubInputsValues (inputified, processedValue, isPartialSet, opts) {

  // add new values to inputified.objectValue so that also values not editable by structure can be saved
  if (isPartialSet) inputified.objectValue = Object.assign(inputified.objectValue, processedValue)
  // do not reset inputified.objectValue to {} because you don't want values that are not editable with this structure to be removed (like _id, _rev or any value that is automatically set)
  else {
    inputified.objectValue = defaultObjectValue(inputified);
    // put values that don't have an input in objectValue
    if ($$.isObjectLiteral(inputified.objectValue)) _.each(processedValue, function (value, key) {
      if (!_.findWhere(inputified.structure, { name: key, })) inputified.objectValue[key] = value;
    });
  };

  // itterate list of inputs and set their value
  if (!_.isUndefined(processedValue)) _.each(inputified.structure, function (childInputified) {

    // if not partial set should redefine all inputs values and set undefined if necessary
    if (!isPartialSet || $$.hasValue(processedValue, childInputified.name)) {
      var valueForThisInput = $$.getValue(processedValue, childInputified.name);
      // sets value even if undefined (because key was defined so undefined is actually it's value)
      inputified.objectValue[childInputified.name] = childInputified.set(valueForThisInput, $$.defaults(opts, { objectResult: false }));
    };

  });

};

/**
  DESCRIPTION: create a subinput with a value/defaultValue/prepareValue and set it's value to inputified.objectValue
  ARGUMENTS: ( inputified, val, key, opts )
  RETURN: <void>
*/
function generateSubInputAndSetValue (inputified, val, key, opts) {
  var subinputifyOptions = $$.defaults(inputified.options.object.model, { name: key, });
  subinputifyOptions[opts.customSetContext || "value"] = val;
  inputified.addSubKey(subinputifyOptions, function (childInputified) {
    inputified.objectValue[key] = childInputified.get({ objectResult: false, });
  });
};

/**
  DESCRIPTION: find subinputs in structure and set them, create the missing ones
  ARGUMENTS: ( inputified, processedValue, opts )
  RETURN: <void>
*/
function addOrChangeUnstructuredSubInputs (inputified, processedValue, opts) {
  _.each(processedValue, function (val, key) {
    var childInputified = _.findWhere(inputified.structure, { name: key, });
    if (childInputified) inputified.objectValue[key] = childInputified.set(val, opts).value
    else childInputified = generateSubInputAndSetValue(inputified, val, key, opts);
  });
};

/**
  DESCRIPTION: recreate all subinputs from scratch
  ARGUMENTS: ( inputified, processedValue, opts )
  RETURN: <void>
*/
function resetAllUnstructuredSubInputs (inputified, processedValue, opts) {

  inputified.$childrenInputs.empty();
  inputified.structure = [];
  inputified.objectValue = defaultObjectValue(inputified);

  // generate sub inputs
  _.each(processedValue, function (val, key) {
    generateSubInputAndSetValue(inputified, val, key, opts);
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeIsAsync: true,
  make: function (makeCallback) {

    var inputified = this;
    var spinner = uify.spinner({
      $container: inputified.$inputContainer,
      size: 12,
      overlay: true,
      delay: 400,
    });

    var objectOptions = inputified.getSpecificOptions("object", function (specificOptions) {
      if (specificOptions.shape != "object" && specificOptions.shape != "array") specificOptions.shape = "object";
      if (!specificOptions.model && !specificOptions.structure) specificOptions.model = { type: "normal", };
      // if (_.isUndefined(specificOptions.allowKeysReordering) && specificOptions.model) specificOptions.allowKeysReordering = true;
      return specificOptions;
    });
    inputified.objectOptionsCache = objectOptions;

    // for some inputs, some children's model options can be passed directly in the parent
    if (_.isArray(objectOptions.passParentOptionsToChildrenModel)) {
      if (!objectOptions.model) objectOptions.model = {};
      _.each(objectOptions.passParentOptionsToChildrenModel, function (optionName) {
        if (inputified.options[optionName]) objectOptions.model[optionName] = $$.defaults(objectOptions.model[optionName], inputified.options[optionName]);
      });
    };

    // ADD inputify-objects CLASS
    inputified.$input.addClass("inputify-objects");

    // PREPARE OBJECT VALUE AND STRUCTURE
    var isRemake = false;
    if (!inputified.objectValue) inputified.objectValue = defaultObjectValue(inputified)
    else isRemake = true
    inputified.structure = [];
    inputified.$childrenInputs = inputified.$input.div({ class: "children_inputs", });

    //
    //                              INITIALIZE LIST OF CHILDREN SUBSCRIPTIONS (necessary for structure initializing)

    inputified.childEventsSubscriptions = {};

    //
    //                              FIGURE OUT IF IT'S A STRUCTURED OBJECT OR NOT

    inputified.hasDefinedStructure = objectOptions.structure ? true : false;

    //
    //                              ALLOW TO CREATE OTHER KEY/VALUES

    if (!_.isUndefined(objectOptions.allowNewKeyCreation)) inputified.allowNewKeyCreation = objectOptions.allowNewKeyCreation
    else inputified.allowNewKeyCreation = !inputified.hasDefinedStructure;

    // create add key/value button
    if (inputified.allowNewKeyCreation) {
      inputified.$input.addClass("inputify-allow_new_key_creation");
      uify.fab({
        $container: inputified.$input,
        size: 30,
        icomoon: "plus",
        title: "add new key/value",
        position: {
          bottom: "stick",
          right: "default",
        },
        click: function () {
          // if array, just +1
          if (objectOptions.shape === "array") defineNewChildInput(inputified, inputified.structure.length)
          // if object, prompt to user
          else keyPrompter(inputified, function (childKey) { defineNewChildInput(inputified, childKey) });
        },
      });
    };

    if (objectOptions.shape === "array" && !inputified.hasDefinedStructure) uify.fab({
      $container: inputified.$input,
      size: 30,
      icomoon: "shuffle", // list-ol
      title: "reorder items",
      position: {
        bottom: "stick",
        left: "default",
      },
      click: function () { sortSubEntries(inputified); },
    });

    //
    //                              DISPLAY CHILDREN INPUTS (AND CALLBACK MAKE HAS FINISHED)

    // make an iterator function that will callback automatically after a timeout if it's not done as it should
    function iterator (subinputifyOptions, index, next) {
      async.timeout(function (callback) {
        if (objectOptions.shape === "array") subinputifyOptions.name = index;
        inputified.addSubKey(subinputifyOptions, function () {
          if (callback) callback();
        });
      }, 5000, subinputifyOptions)(next);
    };

    // build children inputs from structure and then callback
    if (inputified.hasDefinedStructure && objectOptions.structure.length) async.eachOf(objectOptions.structure, iterator, function (err) {
      if (err) {
        if (!err.info) $$.log.error(err)
        else $$.log.detailedError(
          "[INPUTIFIED] object input",
          "Creation of subkey timed out. Maybe some choices options miss a mandatory callback somewhere.",
          "options of child that failed to callback: ", err.info,
          "inputified: ", inputified,
        );
      };
      done();
    })
    // or callback directly
    else done();

    // full callback
    function done () {
      // IN CASE OF inputified.remake, REFETCH PREVIOUS VALUE
      if (isRemake) inputified.set(inputified.objectValue);
      // REMOVE SPINNER IF THERE WAS ONE
      spinner.destroy();
      // CALLBACK MAKE HAS FINISHED
      makeCallback();
    };

    //
    //                              SUPPORT CUSTOM COLORS

    inputified.initializeColor({
      primary: { backgroundColor: ["&& > .intertitle", "&& > .inputify > .uify-fab"], },
      primaryContrast: { color: ["&& > .intertitle", "&& > .inputify > .uify-fab"], },
    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    var inputified = this;
    return _.isEqual(inputified.objectValue, defaultObjectValue(inputified)) ? undefined : inputified.objectValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiSet: function (processedValue, verifiedValue, opts) {
    var inputified = this;
    if (!opts) opts = {};

    // partial set
    if (opts.isPartialSet || opts.customSetContext == "prepareValue" || opts.customSetContext == "defaultValue") var isPartialSet = true;

    // if there is a structure should not recreate list of inputs, just set their values
    if (inputified.hasDefinedStructure) resetStructuredSubInputsValues(inputified, processedValue, isPartialSet, opts)

    // if there is not a structure, handle list of inputs from the given value
    else {
      if (isPartialSet) addOrChangeUnstructuredSubInputs(inputified, processedValue, opts)
      else resetAllUnstructuredSubInputs(inputified, processedValue, opts);
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  NEW KEY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: add a key in this object-like input
    ARGUMENTS: (
      ?subinputifyOptions <inputify·options> « options of this new child input »,
      ?done <function(<inputified> « the inputified of the created child input »):<void>> « executed when subkey input has been created »,
    )
    RETURN: <void>
  */
  addSubKey: function (subinputifyOptions, done) {
    var inputified = this;

    if (!subinputifyOptions) subinputifyOptions = {};

    // MAKE RECURSIVE STORAGE FOR OPTIONS
    var recursiveStorage = $$.defaults(inputified.options.storageRecursive, subinputifyOptions.storageRecursive) || {};
    if (!recursiveStorage.rootInputified) recursiveStorage.rootInputified = inputified;
    // FIGURE OUT PARENT INPUTIFIED
    if (inputified.options.ignoreInDeepKeyChain) var parentInputified = inputified.storage ? inputified.storage("parentInputified") : undefined
    else var parentInputified = inputified;
    // MANDATORILY PASSED CHILD OPTIONS
    var valToPassToEvent;
    var mandatoryChildOptions = {
      $container: inputified.$childrenInputs,
      storage: $$.defaults(subinputifyOptions.storage, { parentInputified: parentInputified, }),
      storageRecursive: recursiveStorage,
      labelButtons: [
        {
          icomoon: "pencil2",
          title: "rename this key",
          condition: function () { return inputified.allowNewKeyCreation && $$.getValue(inputified, "options.object.shape") !== "array"; },
          click: function (ev) {

            var childInputName = $($(ev.target).parents(".inputify_container")[0]).attr("inputify-name");
            var childInputified = _.findWhere(inputified.structure, { name: childInputName, });

            keyPrompter(inputified, function (newChildKey) {

              // store child input value before removing the input
              var childInputValue = childInputified.get();
              // remove child input
              inputified.removeSubKey(childInputName);
              // readd child input with new key
              defineNewChildInput(inputified, newChildKey, function (newChildInputified) {
                newChildInputified.set(childInputValue);
              });

            }, childInputName);

          },
        },
        {
          icomoon: "trash",
          title: "remove key",
          condition: function () { return inputified.allowNewKeyCreation; },
          click: function (ev) {
            if (confirm("remove this key")) {
              var $parentInputifiedContainer = $($(ev.target).parents(".inputify_container")[0]);
              inputified.removeSubKey($parentInputifiedContainer.attr("inputify-name"));
            };
          },
        },
      ],
      setCallback: function () {
        var childInputified = this; // important because may be executed before the inputify() below has returned
        if (subinputifyOptions.setCallback) subinputifyOptions.setCallback.call(childInputified, valToPassToEvent);
        inputified.setCallback();
        inputified.childEventFired("setCallback", childInputified);
      },
      changed: function () {
        var childInputified = this; // important because may be executed before the inputify() below has returned
        if (subinputifyOptions.changed) subinputifyOptions.changed.call(childInputified, valToPassToEvent);
        inputified.changed();
        inputified.childEventFired("changed", childInputified);
      },
      modified: function (value) {
        var childInputified = this; // important because may be executed before the inputify() below has returned

        // ATTACH NEW VALUE TO inputified.objectValue OR REMOVE KEY IF NEW VALUE IS undefined
        if (childInputified.options.object && childInputified.options.object.keepUndefinedEntries) var val = value
        else var val = $$.isObjectLiteral(value) ? value : $$.compact(value, [undefined]); // compared to the next line, this allow to remember the inputed key while opening/closing/opening modals, but it will still not keep undefined values on save, so it's ok // but maybe still the next line would be better, because more consistent
        // var val = $$.compact(value, [undefined]);
        if (_.isUndefined(val)) delete inputified.objectValue[childInputified.name]
        else inputified.objectValue[childInputified.name] = val;

        // SAVE VALUE FOR USAGE IN changed/setCallback (THIS IS OK BECAUSE modified IS EXECUTED BEFORE changed/setCallback)
        valToPassToEvent = subinputifyOptions.objectResult ? { name: childInputified.name, value: val, } : val;

        // EXECUTE modified CALLBACK IN CHILD AND PARENT
        if (subinputifyOptions.modified) subinputifyOptions.modified.call(childInputified, valToPassToEvent);
        if (inputified.options.modified) inputified.options.modified.call(inputified, inputified.internalGet());

        // FIRE CHILD EVENT IN PARENT
        inputified.childEventFired("modified", childInputified);

      },
      inputMethods: inputified.makeChildInputMethods(),
    };

    // MAKE SURE THAT CHILD INPUT WILL RUN CALLBACK WHEN IT'S BEEN CREATED
    var previousCreatedMethod = subinputifyOptions.created;
    mandatoryChildOptions.created = function () {
      if (previousCreatedMethod) previousCreatedMethod.apply(this, arguments);

      var childInputified = this;
      inputified.structure.push(childInputified);

      // STORE LIST OF SIBLING SUBSCRIPTIONS IN PARENT
      var siblingSubscriptions = $$.result.call(childInputified, childInputified.options.siblingsSubscriptions);
      if (siblingSubscriptions) {
        _.each(siblingSubscriptions, function (subscriptionObject) {
          if (!inputified.childEventsSubscriptions[subscriptionObject.key]) inputified.childEventsSubscriptions[subscriptionObject.key] = [];
          inputified.childEventsSubscriptions[subscriptionObject.key].push({
            subscriber: childInputified,
            event: subscriptionObject.event,
            action: subscriptionObject.action,
          });
        });
      };

      // CALLBACK CHILD INPUTIFIED
      if (done) done(childInputified);

    };

    // CREATE CHILD INPUT
    var childInputified = inputified.inputify($$.defaults(subinputifyOptions, mandatoryChildOptions));

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  ATTACH METHODS TO CHILD INPUT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeChildInputMethods: function () {
    var parentInputified = this;

    return {
      // o            >            +            #            °            ·            ^            :            |
      //                                           GET SIBLING

      /**
        DESCRIPTION: get a sibling input
        ARGUMENTS: ( !siblingName <string|number>, )
        RETURN: <inputified|undefined>
      */
      getSibling: function (siblingName) {
        return parentInputified.getSubInputified(siblingName);
      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           REMAKE SIBLINGS

      /**
        DESCRIPTION: execute an action on some or all sibling inputs
        ARGUMENTS: (
          !actionName <string> « the name of the action to execute on siblings (should be a valid inputified method name) »,
          ?nameOfSiblings <string|string[]> «
            list of names/keys of the siblings to execute the action on
            if not defined will do it on all (except this one ;)
          »,
        )
        RETURN: <void>
      */
      doOnSiblings: function (actionName, inputsKeys) {
        var childInputified = this;

        // if no keys given, get all siblings keys
        if (!inputsKeys) inputsKeys = _.chain(parentInputified.structure).pluck("name").without(childInputified.name).value();

        // if only one sibling given, make it a list
        if (!_.isArray(inputsKeys)) inputsKeys = [inputsKeys];

        // do it for each passed inputKey
        _.each(inputsKeys, function (inputKey) {

          // get sibling inputified
          var siblingInputified = childInputified.getSibling(inputKey);
          // execute action or log error
          if (siblingInputified) {
            if (siblingInputified[actionName]) siblingInputified[actionName]()
            else $$.log.error("'"+ inputKey +"' sibling inputified doesn't have a method named: "+ actionName);
          }
          else $$.log.error("Cannot find sibling inputified named: "+ inputKey, childInputified);

        });

      },

      // o            >            +            #            °            ·            ^            :            |
      //                                           SIBLING VALUE

      /**
        DESCRIPTION: get the value of a sibling input
        ARGUMENTS: (
          ?siblingKey <string>
          ?logErrorIfSiblingNotFound <boolean>@default=false « if true, will log an error if sibling not found »,
        )
        RETURN: <any> « if can't find sibling, will simply return nothing, same as if sibling has no value »
      */
      getSiblingValue: function (siblingKey, logErrorIfSiblingNotFound) {
        var childInputified = this;

        var siblingInputified = childInputified.getSibling(siblingKey);
        if (siblingInputified) return siblingInputified.get()
        else if (logErrorIfSiblingNotFound) $$.log.error("Can't find sibling: "+ siblingKey, childInputified);

      },

      // o            >            +            #            °            ·            ^            :            |
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CHILD EVENT FIRING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: notify this input that one of child fired an event
    ARGUMENTS: (
      !eventName <inputify·subscribableChildEvent>,
      !childName <string>,
    )
    RETURN: <void>
  */
  childEventFired: function (eventName, childInputified) {
    var inputified = this;
    if (inputified.childEventsSubscriptions[childInputified.name]) _.chain(inputified.childEventsSubscriptions[childInputified.name]).where({ event: eventName, }).each(function (eventSubscription) {
      if (_.isFunction(eventSubscription.action)) eventSubscription.action.call(eventSubscription.subscriber, childInputified)
      else if (_.isFunction(eventSubscription.subscriber[eventSubscription.action])) eventSubscription.subscriber[eventSubscription.action].call(eventSubscription.subscriber)
      else $$.log.detailedError(
        "inputify/childEventFired",
        "Event subscription action is not valid, it should be a function or a valid inputified key.",
        "Invalid action:",
        eventSubscription.action
      );
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOVE KEY
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: remove a key in this object-like input
    ARGUMENTS: ( !key <string|number> « the key to remove », )
    RETURN: <void>
  */
  removeSubKey: function (key) {
    var inputified = this;

    inputified.structure = _.reject(inputified.structure, function (childInputified) {
      if (childInputified.name == key) {
        // remove value in object
        if (_.isArray(inputified.objectValue)) inputified.objectValue.splice(key, 1)
        else delete inputified.objectValue[key];
        // remove display of input
        childInputified.$inputContainer.remove();
        // fire "removed" event on child input
        childInputified.removed();
        // remove input from list of children inputified
        return true;
      };
    });

    // for array like inputs, removing a value before will not pop the entry, but remove it's index
    // the following assures that the arrays is recreated
    if (_.isArray(inputified.objectValue)) inputified.set(inputified.internalGet({ objectResult: false, }));

    inputified.changed();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET SUB INPUTIFIED
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  getSubInputified: function (keyOrKeyArray) {
    var inputified = this;
    if (_.isArray(keyOrKeyArray)) return _.map(keyOrKeyArray, function (key) {
      return _.findWhere(inputified.structure, { name: key, });
    })
    else return _.findWhere(inputified.structure, { name: keyOrKeyArray, });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
