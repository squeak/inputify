var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
require("squeak/plugin/squeakDate");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: convert date string to date object
  ARGUMENTS: (
    !dateString <string>,
    ?inputified « if defined, will set the precision and rangePicker properties of inputified.dateOptions in accordance with the passed date string »,
    ?fireOpts <{
      ?noChanged: <boolean> « if true, will prevent precisionPick and rangeSwitch events from firing a changed event »,
      ?noSetInput: <boolean> « if true, will prevent precisionPick and rangeSwitch events from modifying input in DOM »,
      ?noSetValue: <boolean> « if true, will not recreate value to inputified.value »,
    }>,
  )
  RETURN: <{
    start: <momentObject>,
    end: <momentObject>,
  }>
*/
module.exports = function (dateString, inputified, fireOpts) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // no date string
  if (!dateString) return { start: $$.date.moment(), end: $$.date.moment(), };

  // get squeak date object
  var squeakDate = $$.squeakDate(dateString);

  // figure out and save precision of the date
  if (inputified) {
    var sqd = squeakDate.isSqueakDateRange ? squeakDate.start : squeakDate;
    if (sqd.hasTime === true) var newPrecision = "time"
    else if (sqd.day) var newPrecision = "date"
    else if (sqd.month) var newPrecision = "month"
    else if (sqd.year) var newPrecision = "year"
    else var newPrecision = "date";
    // change picker to new precition (but only if precision choice is enabled)
    if (
      !inputified.dateOptions.precisionChoiceDisable
      && newPrecision !== inputified.dateOptions.precision
    ) inputified.fireDatePickerEvent("precisionPick", newPrecision, fireOpts);
  };

  // is range
  if (squeakDate.isSqueakDateRange) {
    // change picker to range type (but only if range choice is enabled)
    if (
      inputified
      && inputified.dateOptions
      && !inputified.dateOptions.rangeChoiceDisable
      && !inputified.dateOptions.rangePicker
    ) inputified.fireDatePickerEvent("rangeSwitch", true, fireOpts);
    return {
      start: squeakDate.start.getMoment(),
      end: squeakDate.end.getMoment(),
      isValid: squeakDate.start.getMoment()._isValid && squeakDate.end.getMoment()._isValid,
    };
  }
  // is single date
  else {
    // change picker to simple type (but only if range choice is enabled)
    if (
      inputified
      && inputified.dateOptions
      && !inputified.dateOptions.rangeChoiceDisable
      && inputified.dateOptions.rangePicker
    ) inputified.fireDatePickerEvent("rangeSwitch", false, fireOpts);
    return {
      start: squeakDate.getMoment(),
      end: squeakDate.getMoment(),
      isValid: squeakDate.getMoment()._isValid,
    };
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
