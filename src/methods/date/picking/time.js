var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate time inputs
  ARGUMENTS: (
    $container <yquerjObject>,
    inputified,
  )
  RETURN: <void>
*/
module.exports = function ($container, inputified) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // START
  var startTimeInputified = inputify({
    $container: $container,
    name: "start",
    type: "time",
    labelLayout: "stacked",
    containerClass: "panel-start",
    color: inputified.options.color,
    value: inputified.dateValueObject.start.format("HH:mm"),
    changed: function (newTimeValue) {
      var newTimeValueArray = (newTimeValue || "").split(":");
      inputified.dateValueObject.start.hours(newTimeValueArray[0] || "00");
      inputified.dateValueObject.start.minutes(newTimeValueArray[1] || "00");
      inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject);
    },
  });
  // make sure lable of time input has proper class (to style it and hide it when necessary)
  startTimeInputified.$label.addClass("startend_title");

  // END
var endTimeInputified = inputify({
    $container: $container,
    name: "end",
    type: "time",
    labelLayout: "stacked",
    containerClass: "panel-end",
    color: inputified.options.color,
    value: inputified.dateValueObject.end.format("HH:mm"),
    changed: function (newTimeValue) {
      var newTimeValueArray = (newTimeValue || "").split(":");
      inputified.dateValueObject.end.hours(newTimeValueArray[0] || "00");
      inputified.dateValueObject.end.minutes(newTimeValueArray[1] || "00");
      inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject);
    },
  });
  // make sure lable of time input has proper class (to style it and hide it when necessary)
  endTimeInputified.$label.addClass("startend_title");

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REACT TO EVENTS IN OTHER UI PARTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  inputified.onDatePickerEvent("dateChange", function (dateValueObject) {

    // start hours or minutes have changed
    if (startTimeInputified.get() !== dateValueObject.start.format("HH:mm")) startTimeInputified.set(dateValueObject.start.format("HH:mm"));
    // end hours or minutes have changed
    if (endTimeInputified.get() !== dateValueObject.end.format("HH:mm")) endTimeInputified.set(dateValueObject.end.format("HH:mm"));

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
