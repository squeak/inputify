var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
require("uify/extension/calendar");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate date (calendar) inputs
  ARGUMENTS: (
    $container <yquerjObject>,
    inputified,
  )
  RETURN: <void>
*/
module.exports = function ($container, inputified) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var uifyCalendar = uify.calendar({
    $container: $container,
    clickSelect: inputified.dateOptions.rangePicker ? "range" : "date",
    selected: inputified.dateOptions.rangePicker ? [ inputified.dateValueObject.start, inputified.dateValueObject.end ] : inputified.dateValueObject.start,
    separateYearAndMonthInHeader: true,
    onDateChoose: function (chosenDateOrRange) {

      // SINGLE DATE
      if (_.isString(chosenDateOrRange)) {
        var dateArray = chosenDateOrRange.split("-");
        inputified.dateValueObject.start.set({ year: dateArray[0], month: dateArray[1] - 1, date: dateArray[2], });
      }
      // DATE RANGE
      else {
        var startDateArray = chosenDateOrRange.start.split("-");
        inputified.dateValueObject.start.set({ year: startDateArray[0], month: startDateArray[1] - 1, date: startDateArray[2], });
        var endDateArray = chosenDateOrRange.end.split("-");
        inputified.dateValueObject.end.set({ year: endDateArray[0], month: endDateArray[1] - 1, date: endDateArray[2], });
      };

      // FIRE DATE CHANGE EVENT
      inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject);

    },
    onRefresh: function () {
      var uifyCalendar = this; // necessary because uifyCalendar global object hasn't been created yet
      if (inputified.options.color) {
        if (inputified.options.color.baseContrast) uifyCalendar.$el.find(".nextprev").css("color", inputified.options.color.baseContrast);
        if (inputified.options.color.primary) uifyCalendar.$el.find(".selected").css("background-color", inputified.options.color.primary);
        if (inputified.options.color.primaryContrast) uifyCalendar.$el.find(".selected").css("color", inputified.options.color.primaryContrast);
      };
    },
  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REACT TO EVENTS IN OTHER UI PARTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // SWITCHED BETWEEN RANGE AND DATE
  inputified.onDatePickerEvent("rangeSwitch", function (newRangePickerValue) {
    uifyCalendar.options.clickSelect = newRangePickerValue ? "range" : "date";
    if (!newRangePickerValue) uifyCalendar.selectedRange[1] = uifyCalendar.selectedRange[0].clone().hours(23).minutes(59).seconds(59)
    else uifyCalendar.selectedRange[1] = inputified.dateValueObject.end.clone().hours(23).minutes(59).seconds(59);
    uifyCalendar.refresh();
  });

  // DATE HAS CHANGED
  inputified.onDatePickerEvent("dateChange", function (dateValueObject) {

    // RANGE CASE
    if (inputified.dateOptions.rangePicker) {
      // refresh ui if any date has been modified in range
      if (
        !uifyCalendar.selectedRange ||
        dateValueObject.start.format("YYYY-MM-DD") !== uifyCalendar.selectedRange[0].format("YYYY-MM-DD") ||
        dateValueObject.end.format("YYYY-MM-DD") !== uifyCalendar.selectedRange[1].format("YYYY-MM-DD")
      ) uifyCalendar.setSelected([dateValueObject.start, dateValueObject.end]);
      // switch displayed month if necessary
      if (
        dateValueObject.start.format("YYYY-MM") !== uifyCalendar.momentYearMonth.format("YYYY-MM") &&
        dateValueObject.end.format("YYYY-MM") !== uifyCalendar.momentYearMonth.format("YYYY-MM")
      ) uifyCalendar.change(dateValueObject.start.format("YYYY-MM"));
    }

    // SINGLE DATE CASE
    else {
      // refresh ui if date has been modified
      if (
        !uifyCalendar.selectedRange ||
        dateValueObject.start.format("YYYY-MM-DD") !== uifyCalendar.selectedRange[0].format("YYYY-MM-DD")
      ) uifyCalendar.setSelected(dateValueObject.start);
      // switch displayed month if necessary
      if (dateValueObject.start.format("YYYY-MM") !== uifyCalendar.momentYearMonth.format("YYYY-MM")) uifyCalendar.change(dateValueObject.start.format("YYYY-MM"));
    };

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
