var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  YEAR INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function yearInput ($container, inputified, startOrEnd) {

  return inputify({
    $container: $container,
    type: "integer",
    name: "year",
    labelLayout: "hidden",
    color: inputified.options.color,
    number: {
      orientation: "vertical",
      // NOTE: here could implement min/max values
    },
    value: inputified.dateValueObject[startOrEnd].year(),
    changed: function (value) {
      inputified.dateValueObject[startOrEnd].year(value);
      inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject);
    },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MONTH INPUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function monthInput ($container, inputified, startOrEnd) {

  return inputify({
    $container: $container,
    type: "integer",
    name: "month",
    labelLayout: "hidden",
    color: inputified.options.color,
    number: {
      orientation: "vertical",
      min: 1,
      max: 12,
      // NOTE: here could implement min/max values from the date options
    },
    value: inputified.dateValueObject[startOrEnd].month(),
    changed: function (value) {
      inputified.dateValueObject[startOrEnd].month(value - 1);
      inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject);
    },
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate year and month inputs
  ARGUMENTS: (
    $container <yquerjObject>,
    inputified,
  )
  RETURN: <void>
*/
module.exports = function ($container, inputified) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // START
  var $yearMonth_startContainer = $container.div({ class: "panel-year_month-inputs panel-start", });
  var $yearMonth_startTitle = $yearMonth_startContainer.div({ class: "startend_title", text: "start", });
  var $yearMonth_startYear = $yearMonth_startContainer.div({ class: "panel-year", });
  var $yearMonth_startMonth = $yearMonth_startContainer.div({ class: "panel-month", });
  var startYearInputified = yearInput($yearMonth_startYear, inputified, "start");
  var startMonthInputified = monthInput($yearMonth_startMonth, inputified, "start");

  // END
  var $yearMonth_endContainer = $container.div({ class: "panel-year_month-inputs panel-end", });
  var $yearMonth_endTitle = $yearMonth_endContainer.div({ class: "startend_title", text: "end", });
  var $yearMonth_endYear = $yearMonth_endContainer.div({ class: "panel-year", });
  var $yearMonth_endMonth = $yearMonth_endContainer.div({ class: "panel-month", });
  var endYearInputified = yearInput($yearMonth_endYear, inputified, "end");
  var endMonthInputified = monthInput($yearMonth_endMonth, inputified, "end");

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REACT TO EVENTS IN OTHER UI PARTS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  inputified.onDatePickerEvent("dateChange", function (dateValueObject) {

    // start year or month have changed
    if (+startYearInputified.get() !== +dateValueObject.start.year()) startYearInputified.set(dateValueObject.start.year());
    if (+startMonthInputified.get() !== (dateValueObject.start.month() + 1)) startMonthInputified.set(dateValueObject.start.month() + 1);

    // end year or month have changed
    if (+endYearInputified.get() !== +dateValueObject.end.year()) endYearInputified.set(dateValueObject.end.year());
    if (+endMonthInputified.get() !== (dateValueObject.end.month() + 1)) endMonthInputified.set(dateValueObject.end.month() + 1);

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
