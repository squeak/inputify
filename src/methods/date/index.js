var _ = require("underscore");
var $$ = require("squeak");
var keyboardify = require("keyboardify");
var tippy = require("tippy.js");
var panels = {
  yearMonth: require("./picking/yearMonth"),
  date: require("./picking/date"),
  time: require("./picking/time"),
  precision: require("./setup/precision"),
  rangeSwitch: require("./setup/rangeSwitch"),
  result: require("./setup/result"),
};
var dateStringFrom = require("./stringFrom");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE PICKER AND EVENTS SUBSCRIPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makePickerAndEventsSubscriptions (inputified) {
  inputified.subscribedDatePickerEvent = {};

  //
  //                              CREATE DATE PICKER TIPPY

  inputified.dateTippied = tippy(inputified.$inputContainer[0], {
    allowHTML: true,
    interactive: true,
    placement: "bottom",
    theme: "default",
    trigger: "click",
    appendTo: $app[0],
    content: function () {
      inputified.$dateTimePickerContainer = $('<div class="inputify-calendar_input-popup"/>');
      displayDatePicker(inputified);
      return inputified.$dateTimePickerContainer[0];
    },
    onHide: function () {
      // remove esc key binding on hiding date picker
      keyboardify.unbind("esc", inputified.dateTippied.hide);
    },
    onShown: function () {
      // hide date picker on hitting esc key
      keyboardify.bind("esc", inputified.dateTippied.hide);
      // make sure that tippy container has custom class
      $(inputified.dateTippied.popper).addClass("inputify-calendar_input-tippy");
    },
  });

  //
  //                              ALLOWS EXTERNAL OPTIONS TO ALSO SUBSCRIBE TO EVENTS
  // done after all other events registration from the UI parts themselves (like that external callback is always last one)

  if (inputified.dateOptions.onRangeSwitch) inputified.onDatePickerEvent("rangeSwitch", inputified.dateOptions.onRangeSwitch);
  if (inputified.dateOptions.onPrecisionPick) inputified.onDatePickerEvent("precisionPick", inputified.dateOptions.onPrecisionPick);
  if (inputified.dateOptions.onDateChange) inputified.onDatePickerEvent("dateChange", inputified.dateOptions.onDateChange);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE CALENDAR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayDatePicker (inputified) {

  //
  //                              INITIALIZE

  // empty container in case of recreation
  inputified.$dateTimePickerContainer.empty();

  // set picker precision
  inputified.$dateTimePickerContainer.attr("precision", inputified.dateOptions.precision);
  inputified.$dateTimePickerContainer.attr("rangePicker", inputified.dateOptions.rangePicker);

  //
  //                              CREATE ALL CONTAINERS

  // different panels position
  var $header = inputified.$dateTimePickerContainer.div({ class: "header-panels", });
  var $body = inputified.$dateTimePickerContainer.div({ class: "body-panels", });
  var $footer = inputified.$dateTimePickerContainer.div({ class: "footer-panels", });

  // panels containers
  var $panels = {
    // date picking
    yearMonth: $body.div({ class: "panel panel-year_month", }),
    date: $body.div({ class: "panel panel-date", }),
    time: $body.div({ class: "panel panel-time", }),
    // date setup
    precision: $header.div({ class: "panel panel-precision", }),
    rangeSwitch: $footer.div({ class: "panel panel-range_switch", }),
    result: $footer.div({ class: "panel panel-result", }),
  };

  //
  //                              CREATE UI

  // date picking
  panels.yearMonth($panels.yearMonth, inputified);
  panels.date($panels.date, inputified);
  panels.time($panels.time, inputified);

  // date setup
  panels.precision($panels.precision, inputified);
  panels.rangeSwitch($panels.rangeSwitch, inputified);
  panels.result($panels.result, inputified);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DATE DEFAULT OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeDefaultDateOptions (inputified) {
  return {
    precision: "date",
    precisionChoiceDisable: false,
    rangePicker: false,
    rangeChoiceDisable: false,
  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  make: function () {
    var inputified = this;

    //
    //                              INITIALIZE

    // calculate options
    inputified.dateOptions = inputified.getSpecificOptions("date", makeDefaultDateOptions(inputified));
    // initialize dateValueObject
    inputified.dateValueObject = dateStringFrom(inputified.value, inputified);

    //
    //                              ADD EVENTS SYSTEM
    // allow to subscribe to some events fired by other parts of the code

    inputified.onDatePickerEvent = function (eventType, action) {
      if (!inputified.subscribedDatePickerEvent[eventType]) inputified.subscribedDatePickerEvent[eventType] = [];
      inputified.subscribedDatePickerEvent[eventType].push(action);
    };
    inputified.fireDatePickerEvent = function (eventType) {
      var actionArgs = _.rest(arguments);
      if (!inputified.subscribedDatePickerEvent[eventType]) return;
      else _.each(inputified.subscribedDatePickerEvent[eventType], function (action) {
        action.apply(inputified, actionArgs);
      });
    };

    //
    //                              INITIALIZE UI AND EVENTS

    makePickerAndEventsSubscriptions(inputified);

    //
    //                              LISTEN TO CHANGES ON THE INPUT

    inputified.$input.on("input", function () {
      var newValue = $(this).val();
      // set value, but only if it's valid
      if (newValue === "" || dateStringFrom(newValue).isValid) {
        inputified.value = newValue;
        inputified.dateValueObject = dateStringFrom(newValue, inputified, { noChanged: true, noSetInput: true, noSetValue: true, });
        inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject, { noSetInput: true, noSetValue: true, });
        inputified.$inputContainer.removeClass("invalid");
      }
      else inputified.$inputContainer.addClass("invalid");
    });

    //
    //                              COLORIZE INPUT

    inputified.initializeColor({
      base: { backgroundColor: ["&"], },
      baseContrast: { color: ["&"], },
    });

    if (inputified.options.color) {
      if (inputified.options.color.base) {
        inputified.$dateTimePickerContainer.css("background-color", inputified.options.color.base);
        inputified.$dateTimePickerContainer.parent(".tippy-content").css("background-color", inputified.options.color.base);
        inputified.$dateTimePickerContainer.parents(".tippy-box").find(".tippy-arrow").css("border-color", inputified.options.color.base);
      };
      if (inputified.options.color.baseContrast) {
        inputified.$dateTimePickerContainer.css("color", inputified.options.color.baseContrast);
      };
      // NOTE: modifications of calendar and inputs colors are done in their scripts respectively
    };

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    return this.value;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // control if value is an appropriate date or range
  valueSetVerify: function (processedValue) {
    if (processedValue && !dateStringFrom(processedValue).isValid) throw "Cannot set value, it's not a proper date or range.";
  },

  uiSet: function (processedValue) {
    var inputified = this;

    // set value
    inputified.value = processedValue;

    // regenerate dateValueObject
    inputified.dateValueObject = dateStringFrom(inputified.value, inputified, { noChanged: true, noSetValue: true, });

    // fire date change event
    inputified.fireDatePickerEvent("dateChange", inputified.dateValueObject, {
      // do not fire changed event
      noChanged: true,
      // don't set value, it's already done
      noSetValue: true,
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

};
