var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/date");
require("squeak/plugin/squeakDate");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (inputified) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // FIGURE OUT WHAT PARTS OF THE DATE TO PARSE
  if (inputified.dateOptions.precision === "year") var momentParseParts = ["year"];
  else if (inputified.dateOptions.precision === "month") var momentParseParts = ["year", "month"];
  else if (inputified.dateOptions.precision === "time") var momentParseParts = ["year", "month", "day", "hours", "minutes"];
  else var momentParseParts = ["year", "month", "day"];

  // FIGURE OUT IF SHOULD PARSE DATE OR DATE RANGE
  if (inputified.dateOptions.rangePicker) var dateOrRange = inputified.dateValueObject
  else var dateOrRange = inputified.dateValueObject.start;

  // PARSE DATE AND CONVERT IT TO STRING
  return $$.squeakDate(dateOrRange, { momentParseParts: momentParseParts, }).getString();

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
