var _ = require("underscore");
var $$ = require("squeak");
var L = require("leaflet");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MARKER ICON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var markerIcon = L.divIcon({
  className: "geoputify-icon icon-circle2", // radio-checked2
  iconSize: L.point(20, 20),
  iconAnchor: L.point(10, 10),
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ADD MARKER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function addMarker (map, coordinates, dragEventCallback) {

  var marker = L.marker(coordinates, {
    icon: markerIcon,
    draggable: true,
    color: "black",
  }).addTo(map).on("move", dragEventCallback);

  map.allInputifyGeometries.push(marker);

  return marker;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE RANDOMLY POSITIONNED POINTS IN VIEW
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function randomPoints (map, howMany) {
  var mapBounds = map.getBounds();
  var resultPoints = [];
  for (var i = 0; i < howMany; i++) {
    resultPoints.push([
      $$.random.number(mapBounds._northEast.lat, mapBounds._southWest.lat),
      $$.random.number(mapBounds._northEast.lng, mapBounds._southWest.lng),
    ]);
  };
  return resultPoints;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE POLYLINE OR POLYGON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create polyline or polygon
  ARGUMENTS: (
    !polywhat <"polyline"|polygon">
    !map <leafletMap>,
    ?geoValue <object>,
  )
  RETURN: <leflet polyline or polygon>
*/
function makePolySomething (polywhat, map, geoValue) {

  //
  //                              FIGURE OUT SHAPE COORDINATES

  var coordinates = $$.clone(geoValue.c, 3) || randomPoints(map, 3);
  geoValue.c = coordinates;

  //
  //                              CREATE SHAPE

  var shape = L[polywhat](coordinates, {
    color: "black",
  }).addTo(map);
  map.allInputifyGeometries.push(shape);

  //
  //                              INITIALIZE POINTS MARKERS

  var markers;
  drawPathMarkers();

  //
  //                              INITIALIZE, DETECTION WHEN POINTS SHOULD BE ADDED

  shape.on("click", function (e) {
    if (e.originalEvent && e.originalEvent.ctrlKey) {
      addPointInCoordinatesList(map, coordinates, [e.latlng.lat, e.latlng.lng], polywhat);
      redraw();
    };
  });

  //
  //                              REDRAW PATH AND IT'S MARKERS

  function redraw () {
    // redraw polyline
    shape.setLatLngs(coordinates);
    // redraw markers
    _.each(markers, function (marker) { map.removeLayer(marker); });
    drawPathMarkers();
  };

  //
  //                              DRAW PATH MARKERS

  function drawPathMarkers () {
    // create a marker for each line point
    markers = _.map(coordinates, function (coord, index) {

      var marker = addMarker(map, coord, function (e) {
        // modify point's coordinates
        coordinates[index] = [e.latlng.lat, e.latlng.lng];
        // redraw polyline
        shape.setLatLngs(coordinates);
      });
      marker.inputifyPolyIndex = index;

      // point removal
      marker.on("click", function (e) {
        if (e.originalEvent && e.originalEvent.ctrlKey) {
          coordinates.splice(marker.inputifyPolyIndex, 1);
          // redraw polyline
          redraw();
        };
      });

      return marker;
    });
  };

  //
  //                              RETURN PATH SHAPE

  return shape;

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FIGURE OUT AT WHICH INDEX IN POLYLINE LIST OF POINTS ADD THE GIVEN ONE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function addPointInCoordinatesList (map, coordinates, additionalPointCoordinates, polywhat) {
  var insertionIndex = getIndexWhereToInsert(map, coordinates, additionalPointCoordinates, polywhat == "polygon");
  coordinates.splice(insertionIndex, 0, additionalPointCoordinates);
};

function getIndexWhereToInsert (map, coordinates, additionalPointCoordinates, firstAndLastIsAlsoLine) {

  var closestPointSegment = _.chain(coordinates).map(function (coord, index) {
    return {
      index: index,
      coord: coord,
    };
  }).min(function (coordObj, index) {
    if (index < coordinates.length - 1) return distanceToSegment(map, additionalPointCoordinates, [coordObj.coord, coordinates[index+1]])
    else if (firstAndLastIsAlsoLine) return distanceToSegment(map, additionalPointCoordinates, [coordObj.coord, coordinates[0]])
    else return 9999999;
  }).value();

  return closestPointSegment.index + 1;

};

function distanceToSegment (map, pointCoordinates, lineCoordinates) {

  var point = map.latLngToLayerPoint(pointCoordinates);
  var linePoint1 = map.latLngToLayerPoint(lineCoordinates[0]);
  var linePoint2 = map.latLngToLayerPoint(lineCoordinates[1]);

  return L.LineUtil.pointToSegmentDistance(point, linePoint1, linePoint2);

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  POINT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  point: {
    icon: "radio-checked2",
    create: function (map, geoValue) {

      // figure out coordinates
      var coordinates = geoValue.c || [map.getCenter().lat, map.getCenter().lng];
      geoValue.c = coordinates;

      // add marker to map
      var marker = addMarker(map, coordinates, function (e) {
        geoValue.c = [e.latlng.lat, e.latlng.lng];
      });
      this.marker = marker;

    },
    focus: function (map) {
      map.panTo(this.marker.getLatLng());
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CIRCLE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  circle: {
    icon: "circle-thin",
    create: function (map, geoValue) {

      // figure out coordinates
      var coordinates = geoValue.c || [map.getCenter().lat, map.getCenter().lng];
      geoValue.c = coordinates;

      // figure out radius
      if (geoValue.radius) var radius = geoValue.radius
      else var radius = map.distance(map.getBounds()._northEast, map.getBounds()._southWest) / 10;
      radius = geoValue.radius = $$.round(radius, 2);

      // add circle to map
      var circle = L.circle(coordinates, {
        radius: radius,
        color: "black",
      }).addTo(map);
      map.allInputifyGeometries.push(circle);
      this.circle = circle;

      // add center marker
      var centerMarker = addMarker(map, coordinates ,function (e) {
        coordinates = geoValue.c = [e.latlng.lat, e.latlng.lng];
        circle.setLatLng(e.latlng);
        radiusMarker.setLatLng(getRadiusMarkerCoordinates());
      });

      // add radius marker
      var radiusMarker = addMarker(map, getRadiusMarkerCoordinates(), function (e) {
        // only resize if marker was moved by user, not programmatically (like when center is moved)
        if (e.originalEvent) {
          radius = geoValue.radius = $$.round(map.distance(centerMarker.getLatLng(), radiusMarker.getLatLng()), 2);
          circle.setRadius(radius);
        };
      });

      function getRadiusMarkerCoordinates () {
        var circleBounds = circle.getBounds();
        var radiusMarkerCoordinates = _.clone(coordinates);
        radiusMarkerCoordinates[1] = circleBounds._northEast.lng;
        return radiusMarkerCoordinates;
      };

    },
    focus: function (map) {
      map.fitBounds(this.circle.getBounds());
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  POLYLINE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  polyline: {
    icon: "share2",
    tip: "ctrl + click on the polyline to add new points<br>ctrl + click on points to remove them",
    create: function (map, geoValue) {
      this.polyline = makePolySomething("polyline", map, geoValue);
    },
    focus: function (map) {
      map.fitBounds(this.polyline.getBounds());
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  POLYGON
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  polygon: {
    icon: "star-full",
    tip: "ctrl + click on the polyline to add new points<br>ctrl + click on points to remove them",
    create: function (map, geoValue) {
      this.polygon = makePolySomething("polygon", map, geoValue);
    },
    focus: function (map) {
      map.fitBounds(this.polygon.getBounds());
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
