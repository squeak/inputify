var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (inputified) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var longInputified;
  uify.dialog({
    // o            >            +            #            °            ·            ^            :            |

    title: "Edit '"+ inputified.name +"' key",
    dialogClass: "inputify-long-dialog",
    disableEnterKeyValidation: true,

    content: function ($container) {
      longInputified = inputified.inputify({

        // make textarea
        $container: $container,
        type: "textarea",
        inputifyType: "textarea",
        labelLayout: "hidden",
        value: inputified.geoValue,
        monospace: true,

        // pass value verification and processing functions
        valueOutVerify: function (val) { if ($$.isJson(val)) return val; },
        valueInProcess: function (val) { return $$.json.pretty(val) || ""; },
        valueOutProcess: function (text) { return JSON.parse(text); },

      });
    },

    ok: function (value) {
      inputified.set(longInputified.get());
    },
    cancel: function () {
      if (inputified.internalGet() != longInputified.get() && !confirm("Close and lose modifications.")) return false;
    },

    // o            >            +            #            °            ·            ^            :            |
  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
