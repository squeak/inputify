var _ = require("underscore");
var $$ = require("squeak");
var L = require("leaflet");
var uify = require("uify");
var geoShapes = require("./shapes");
var editRaw = require("./raw");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INITIALIZE LEAFLET MAP
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var inputifyLeafletMapDialog;
var inputifyLeafletMap;
var resultGeoValue;
function initializeLeafletMap () {

  var leafletMap;

  inputifyLeafletMapDialog = uify.confirm({
    title: "Edit geometry",
    dialogClass: "geoputify-dialog",
    height: 95,
    width: 95,
    overlayClickExits: false,
    disableEscKeyCancel: true,
    content: function ($container) {
      var dialog = this;

      // CREATE MAP CONTAINER
      var mapUUID = "geoputify--"+ $$.uuid();
      var $map = $container.div({ class: "geoputify-map", });

      // SET MAP INITIAL VIEW
      inputifyLeafletMap = dialog.leafletMap = L.map($map[0]).setView([25.84, 16.83], 2);

      // ADD METHODS TO MAP OBJECT
      inputifyLeafletMap.allInputifyGeometries = [];
      inputifyLeafletMap.inputifiedClearMap = function () {
        _.each(inputifyLeafletMap.allInputifyGeometries, function (geometry) {
          inputifyLeafletMap.removeLayer(geometry)
        });
      };

      // SETUP MAP TILES
      L.tileLayer("https://{s}.tile.osm.org/{z}/{x}/{y}.png", {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors',
      }).addTo(inputifyLeafletMap);

      // ADD MAP SCALE
      L.control.scale({
        imperial: false,
        position: "bottomright",
      }).addTo(inputifyLeafletMap);

      // CREATE BUTTONS CONTAINER
      dialog.$mapButtons = $container.div({ class: "geoputify-buttons", });
      dialog.$mapButtonBack = $container.div({ class: "geoputify-buttons", });
      uify.button({
        $container: dialog.$mapButtonBack,
        icomoon: "trash",
        title: "discard",
        inlineTitle: "right",
        invertColor: true,
        click: function () {
          resultGeoValue = {};
          inputifyLeafletMap.inputifiedClearMap();
          chooseMode();
        },
      });
      dialog.$mapButtonBack.hide();

      // CREATE TIP CONTAINER
      dialog.$tip = $container.div({ class: "geoputify-tip", })

    },
    ok: function () {
      inputifyLeafletMap.inputified.set(resultGeoValue);
      inputifyLeafletMapDialog.hide();
      return false;
    },
    cancel: function () {
      inputifyLeafletMapDialog.hide();
      return false;
    },
  });

};

function openMap () {
  if (!inputifyLeafletMapDialog) initializeLeafletMap()
  else inputifyLeafletMapDialog.show();
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHANGE MODE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function editMode (shapeObject) {
  inputifyLeafletMapDialog.$mapButtons.hide();
  inputifyLeafletMapDialog.$mapButtonBack.show();
  inputifyLeafletMapDialog.$tip.htmlSanitized(shapeObject.tip || "");
};

function chooseMode () {
  inputifyLeafletMapDialog.$mapButtonBack.hide();
  inputifyLeafletMapDialog.$mapButtons.show();
  inputifyLeafletMapDialog.$tip.htmlSanitized("");
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAP BUTTONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function regenerateMapButtons (inputified) {
  inputifyLeafletMapDialog.$mapButtons.empty();

  // create button for each allowed shape
  _.each(inputified.geoOptions.shape, function (shapeName) {

    // GET SHAPE OBJECT
    var shapeObject = geoShapes[shapeName];

    // UNSUPPORTED TYPE OF SHAPE
    if (!shapeObject) return $$.log.error("[inputify-geo] Geo shape named '"+ shapeName +"' don't seem to be supported.", "Available shapes are: "+ _.keys(geoShapes).join(", "));

    // CREATE BUTTON
    uify.button({
      $container: inputifyLeafletMapDialog.$mapButtons,
      icomoon: shapeObject.icon,
      title: shapeName,
      inlineTitle: "right",
      invertColor: true,
      click: function () {
        resultGeoValue.shape = shapeName;
        shapeObject.create(inputifyLeafletMap, resultGeoValue);
        editMode(shapeObject);
      },
    });

  });

  // make sure to be in choose mode
  chooseMode();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  EDIT SHAPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function editShape (inputified) {

  // OPEN/REOPEN MAP
  openMap();

  // DETECT IF SHOULD REGENERATE MAP BUTTONS AND CLEAR MAP OR NOT
  if (inputified.geoInputifiedId !== inputifyLeafletMap.geoInputifiedId) {
    inputifyLeafletMap.inputifiedClearMap();
    inputifyLeafletMap.inputified = inputified;
    regenerateMapButtons(inputified);
  };

  resultGeoValue = _.clone(inputified.geoValue) || {};

  // INITIALIZE SHAPE IF ALREADY ONE DEFINED
  if (resultGeoValue.shape) {
    var shapeObject = geoShapes[resultGeoValue.shape];
    if (!shapeObject) $$.log.error("[inputify-geo] Geo shape named '"+ resultGeoValue.shape +"' don't seem to be supported.", "Available shapes are: "+ _.keys(geoShapes).join(", "))
    else {
      // enter in edit mode
      editMode(shapeObject);
      // create shape
      shapeObject.create(inputifyLeafletMap, resultGeoValue);
      // zoom the map to the shape
      shapeObject.focus(inputifyLeafletMap);
    };
  };

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  make: function () {
    var inputified = this;

    inputified.geoValue = {};
    inputified.geoInputifiedId = $$.uuid();
    inputified.geoOptions = inputified.getSpecificOptions("geo", {
      shape: [ "point", "circle", "polyline", "polygon", ], // TODO: ADD SUPPORT FOR RECTANGLES (and also in wiki)
    });

    // value display
    inputified.$geoValuePre = inputified.$input.pre().click(function () {
      editShape(inputified);
    });

    // edit buttons
    uify.button({
      $container: inputified.$input,
      icomoon: "quill",
      title: "edit interactively",
      // inlineTitle: "right",
      // invertColor: true,
      click: function () {
        editShape(inputified);
      },
    });
    uify.button({
      $container: inputified.$input,
      icomoon: "code",
      title: "edit raw",
      // inlineTitle: "right",
      // invertColor: true,
      click: function () {
        editRaw(inputified);
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    var inputified = this;
    return inputified.geoValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET VALUE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiSet: function (processedValue) {
    var inputified = this;

    // save in geoValue
    inputified.geoValue = processedValue;

    // display
    if (_.isObject(processedValue)) processedValue = $$.json.pretty(processedValue, true);
    inputified.$geoValuePre.text(processedValue || "");

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
