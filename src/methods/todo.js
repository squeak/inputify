var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  make: function () {
    this.$input.div({ text: "||TODO|| this input is not yet editable ||TODO||", });
  },

  uiGet: function () {
    return this.todoInputValue;
  },

  uiSet: function (processedValue, valueVerified) {
    this.todoInputValue = valueVerified;
    this.$input.empty();
    this.$input.div({ text: "||TODO|| this input is not yet editable ||TODO||", });
    if (processedValue) this.$input.div({ text: processedValue, });
  },

};
