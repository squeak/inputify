var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  make: function () {
    var inputified = this;

    var defaultNumberOptions = {
      orientation: "horizontal",
      min: 0,
      step: 1,
    };
    var numberOptions = $$.defaults(defaultNumberOptions, inputified.getSpecificOptions("number"));

    //
    //                              TRADUCE POSITIONS

    var positions = {
      horizontal: ["left", "right"],
      vertical: ["bottom", "top"],
    };
    var position = positions[numberOptions.orientation];

    // add orientation class
    inputified.$input.addClass("inputify-integer-"+ numberOptions.orientation);

    //
    //                              CREATE BUTTONS

    inputified.number = {

      // LEFT/BOTTOM BUTTON
      $leftButton: inputified.$input.div({
        class: "button button-"+ position[0] +" icon-minus",
      }).click(function () {
        if (_.isUndefined(inputified.number.value)) {
          inputified.set(numberOptions.max || 0);
          inputified.changed();
        }
        else if (_.isNull(numberOptions.min) || _.isUndefined(numberOptions.max) || (inputified.number.value > numberOptions.min)) {
          inputified.set((inputified.number.value || numberOptions.max) - numberOptions.step);
          inputified.changed();
        };
      }),

      // TEXT
      $centerText: inputified.$input.div({
        class: "text",
      }).dblclick(function () {
        var newNumber = prompt("Set "+ inputified.name +" value:");
        if (!_.isNull(newNumber)) {
          newNumber = +newNumber;
          if (_.isNaN(newNumber)) alert("You must enter a number.")
          else if (!_.isNull(numberOptions.min) && newNumber < numberOptions.min) alert("Minimum value is: "+ numberOptions.min +"\nThe value you chose ("+ newNumber +") is to low.")
          else if (!_.isNull(numberOptions.max) && newNumber > numberOptions.max) alert("Maximume value is: "+ numberOptions.max +"\nThe value you chose ("+ newNumber +") is to high.")
          else {
            inputified.set(newNumber);
            inputified.changed();
          };
        };
      }),

      // RIGHT/TOP BUTTON
      $rightButton: inputified.$input.div({
        class: "button button-"+ position[1] +" icon-plus",
      }).click(function () {
        if (_.isUndefined(inputified.number.value)) {
          inputified.set(numberOptions.min || 0);
          inputified.changed();
        }
        else if (_.isNull(numberOptions.max) || (inputified.number.value < numberOptions.max)) {
          inputified.set((inputified.number.value || numberOptions.min) + numberOptions.step);
          inputified.changed();
        };
      }),

    };


    //
    //                              COLOR

    inputified.initializeColor({
      primary: { backgroundColor: [".button"], },
      primaryContrast: { color: [".button"], },
      hover: { backgroundColor: [".button"], },
      hoverContrast: { color: [".button"], },
      unselectedColor: "baseContrast",
    });


    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    var inputified = this;
    return inputified.number.value;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiSet: function (processedValue) {
    var inputified = this;
    inputified.number.value = +processedValue || inputified.options.number.min;
    inputified.number.$centerText.htmlSanitized(inputified.number.value);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
