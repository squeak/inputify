var _ = require("underscore");
var $$ = require("squeak");
var makeDropdown = require("./dropdown");
var uify = require("uify");
var updateSuggestionsInDropdown = require("./suggestions");
var Sortablejs = require("sortablejs");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeIsAsync: true,
  make: function (makeCallback) {
    var inputified = this;

    //
    //                              GET OPTIONS WITH DEFAULTS

    // set choices are not ready yet
    inputified.choicesAreReady = false;

    // get full options
    inputified.suggestOptions = inputified.getSpecificOptions("suggest", {
      // maxItemCount: 0, // implicit
      addItems: true,
      duplicateItemsAllowed: false,
      noResultsText: "No results found",
      tooManyItemsText: "You have chosen the maximum number of items",
      errorResultText: function (e) { return '<span><b>This value is invalid:</b><br><span class="error-text">'+ e +'</span></span>'; },
    });

    // no results text (displayed addItems is disabled and the text entered doesn't match any choice)
    inputified.noResultsText = "No results found";

    // log that valueOutVerify is not supported
    if (inputified.options.valueOutVerify) $$.log.detailedError(
      "inputify/suggest:make",
      "You used valueOutVerify option in a suggest-like input. Those inputs don't support this option, you shoul use suggest.labelToValue option instead.",
      "the options you've set: ", inputified
    );

    // apply general classes (for easier stryling)
    inputified.$inputContainer.addClass("inputify_container-suggest_like");
    inputified.$input.addClass("inputify-suggest_like");
    if (inputified.suggestOptions.maxItemCount === 1) inputified.$input.addClass("inputify-suggest_like-single");

    //
    //                              MAKE INPUT

    inputified.$inputResultContent = inputified.$input.div({ class: "inputify-sl-result", });
    makeDropdown(inputified);

    //
    //                              MAKE CHOSEN ITEMS REORDERABLE

    if (!inputified.hasSingleItem()) Sortablejs.create(inputified.$inputResultContent[0], {
      onSort: function (evt) {

        // this should never happen, but nothing to do if result value is undefined
        if (!inputified.value) return;

        // update array
        if (evt.from === evt.to) inputified.value = $$.array.move(inputified.value, { from: evt.oldIndex, to: evt.newIndex });

        // callback result list if changed function
        inputified.changed();

        // recalculate position of dropdown (in case the height of the input has changed) // really just in case
        if (inputified.tippySuggestDropdown && inputified.tippySuggestDropdown.popperInstance) inputified.tippySuggestDropdown.popperInstance.forceUpdate();

      },
    });

    //
    //                              INITIALIZE RESULTS CUSTOM COLORS

    inputified.refreshResultsCustomColors();

    //
    //                              MAKE APPROPRIATE CHOICES LIST (async) AND CALLBACK MADE FINISHED

    inputified.refreshChoices(function () {
      // run make callback ('cause make is async)
      makeCallback();
    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGetRaw: function () {
    var inputified = this;

    // return value as is
    if (inputified.hasSingleItem()) return inputified.value
    // make sure to return undefined instead of an empty array
    else return _.isUndefined(inputified.value) || !inputified.value.length ? undefined : inputified.value;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  systemValueInVerify: function (value) {
    var inputified = this;

    // no value, ok
    if (_.isUndefined(value)) return value

    // accept all values
    else if (inputified.suggestOptions.addItems) return value

    // filter out values that are not in choices list
    else return inputified.getAcceptedValues(value, inputified.choicesList);

  },

  uiSet: function (processedValue, verifiedValue) {
    var inputified = this;

    // set value to inputified
    inputified.value = verifiedValue;

    // trying to set value too early, before choices list is created
    if (!inputified.choicesAreReady) return $$.log.detailedError(
      "inputify/suggest — inputified.name: "+ inputified.name,
      "You tried to set this input value, but choices are not interpretted yet, you need to wait for them to be ready before setting a value.",
      "The value you tried to set: ", verifiedValue,
      "To make sure to set and input after it's choices have been calculated, use the 'value', 'defaultValue', 'prepareValue' options, or put your code in a callback passed using the 'created' option.",
      "If you manually called refreshChoices, pass it a callback to know when it has finished."
    );

    // get list of choices to select (if choice not in list, will generate it — this is ok because if creation is not allowed, the values not matching an existing choice have been removed by inputified.systemValueInVerify)
    var choicesToSelect = inputified.findOrMakeChoicesForThisValue(verifiedValue, inputified.choicesList);

    // empty result content
    inputified.$inputResultContent.empty();

    // set values in result content
    if (!_.isUndefined(choicesToSelect)) _.each(_.isArray(choicesToSelect) ? choicesToSelect : [choicesToSelect], function (choice) {
      inputified.addValueInResult(choice);
    });

    // update list of choices
    updateSuggestionsInDropdown(inputified);

    // refresh results colors
    inputified.refreshResultsCustomColors();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH LIST OF CHOICES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: refresh the list of choices (this is asynchronous)
    ARGUMENTS: ( ?callback <function(ø)> )
    RETURN: <void>
  */
  refreshChoices: function (callback) {
    var inputified = this;
    var spinner = uify.spinner({
      $container: inputified.$inputContainer,
      size: 5,
      overlay: true,
      delay: 200,
    });
    inputified.choicesAreReady = false;
    inputified.interpretChoices(function (choices) {
      inputified.choicesAreReady = true;
      inputified.choicesList = choices;
      updateSuggestionsInDropdown(inputified);
      spinner.destroy();
      if (callback) callback();
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE VALUE DIV CONTENT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  addValueInResult: function (choice) {
    var inputified = this;

    // VALUE CONTAINER
    var $valueContent = inputified.$inputResultContent.div({ class: "inputify-sl-value", }).click(function (e) {
      // prevent dropdown from closing on result click (but not for single inputs)
      if (!inputified.hasSingleItem() && inputified.$inputContainer.hasClass("inputify-sl-dropdown_visible")) e.stopPropagation();
      // set current item
      inputified.$inputResultContent.children().removeClass("inputify-sl-value_current");
      $(this).addClass("inputify-sl-value_current");
    });

    // CONTENT (display choice label or value)
    $valueContent.div({
      class: "inputify-sl-value_content",
      htmlSanitized: _.isUndefined(choice.label) ? choice.value : choice.label,
    });

    // CROSS (remove value from list)
    $valueContent.div({ class: "inputify-sl-value_cross icon-cross", }).click(function (e) {
      var indexOfItemToRemove = $(this).parent().index();
      if (inputified.hasSingleItem()) inputified.value = undefined
      else inputified.value = _.reject(inputified.value, function (value, index) { return index === indexOfItemToRemove; });
      $valueContent.remove();
      inputified.changed();
      updateSuggestionsInDropdown(inputified);
      // recalculate position of dropdown (in case the height of the input has changed)
      if (inputified.tippySuggestDropdown && inputified.tippySuggestDropdown.popperInstance) inputified.tippySuggestDropdown.popperInstance.forceUpdate()
      // do not close dropdown
      e.stopPropagation();
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH RESULTS CUSTOM COLORS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  refreshResultsCustomColors: function () {
    var inputified = this;
    if (inputified.options.color) inputified.initializeColor({
      base: { backgroundColor: [ "&" ], },
      baseContrast: { color: [ "&" ], },
      primary: { backgroundColor: [ ".inputify-sl-value", ], },
      primaryContrast: { color: [ ".inputify-sl-value", ".inputify-sl-value_cross", ], },
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REFRESH DROPDOWN CUSTOM COLORS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  refreshDropdownCustomColors: function () {
    var inputified = this;
    if (inputified.options.color) inputified.initializeColor({
      base: { backgroundColor: [
        "&&.inputify-suggest_like-dropdown",
        "&&.inputify-suggest_like-dropdown-searchbox",
      ], },
      baseContrast: { color: [
        "&&.inputify-suggest_like-dropdown",
        "&&.inputify-suggest_like-dropdown-searchbox",
      ], },
      primary: { backgroundColor: [
        "&&.inputify-sld-choice:not(.inputify-sld-choice_no_result)",
      ], },
      primaryContrast: { color: [
        "&&.inputify-sld-choice:not(.inputify-sld-choice_no_result)",
      ], },
      hover: { backgroundColor: [
        "&&.inputify-sld-choice:not(.inputify-sld-choice_no_result)",
        "&&.inputify-sld-choice:not(.inputify-sld-choice_no_result).inputify-sld-choice_hovered",
      ], },
      hoverContrast: { color: [
        "&&.inputify-sld-choice:not(.inputify-sld-choice_no_result)",
        "&&.inputify-sld-choice:not(.inputify-sld-choice_no_result).inputify-sld-choice_hovered",
      ], },
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
