var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ALLOW ITEMS CREATION FOR SIMPLE AND MULTI INPUTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: generate choice (with label and value) from the given label
  ARGUMENTS: (
    !inputified,
    ?label <any>,
  )
  RETURN: <inputify·choiceObject|undefined>
*/
function makeChoiceFromLabel (inputified, label) {
  try {
    // make choice
    if (!_.isUndefined(label) && label !== "") var enteredChoice = {
      _isChoice: true,
      label: label,
      value: inputified.suggestOptions.labelToValue ? inputified.suggestOptions.labelToValue.call(inputified, label) : label,
    };
    inputified.noResultsText = inputified.suggestOptions.noResultsText;
    return enteredChoice;
  }
  catch (e) {
    inputified.noResultsText = inputified.suggestOptions.errorResultText(e);
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE THE BEST POSSIBLE STRING FROM THE PASSED VALUE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function checkMatches (valueInChoice, enteredValue) {
  var val = _.isObject(valueInChoice) ? JSON.stringify(valueInChoice) : valueInChoice +"";
  val = val.toLowerCase();
  return val.includes(enteredValue.toLowerCase());
};

// exact match is worth 1000
// starting with the entered value is worth 10 + each match is worth 1
function checkHowManyMatchesAndIfStartsWithEnteredValue (valueInChoice, enteredValue) {
  var val = _.isObject(valueInChoice) ? JSON.stringify(valueInChoice) : valueInChoice +"";
  if (val.toLowerCase() === enteredValue) return -1000
  else {
    var matchesList = val.toLowerCase().match($$.regexp.make(enteredValue.toLowerCase(), "g")) || [];
    return ($$.match(valueInChoice, "^"+ enteredValue) ? 0 : 10) - matchesList.length;
  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SUGGESTION DIV CONTENT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeSuggestionDivContent ($container, choice, inputified) {
  $container.div({
    class: "inputify-sld-choice" + (choice.isToCreate ? " inputify-sld-choice_to_create" : ""),
    htmlSanitized: _.isUndefined(choice.label) ? choice.value : choice.label,
  }).click(function (e) {
    if (inputified.hasSingleItem()) {
      inputified.value = choice.value;
      // clear list of results (before adding this result below)
      inputified.$inputResultContent.empty();
      // close dropdown
      if (inputified.tippySuggestDropdown) inputified.tippySuggestDropdown.hide();
    }
    else {
      if (!inputified.value) inputified.value = [];
      inputified.value.push(choice.value);
    };
    inputified.addValueInResult(choice);
    inputified.changed();
    inputified.$dropdownSearchBox.val(""); // remove text in dropdown search box
    updateSuggestionsInDropdown(inputified);
    // recalculate position of dropdown (in case the height of the input has changed)
    if (inputified.tippySuggestDropdown && inputified.tippySuggestDropdown.popperInstance) inputified.tippySuggestDropdown.popperInstance.forceUpdate();
    // refresh results colors
    inputified.refreshResultsCustomColors();
    // avoid
    e.stopPropagation();
  });
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: refresh the list of suggestions in the dropdown
  ARGUMENTS: (
    inputified,
    enteredValue <string>,
  )
  RETURN: <void>
*/
function updateDropdownSuggestions (inputified, enteredValue) {

  //
  //                              GET ALL CHOICES

  var choices = _.clone(inputified.choicesList);

  //
  //                              FILTER LIST OF CHOICES

  if (enteredValue) choices = _.filter(choices, function (choice) {
    // choice.value matches
    if (checkMatches(choice.value, enteredValue)) return true;
    // choice.label matches
    if (choice.label && checkMatches(choice.label, enteredValue)) return true;
    // choice.search matches
    if (choice.search && checkMatches(choice.search, enteredValue)) return true;
  });

  //
  //                              SORT LIST OF CHOICES

  if (enteredValue) choices = _.sortBy(choices, function (choice) {
    return checkHowManyMatchesAndIfStartsWithEnteredValue(choice.value, enteredValue) +
           checkHowManyMatchesAndIfStartsWithEnteredValue(choice.label, enteredValue) +
           checkHowManyMatchesAndIfStartsWithEnteredValue(choice.search, enteredValue);
  });

  //
  //                              GET CURRENTLY INPUTED CHOICE (if this choice doesn't exist, add possibility to create it)

  if (inputified.suggestOptions.addItems) {
    var enteredChoice = makeChoiceFromLabel(inputified, enteredValue);
    // there is an entered choice, and it's not part of the list
    if (enteredChoice && !_.find(choices, function (choice) {
      return _.isEqual(choice.value, enteredChoice.value);
    })) {
      // set choice is to create
      enteredChoice.isToCreate = true;
      // add value to choices
      choices.unshift(enteredChoice);
    };
  };

  //
  //                              GET LIST OF CHOICES WITHOUT SELECTED ITEMS

  // list chosen items
  var itemValue = inputified.internalGet({ objectResult: false, });
  if (itemValue) var chosenItems = inputified.doOnVal(itemValue, function (value) {
    return inputified.makeChoice(value);
  });

  // make list of choices without selected items
  if (!inputified.suggestOptions.duplicateItemsAllowed) choices = _.reject(choices, function (choice) {
    return _.some(chosenItems, function (item) {
      return _.isEqual(item.value, choice.value);
    });
  });

  //
  //                              IF MAX ITEMS COUNT HAS BEEN PASSED

  if (
    inputified.suggestOptions.maxItemCount > 1 &&
    _.isArray(inputified.value) &&
    inputified.value.length >= inputified.suggestOptions.maxItemCount
  ) {
    // display custom message
    inputified.noResultsText = inputified.suggestOptions.tooManyItemsText;
    // do not suggest any choices
    choices = [];
  }
  // revert only if it's set to tooManyItemsText, otherwise it must be the right text (normal or error text)
  else if (inputified.noResultsText === inputified.suggestOptions.tooManyItemsText) inputified.noResultsText = inputified.suggestOptions.noResultsText;

  //
  //                              SHOW MATCHING SUGGESTIONS IN DROPDOWN

  // make sure list of suggestions is empty
  inputified.$dropdownSuggestionsList.empty();

  // add suggestions to list
  _.each(choices, function (choice) {
    makeSuggestionDivContent(inputified.$dropdownSuggestionsList, choice, inputified);
  });

  // if no choice to propose, display "No results" message
  if (!choices || !choices.length) inputified.$dropdownSuggestionsList.div({
    class: "inputify-sld-choice inputify-sld-choice_no_result",
    htmlSanitized: inputified.noResultsText,
  });

  // remake dropdown colors every time suggestions are refreshed
  inputified.refreshDropdownCustomColors();

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UPDATE SUGGESTIONS IN DROPDOWN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function updateSuggestionsInDropdown (inputified, enteredValue) {

  // LAZY LOAD
  if (inputified.suggestOptions.lazyLoad) {
    // NOT YET NECESSARY TO UPDATE SUGGESTIONS
    if (!inputified.lazyLoadInitialized) return
    // LOAD WITH SPINNER
    else {
      var spinner = uify.spinner({
        $container: inputified.$dropdownContainer, // NOTE: inputified.$dropdownSuggestionsList would be prettier, but if the user scroll the overlay scrolls as well
        size: 5,
        overlay: true,
      });
      setTimeout(function () {
        updateDropdownSuggestions(inputified, enteredValue);
        spinner.destroy();
      }, 10);
    };
  }

  // SIMPLE LOAD
  else updateDropdownSuggestions(inputified, enteredValue);

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = updateSuggestionsInDropdown;
