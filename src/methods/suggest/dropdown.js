var _ = require("underscore");
var $$ = require("squeak");
var tippy = require("tippy.js");
var updateSuggestionsInDropdown = require("./suggestions");
var $ = require("yquerj");
var keyboardify = require("keyboardify");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DROPDOWN CONTENT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function initializeDropdownContent ($container, inputified) {

  var itemToSelect = -1;
  inputified.$dropdownSearchBox = $container.input({ class: "inputify-suggest_like-dropdown-searchbox", }).on("input", function () {
    updateSuggestionsInDropdown(inputified, inputified.$dropdownSearchBox.val());
  }).keyup(function (event) {

    // click first suggested element when hitting enter in input
    if (event.keyCode === 13) $(inputified.$dropdownSuggestionsList.children()[itemToSelect == -1 ? 0 : itemToSelect]).click();

    // switch focus up/down in list of elements
    if (event.keyCode === 38 || event.keyCode === 40) {
      if (event.keyCode === 38 && itemToSelect > 0) { itemToSelect--; highlightHoveredItem(inputified, itemToSelect) }
      else if (
        event.keyCode === 40 && // down key
        itemToSelect < inputified.$dropdownSuggestionsList.children().length // didn't reach last element in suggestion list yet
      ) { itemToSelect++; highlightHoveredItem(inputified, itemToSelect) }
    }
    // reset item to select to initial state on typing any key that is down or up
    else itemToSelect = -1;

    // set previous/next chosen item as current to edit
    if (inputified.$dropdownSearchBox.val() === "") {
      if (event.keyCode === 37) setCurrentItem(inputified, "prev", "last")
      else if (event.keyCode === 39) setCurrentItem(inputified, "next", "first");
    };

  }).keydown(function (event) {

    // go back to editing last chosen item in list when hitting backspace (and search box is empty)
    // do this on keydown, before the backspace had effect
    if (event.keyCode === 8 && inputified.$dropdownSearchBox.val() === "") editCurrentItem(inputified);

  });

  inputified.$dropdownSuggestionsList = $container.div({ class: "inputify-suggest_like-dropdown-suggestions", });
  updateSuggestionsInDropdown(inputified);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHANGE CURRENT ITEM
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: figure out the item (in result value) that should be selected
  ARGUMENTS: (
    inputified,
    nextOrPrev <"next"|"prev"> « select previous or next item than the one that is currently selected »,
    firstOrLast <"first"|"last"> « item to select in list of results, if no item was previously selected »,
  )
  RETURN: <void>
*/
function setCurrentItem (inputified, nextOrPrev, firstOrLast) {

  // figure out if there is a currently selected item
  var $currentItem = inputified.$inputResultContent.children(".inputify-sl-value_current")

  // unselect all items
  inputified.$inputResultContent.children().removeClass("inputify-sl-value_current");

  // select first/last item if nothing was selected before
  if (!$currentItem.length) var $itemToSelect = inputified.$inputResultContent.children()[firstOrLast]()
  // select previous item if there is a previous one
  else if ($currentItem[nextOrPrev]().length) var $itemToSelect = $currentItem[nextOrPrev]();
  // reselect current item if no previous item
  else var $itemToSelect = $currentItem;

  // set item as selected
  $itemToSelect.addClass("inputify-sl-value_current");

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  EDIT LAST CHOSEN ITEM
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: reedit the currently selected item in results
  ARGUMENTS: ( inputified )
  RETURN: <void>
*/
function editCurrentItem (inputified) {

  // set searchbox text value to last result value
  if (inputified.$inputResultContent.children(".inputify-sl-value_current").length) var $currentItem = inputified.$inputResultContent.children(".inputify-sl-value_current")
  else var $currentItem = inputified.$inputResultContent.children().last();
  inputified.$dropdownSearchBox.val($currentItem.text() +" "); // +" " so that if backspace was hit, this last space is deleted, and not the last character of the chosen string

  // save current item index before removing it
  var currentItemIndex = $currentItem.index();

  // remove last result
  $currentItem.remove();

  // update list of suggestions
  updateSuggestionsInDropdown(inputified, inputified.$dropdownSearchBox.val());

  // set change in value as well
  if (inputified.hasSingleItem()) inputified.value = undefined
  else inputified.value = _.reject(inputified.value, function (val, index) { return index === currentItemIndex; });
  inputified.changed();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  HIGHLIGHT HOVERED ITEM
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function highlightHoveredItem (inputified, itemToSelect) {
  inputified.$dropdownSuggestionsList.children().removeClass("inputify-sld-choice_hovered");
  $(inputified.$dropdownSuggestionsList.children()[itemToSelect]).addClass("inputify-sld-choice_hovered");
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE TIPPY DROPDOWN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeTippyDropdown (inputified) {

  inputified.$input.div({ class: "inputify-sl-arrow icon-caret-down", });

  inputified.tippySuggestDropdown = tippy(inputified.$input[0], {
    allowHTML: true,
    interactive: true,
    placement: "bottom",
    theme: "suggest-dropdown",
    duration: 0,
    trigger: "click",
    appendTo: inputified.$inputContainer[0],
    maxWidth: "none",
    // NOTE: using fixed popper strategy creates some bugs, but without it, if the input is in a dialog the dropdown is not fully visible
    // so the following line checks if the input is in a uify dialog (won't resolve all, but at least some cases)
    popperOptions: inputified.$input.parents(".uify-dialog").length ? { strategy: "fixed", } : undefined,
    content: function () {
      inputified.$dropdownContainer = $('<div class="inputify-suggest_like-dropdown inputify-'+ inputified.options.type +'-dropdown"/>');
      inputified.$dropdownContainer.width(inputified.$input[0].offsetWidth - 20);
      initializeDropdownContent(inputified.$dropdownContainer, inputified);
      return inputified.$dropdownContainer[0];
    },
    onHide: function () {
      // remove esc key binding on hiding dropdown
      keyboardify.unbind("esc", inputified.tippySuggestDropdown.hide);
      // unset that dropdown is shown on input container
      inputified.$inputContainer.removeClass("inputify-sl-dropdown_visible");
      // make sure focus is out of search box
      inputified.$dropdownSearchBox.blur();
    },
    onShow: function () {
      // recalculate dropdown width when showing it (in case the input dimensions changed since it's creation)
      inputified.$dropdownContainer.width(inputified.$input[0].offsetWidth - 20);
      // initialize lazyLoaded dropdown
      if (!inputified.lazyLoadInitialized) {
        inputified.lazyLoadInitialized = true;
        updateSuggestionsInDropdown(inputified);
      };
    },
    onShown: function () {
      // hide dropdown on hitting esc key
      keyboardify.bind("esc", inputified.tippySuggestDropdown.hide);
      // set that dropdown is shown on input container
      inputified.$inputContainer.addClass("inputify-sl-dropdown_visible");
      // focus on search box input
      inputified.$dropdownSearchBox.focus();
      // initialize dropdown color every time it is shown (since it's recreated every time)
      inputified.refreshDropdownCustomColors();
    },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DIALOG DROPDOWN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeDialogDropdown (inputified) {

  // initialize dropdown container so it's content can exist and be updated even if dropdown dialog is not shown
  inputified.$dropdownContainer = $('<div class="inputify-suggest_like-dropdown inputify-'+ inputified.options.type +'-dropdown"/>');
  initializeDropdownContent(inputified.$dropdownContainer, inputified);

  // create clickable icon to open dropdown
  inputified.$input.div({ class: "inputify-sl-fullscreen icon-enlarge", }).click(function () {
    openDialogDropdown(inputified);
  });

  // open dropdown if clicking anywhere in input
  inputified.$input.click(function () {
    // opend dialog dropdown only if not already in dialog
    if (!$(this).parents(".inputify-sl-fullscreen_dropdown_dialog").length) openDialogDropdown(inputified);
  });

};

function openDialogDropdown (inputified) {

  var uifyDialogFullscreen = uify.dialog.fullscreen({
    preventHidingOnClick: true,
    content: function ($dialogContainer) {

      // LABEL
      $dialogContainer.div({
        class: "inputify-suggest_like-dropdown-input_label",
        htmlSanitized: !_.isUndefined(inputified.options.label) ? inputified.options.label : inputified.options.name,
      });

      // CHOSEN
      inputified.$input.appendTo($dialogContainer);
      // CHOICES
      inputified.$dropdownContainer.appendTo($dialogContainer);
      // initialize lazyLoaded dropdown
      inputified.lazyLoadInitialized = true;
      updateSuggestionsInDropdown(inputified);
      // set that dropdown is shown on input container
      inputified.$inputContainer.addClass("inputify-sl-dropdown_visible");
      // focus on search box input
      inputified.$dropdownSearchBox.focus();
      // initialize dropdown color every time it is shown (since it's recreated every time)
      inputified.refreshDropdownCustomColors();

    },
    onDestroy: function () {
      inputified.$input.appendTo(inputified.$inputContainer);
    },
  });

  // add custom class to fullscreen dialog
  uifyDialogFullscreen.$el.addClass("inputify-sl-fullscreen_dropdown_dialog");

  // modify dialog closing icon
  uifyDialogFullscreen.closeButton.$icon.addClass("icon-shrink");

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DROPDOWN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (inputified) {

  if ($$.isTouchDevice() || $(window).width() < 700) makeDialogDropdown(inputified)
  else makeTippyDropdown(inputified);

};
