var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/dom");
var defaultMethods = require("./_");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  make: function () {
    var inputified = this;

    defaultMethods.make.call(inputified);

    // add tab support to the created textarea
    $$.dom.addTabSupport(inputified.$input);

  },

};
