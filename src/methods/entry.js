var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ENTRY EDIT DIALOG MAKER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create uify.confirm dialog to edit the entry
  ARGUMENTS: ({
    !contentMaker: <function($container)>,
    !save: <function(ø)>,
    !discard: <function(ø)>,
  }){@this=inputified}
  RETURN: <{
    ?dialog: <any> «
      the type of this will depend on your custom dialog method, pass here what is useful to be able to manipulate the dialog easily
      this key is not necessary for inputified internal functioning
    »,
    !destroy: <function(ø)> « destroy the dialog if it's opened »,
  }>
*/
function createEntryEditDialog (dialogOptions) {
  var inputified = this;

  var entryEditDialog = uify.dialog({

    title: "Edit '"+ inputified.name +"' key",
    dialogClass: "inputify-entry-dialog",
    disableEnterKeyValidation: true,

    content: dialogOptions.contentMaker,
    ok: dialogOptions.save,
    cancel: dialogOptions.discard,

  });

  return {
    dialog: entryEditDialog,
    close: function () { entryEditDialog.destroy(); },
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE INPUT IN DIALOG
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeDialogInput (inputified, $container, childInputConfig, objectOptions) {

  if (!objectOptions) objectOptions = {};

  // help message
  var $helpMessage = $container.div({ class: "help-message", });
  var helpMessage = $$.result.call(inputified, objectOptions.help, $helpMessage);
  if (helpMessage) $helpMessage.htmlSanitized(helpMessage);
  if (!$helpMessage.html()) $helpMessage.hide();

  // make entry's object input
  inputified.entry_dialogInputified = inputified.inputify(childInputConfig);

};

//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//                                                  EDIT ENTRY
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

function editEntry (inputified) {

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GENERAL STATE VARIABLES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var entryInitialValueBeforeCreatingInputs = inputified.entryValue;
  var entryInitialValue; // this one could be slightly different than the previous one, because creating inputs could set some default values

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE DIALOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  var dialogCreator = inputified.options.entry.useCustomDialog || createEntryEditDialog;
  inputified.entry_editDialog = dialogCreator.call(inputified, {

    //
    //                              CONTENT MAKER

    contentMaker: function ($container) {

      // make child input config
      var commonChildInputConfig = {
        $container: $container,
        labelLayout: "hidden",
        ignoreInDeepKeyChain: true,
        storage: { parentInputified: inputified, },
        storageRecursive: $$.defaults({ rootInputified: inputified, }, inputified.options.storageRecursive),
        defaultValue: _.clone(inputified.entryValue), // use "defaultValue", because if use "value" it would replace existing keys that are not editable by structure
        // save initial value on creation (inputified.entryValue is not good, because maybe some input made a modification with a defaultValue, and this is not a real modification that should ask to confirm on canceling)
      };
      if (inputified.options.entry.customizedOptions) var childInputConfig = $$.defaults(commonChildInputConfig, inputified.options.entry.customizedOptions)
      else var childInputConfig = $$.defaults(commonChildInputConfig, { type: "object", object: inputified.options.object, });

      // add created and modified methods, queued to existing ones if there are some
      var previousCreatedMethod = childInputConfig.created;
      childInputConfig.created = function () {
        if (previousCreatedMethod) previousCreatedMethod.apply(this, arguments);
        entryInitialValue = inputified.internalGet();
      };
      var previousModifiedMethod = childInputConfig.modified;
      childInputConfig.modified = function () {
        if (previousModifiedMethod) previousModifiedMethod.apply(this, arguments);
        if (inputified.options.entry.saveToParentOnChange) inputified.entry_writeDialogInputValueToOriginInput(true);
      };

      // get object options and make child input (object options is a function)
      if (_.isFunction(childInputConfig.object)) childInputConfig.object(function (objectOptions) {
        childInputConfig.object = objectOptions;
        makeDialogInput(inputified, $container, childInputConfig, objectOptions);
      })
      // make child input (object options is an object)
      else makeDialogInput(inputified, $container, childInputConfig, childInputConfig.object);

    },

    //
    //                              SAVE CHANGES

    save: function () {
      inputified.entry_writeDialogInputValueToOriginInput();
      delete inputified.entry_editDialog;
      delete inputified.entry_dialogInputified;
    },

    //
    //                              DISCARD CHANGES

    discard: function () {
      var entryHasNotBeenModified = $$.isEqual(inputified.entry_dialogInputified.get(), entryInitialValue);
      if (entryHasNotBeenModified) return
      else if (!confirm("Discard the modifications you made?")) return false
      else if (inputified.options.entry.saveToParentOnChange) inputified.set(entryInitialValueBeforeCreatingInputs);
      delete inputified.entry_editDialog;
      delete inputified.entry_dialogInputified;
    },

    //                              ¬
    //

  });

  // add reverse edit dialog changes method to inputified
  inputified.entry_reverseEditDialogChanges = function () {
    if (!inputified.entry_dialogInputified) return $$.log.error("There doesn't seem to be any entry edit dialog currently open.");
    var entryHasNotBeenModified = $$.isEqual(inputified.entry_dialogInputified.get(), entryInitialValue);
    if (entryHasNotBeenModified) return
    else if (inputified.options.entry.saveToParentOnChange) inputified.set(entryInitialValueBeforeCreatingInputs);
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  make: function () {
    var inputified = this;

    if (!inputified.options.entry) inputified.options.entry = {};

    inputified.$entryPre = inputified.$input.pre();
    inputified.$input.div({ class: "icon-quill", });
    inputified.$input.click(function () { inputified.entry_openEditDialog(); });

    if (inputified.entryValue) inputified.set(inputified.entryValue);

    // support custom colors
    inputified.initializeColor({
      primary: { backgroundColor: ["&& > .intertitle"], },
      primaryContrast: { color: ["&& > .intertitle"], },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    var inputified = this;
    return inputified.entryValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiSet: function (processedValue, valueVerified) {
    var inputified = this;
    inputified.entryValue = valueVerified;

    var cleanedValue = $$.compact(inputified.internalGet({ objectResult: false, }), [undefined]);
    if (_.isObject(cleanedValue)) cleanedValue = $$.json.pretty(cleanedValue);
    inputified.$entryPre.text(cleanedValue || "");

    // if entry dialog is open, set it's input value
    if (inputified.entry_editDialog && inputified.entry_dialogInputified && !inputified.entry_isWritingDialogInputValueToOriginInput) inputified.entry_dialogInputified.set(inputified.entryValue);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  OPEN ENTRY EDIT DIALOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  entry_openEditDialog: function () {
    var inputified = this;
    return editEntry(inputified);
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CLOSE ENTRY EDIT DIALOG
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  entry_saveAndCloseEditDialog: function () {
    var inputified = this;
    if (!inputified.entry_writeDialogInputValueToOriginInput || !inputified.entry_editDialog) return $$.log.error("Cannot save and close edit dialog, could not find dialog.");
    inputified.entry_writeDialogInputValueToOriginInput();
    if (!inputified.entry_editDialog.destroy) return $$.log.error("The custom entry edit dialog you chose should return a 'destroy' function for the dialog to be closed programatically.");
    inputified.entry_editDialog.destroy();
    delete inputified.entry_editDialog;
    delete inputified.entry_dialogInputified;
  },

  entry_discardAndCloseEditDialog: function () {
    var inputified = this;
    if (!inputified.entry_reverseEditDialogChanges || !inputified.entry_editDialog) return $$.log.error("Cannot discard and close edit dialog, could not find dialog.");
    inputified.entry_reverseEditDialogChanges();
    if (!inputified.entry_editDialog.destroy) return $$.log.error("The custom entry edit dialog you chose should return a 'destroy' function for the dialog to be closed programatically.");
    inputified.entry_editDialog.destroy();
    delete inputified.entry_editDialog;
    delete inputified.entry_dialogInputified;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET VALUE TO PARENT
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  entry_writeDialogInputValueToOriginInput: function (calledByChanged) {
    var inputified = this;

    // this method can be executed on initialization of inputs and inputified.entry_dialogInputified doesn't yet exist, just ignore those calls
    if (!inputified.entry_dialogInputified) return;

    // check and alert if errors (but not if called from change event)
    if (!calledByChanged) {
      // mandatory missing
      if (inputified.entry_dialogInputified.$inputContainer.find(".mandatory_missing").length) {
        uify.alert.error("Some mandatory inputs haven't been properly defined.");
        return false;
      }
      // some invalid inputs
      else if (inputified.entry_dialogInputified.$inputContainer.find(".invalid").length) {
        uify.alert.error("Some inputs have invalid values, you must correct them to save changes.");
        return false;
      };
    };

    // everything ok, save
    inputified.entry_isWritingDialogInputValueToOriginInput = true;
    inputified.set(inputified.entry_dialogInputified.get());
    delete inputified.entry_isWritingDialogInputValueToOriginInput;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
