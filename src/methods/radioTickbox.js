var _ = require("underscore");
var $$ = require("squeak");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GENERATE ITEM
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: generate one item (one choice)
  ARGUMENTS: (
    !$items <yquerjObject>,
    !choice <{
      label: <string>,
      value: <any>,
    }>,
  ){@this=inputified}
  RETURN: item <{
    value: <any>,
    label: <string>,
    $element: <yquerjObject>,
  }>
*/
function generateItem ($items, choice) {

  var inputified = this;
  var radioTickboxOpts = inputified.options.radioTickbox;

  //
  //                              GROUP

  // TODO implement support for groups (and also for suggests)
  // if (choice.type == "group") {
  //   $items.div({ class: "group", htmlSanitized: choice.value, });
  //   return
  // };

  //
  //                              ITEM

  var item = {

    // store value and label
    value: choice.value,
    label: choice.label,

    // is item currently selected
    selected: false,

    // jquery dom element (create buttons)
    $element: $items.div({ class: "item inputify-unselected", }).click(function () {
      // always execute change in ui...
      if (inputified.hasSingleItem()) inputified.set(item.value) // could be optimized, but this works
      else {
        // toggle selected state
        item.selected = !item.selected;
        // set selected order (dummy if selected to make sure it will be last iterated in following lines)
        item.selectedOrder = item.selected ? 9999999 : -1;
        // iterate selected items to refresh them according to new situation
        _.chain(inputified.radioTickbox.items)
          .where({ selected: true, })
          .sortBy("selectedOrder")
          .each(function (iteratedItem, itemSelectedOrderIndex) {
            iteratedItem.selectedOrder = itemSelectedOrderIndex;
            iteratedItem.refreshItemUI();
          })
        ;
        // if item was unselected, refresh it's display (not necessary if selected since it's done in previous iteration of selected items)
        if (!item.selected) item.refreshItemUI();
      };
      // execute change function if asked
      inputified.changed();
    }),

    /**
      DESCRIPTION: refresh item UI according to it's object state
      ARGUMENTS: ( ø )
      RETURN: <void>
    */
    refreshItemUI: function () {

      if (item.selected) {
        item.$element.addClass("inputify-selected");
        item.$element.removeClass("inputify-unselected");
        if (item.$selectedOrderIndex) item.$selectedOrderIndex.text(item.selectedOrder);
      }
      else {
        item.$element.removeClass("inputify-selected");
        item.$element.addClass("inputify-unselected");
      };

    },


  };

  // GENERATE CHECK ICON
  if (radioTickboxOpts.iconChecked && !radioTickboxOpts.sortBySelectOrder) item.$element.div({ class: "radiotickbox-icon-checked icon-"+ radioTickboxOpts.iconChecked, });
  if (radioTickboxOpts.iconUnchecked) item.$element.div({ class: "radiotickbox-icon-unchecked icon-"+ radioTickboxOpts.iconUnchecked, });

  // ENTRY DISPLAY/TEXT
  if (radioTickboxOpts.customItemDisplay) radioTickboxOpts.customItemDisplay(item)
  else if (!radioTickboxOpts.noText) item.$text = item.$element.div({ class: "text", htmlSanitized: item.label, });

  // SELECTED ORDER INDEX
  if (radioTickboxOpts.sortBySelectOrder) item.$selectedOrderIndex = item.$element.div({ class: "selected-order-index", });

  return item;

  //                              ¬
  //

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeIsAsync: true,
  make: function (makeCallback) {

    var inputified = this;

    var radioTickboxOpts = inputified.getSpecificOptions("radioTickbox");

    // global radio, tickbox and orientation classes
    inputified.$input.addClass("inputify-radiotickbox inputify-radiotickbox_"+ radioTickboxOpts.orientation);

    // generate items container
    var $items = inputified.$input.div({ class: "items" });

    // MAKE APPROPRIATE CHOICES LIST
    inputified.interpretChoices(function (choices) {

      //
      //                              save choices

      inputified.calculatedChoices = choices;

      //
      //                              create all items, and the selected one

      inputified.radioTickbox = {
        items: _.map(inputified.calculatedChoices, function (choice) {
          return generateItem.call(inputified, $items, choice);
        }),
      };

      //
      //                              add color

      inputified.initializeColor({
        base: { backgroundColor: [".inputify-unselected"], },
        baseContrast: { color: [".inputify-unselected"], },
        primary: {
          backgroundColor: [".inputify-selected"],
          color: [".selected-order-index"],
        },
        primaryContrast: {
          color: [".inputify-selected"],
          backgroundColor: [".selected-order-index"],
        },
        hover: {
          backgroundColor: [".inputify-selected", ".inputify-unselected"],
          color: [".selected-order-index"],
        },
        hoverContrast: {
          color: [".inputify-selected", ".inputify-unselected"],
          backgroundColor: [".selected-order-index"],
        },
        // color to go back to when unhovering (do not need to be definer, because by default it's right)
        // selectedColor: "primary",
        // unselectedColor: "base",
      });

      //
      //                              make done

      makeCallback();

      //                              ¬
      //

    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGetRaw: function () {
    var inputified = this;
    // get list of selected elements
    var resultValue = _.chain(inputified.radioTickbox.items)
      .where({ selected: true, })
      .sortBy(inputified.options.radioTickbox.sortBySelectOrder ? "selectedOrder" : "")
      .pluck("value")
      .value()
    ;
    // return value (for radio) or array of values (for tickbox)
    if (inputified.hasSingleItem()) return resultValue[0]
    else return (_.isUndefined(resultValue) || !resultValue.length) ? undefined : resultValue;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  systemValueInVerify: function (value) {
    var inputified = this;
    return inputified.getAcceptedValues(value, inputified.radioTickbox.items);
  },

  uiSet: function (processedValue, verifiedValue) {
    var inputified = this;

    // make sure list of values is an array
    var vals = inputified.hasSingleItem() ? [verifiedValue] : verifiedValue;

    // iterate all items and either select them if they are set in values, else make sure they are unselected
    _.each(inputified.radioTickbox.items, function (item) {
      // figure out if item value is in list of selected values, and at which index
      var indexOfItemInValueToSet = _.findIndex(vals, function (val) { return _.isEqual(item.value, val); });
      // set selected and ordering
      item.selected = indexOfItemInValueToSet == -1 ? false : true;
      item.selectedOrder = indexOfItemInValueToSet;
      // refresh item UI according to new state
      item.refreshItemUI();
    });

    // set colors
    inputified.initializeColor();

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
