var _ = require("underscore");
var $$ = require("squeak");
var spectrum = require("./spectrum");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ADD PALETTE OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeOptionsWithPalette (spectrumOptions) {
  return $$.defaults({
    showPalette: true,
    // showPaletteOnly: true, // problem with validating saving input, needs to click "choose" but button is only on "more" panel
    togglePaletteOnly: true,
    togglePaletteMoreText: 'more',
    togglePaletteLessText: 'less',
    palette: [
      ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
      ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
      ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
      ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
      ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
      ["#cc0000","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
      ["#990000","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
      ["#660000","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
    ],
  }, spectrumOptions);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RANDOM BUTTON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function uiSetRandom (inputified) {
  inputified.$inputContainer.find(".sp-preview-inner").css("background-image", "linear-gradient(to right, #F00, #F0F, #00F, #0FF, #0F0)");
};
function uiResetColor (inputified) {
  inputified.$inputContainer.find(".sp-preview-inner").css("background-image", "")
};

function showRandomButton (inputified, $spectrumContainer, spectrumOptions) {

  $spectrumContainer.find(".sp-picker-container").div({
    class: "inputify-color-button_random",
    text: "random",
  }).click(function () {
    if (spectrumOptions.showRandomButton === "calculateLater") {
      inputified.spectrumSpecialColor = "random";
      uiSetRandom(inputified);
    }
    else {
      delete inputified.spectrumSpecialColor;
      if (spectrumOptions.showRandomButton === "randomTransparency") inputified.set($$.random.color())
      else {
        var colorObject = inputified.$colorInput.spectrum("get");
        inputified.set($$.random.color(colorObject._a));
      };
    };
    inputified.$colorInput.spectrum("hide");
    inputified.changed();
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  make: function () {

    var inputified = this;

    //
    //                              GET SPECTRUM OPTIONS

    var spectrumOptions = inputified.getSpecificOptions("spectrum");
    // custom option to add nice palette with more options
    if (spectrumOptions.showChoicesPalette) var spectrumOptions = makeOptionsWithPalette(spectrumOptions);

    //
    //                              MAKE INPUT

    // make input nested in container
    inputified.$colorInput = inputified.$input.input({ type: "color", });
    inputified.$colorInput.spectrum(spectrumOptions);
    // fire changed event only when choose button has been clicked (hide and change methods provided by spectrum don't work because if value hasn't been modified, it will not fire change, and if value was initally black and user wants black, it fires nothing)
    var $spectrumContainer = inputified.$colorInput.spectrum("container");
    $spectrumContainer.find(".sp-choose").click(function () {
      delete inputified.spectrumSpecialColor;
      uiResetColor(inputified);
      inputified.changed();
    });

    //
    //                              MAKE "random" BUTTON IF ASKED

    if (spectrumOptions.showRandomButton) showRandomButton(inputified, $spectrumContainer, spectrumOptions);

    //
    //                              SET DISPLAY COLOR

    inputified.initializeColor({
      base: { backgroundColor: [".sp-replacer"], }, // .sp-container
      baseContrast: { color: [".sp-replacer"], },
      primary: { color: [".sp-dd"], },
      hover: { backgroundColor: [".sp-replacer"], },
    });

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  uiGet: function () {
    var inputified = this;
    // return undefined
    if (inputified.spectrumSpecialColor === "undefined") return undefined
    // return random
    else if (inputified.spectrumSpecialColor === "random") return "random"
    // return color
    else {
      var colorObject = inputified.$colorInput.spectrum("get");
      if (colorObject._a == 1) return colorObject.toHexString()
      else return colorObject.toRgbString();
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // TODO verify that value is a proper color, but it's quite complicated and spectrum handles it quite nicely without seems
  valueSetVerify: function (processedValue) {
    var inputified = this;
    // custom case if value to define is undefined, add undefined support to spectrum
    if (_.isUndefined(processedValue)) inputified.spectrumSpecialColor = "undefined"
    // custom case if value to define is undefined, add random support to spectrum
    else if (processedValue === "random") inputified.spectrumSpecialColor = "random"
    else delete inputified.spectrumSpecialColor;
  },

  uiSet: function (processedValue) {
    var inputified = this;
    // set ui if random
    if (processedValue === "random") uiSetRandom(inputified)
    // set ui otherwise
    else {
      uiResetColor(inputified);
      inputified.$colorInput.spectrum("set", processedValue);
    };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
}
