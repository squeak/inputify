var _ = require("underscore");
var $$ = require("squeak");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: from the passed options, make the full options, getting defaults from type...
  ARGUMENTS: ( !inputOptions <inputify·option> )
  RETURN: <inputify·options>
*/
module.exports = function (inputOptions) {
  var inputify = this;

  // ACCEPT MULTIPLE NAMES FOR THE SAME OPTIONS
  var translatedInputOptions = _.clone(inputOptions);
  if (translatedInputOptions.key) translatedInputOptions.name = translatedInputOptions.key;
  if (translatedInputOptions.inputifyType) translatedInputOptions.type = translatedInputOptions.inputifyType;

  // MAKE SURE TYPE IS VALID
  if (!inputify.inputsList[translatedInputOptions.type]) {
    $$.log.detailedError(
      "inputify",
      "Could not find inputTypeOptions for type: "+ translatedInputOptions.type,
      "input name is: "+ translatedInputOptions.name,
      "Default input (normal) will be used."
    );
    translatedInputOptions.type = inputify.defaults.type;
  };

  // GET INPUT MODEL FROM inputs/[inputType].js (or quasi)
  var inputTypeOptions = inputify.inputsList[translatedInputOptions.type];

  // MAKE OBJECT WITH ALL OPTIONS
  // var options = _.create(inputTypeOptions, $$.defaults(inputify.defaults, translatedInputOptions)); // could be nice to pass input's options as prototype, but it doesn't seem to work
  var systemOptions = $$.defaults(inputTypeOptions, inputify.defaults);
  var options = $$.defaults(
    systemOptions,
    // ADD FROM storageRecursive OPTIONS (BOTH SYSTEM AND PASSED ONES)
    $$.getValue(systemOptions, "storageRecursive.options"),
    $$.getValue(translatedInputOptions, "storageRecursive.options"),
    translatedInputOptions
  );

  // MAKE THE FULL LIST OF LABEL BUTTONS ASKED BY ALL LEVELS
  options.allLabelButtons = _.compact(_.flatten([inputTypeOptions.labelButtons, inputify.defaults.labelButtons, translatedInputOptions.labelButtons], true));

  // RETURN ALL OPTIONS
  return options;

};
