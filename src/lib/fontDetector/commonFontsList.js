
module.exports = [
  // simples
  "Arial", "Helvetica", "Times", "Times New Roman",
  "Calibri", "Century Gothic", "Comic Sans", "Consolas", "Dejavu Sans", "Dejavu Serif",
  "Georgia", "Gill Sans", "Lucida Sans", "Open Sans", "Palatino", "Tahoma", "Trebuchet", "Verdana", "Zapfino",
  "Impact", // macos
  // Courrier
  "Courier", // macos
  "Courier New",
  // Futura
  "Futura Std",
  // DejaVu
  "DejaVu Sans Light",
  // Akzidenz-Grotesk
  "Akzidenz-Grotesk Extended BQ",
  // SourceSansPro
  "SourceSansProBold", "SourceSansProLight", "SourceSansProRegular",
  // MyriadPro
  "Myriad Pro", "MyriadProBold", "MyriadProRegular",
  // CrimsonRoman
  "CrimsonRoman",
  // Lato
  // "Lato", "Lato-Light", "Lato-Bold",
  "Lato", "Lato Regular", "Lato Light", "Lato Bold",
  // LiberationMono
  "LiberationMono-Bold", "LiberationMono-BoldItalic", "LiberationMono",
  // ZillaSlab
  // "ZillaSlab-Light", "ZillaSlab", "ZillaSlab-Bold", "ZillaSlabHighlight", "ZillaSlabHighlight-Bold",
  "Zilla Slab Light", "Zilla Slab Regular", "Zilla Slab Bold", "Zilla Slab Highlight Regular", "Zilla Slab Highlight Bold",
];
