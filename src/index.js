var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var utilities = require("./utilities");

var inputify = $$.scopeFunction({
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAIN
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  main: function (inputOptions) {

    //
    //                              GET ALL OPTIONS AND CHECK CONTAINER

    var options = inputify.makeFullOptions(inputOptions);

    // MAKE SURE CONTAINER IS AN YQUERJ OBJECT
    options.$container = $(options.$container);

    // CHECK MANDATORY KEYS
    $$.mandatory(options, {
      $container: $$.isJqueryElement,
    });

    //
    //                              CREATE INPUTIFIED OBJECT, ATTACH METHODS TO IT (get, set, make...), ATTACH OPTIONS TO IT

    var inputified = $$.methods({
      isInputified: true,
      inputify: inputify, // global inputify function passed here to be able to use it for example in inputs/objects.js without causing circular dependency
      // whenReadyQueue: [],
      // whenReady: function (functionToExecuteWhenInputIsReady) {
      //   if (!_.isFunction(functionToExecuteWhenInputIsReady)) return $$.log.error("The function you passed to execute when input is ready is not a function.", functionToExecuteWhenInputIsReady);
      //   if (inputified.isReady === true) functionToExecuteWhenInputIsReady.call(inputified)
      //   else inputified.whenReadyQueue.push(functionToExecuteWhenInputIsReady);
      // },
    }, $$.defaults(
      options.methods,
      utilities,
      { _recursiveOption: false, } // make sure inputified inputs are not considered recursible, because when they are attached to storage, they should be shallow copied (also otherwise it would create infinite loops when created default options for subinputs)
    ));
    inputified.attachOptionsToInputified(options);

    //
    //                              INITIALIZE INPUT

    inputified.$inputContainer = inputified.options.$container.div();
    inputified.initialize(function () {

      // executed input creation callback
      if (inputified.options.created) inputified.options.created.call(inputified);
      // _.each(inputified.whenReadyQueue, function (functionToExecuteWhenReady) { functionToExecuteWhenReady.call(inputified); });
      // inputified.isReady = true;
      // if activate, should document "isReady", "whenReady" and "whenReadyQueue" in wiki

    });

    //
    //                              RETURN

    return inputified;

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  defaults: require("./defaultOptions"),
  extendDefaults: function (additionalDefaults) {
    return inputify.defaults = $$.defaults(inputify.defaults, additionalDefaults);
  },
  // get: function (input) {
  //   var $input = $(input);
  //   inputs[$input.attr("inputify-type")].get($input);
  // },
  inputsList: require("./inputs"),
  makeFullOptions: require("./optionsMaker"),


  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  INPUTIFY CORE SETTINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  settings: {},

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
});

module.exports = inputify;

// make inputify globally accessible (to remove someday TODO TODO TODO)
window.inputify = inputify;
