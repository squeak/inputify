var radioTickboxMethods = require("../methods/radioTickbox");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              TICKBOX

  tickbox: {
    description: "Choose one or multiple options from the given ones.",
    specificOpts: ["radioTickbox", "choices"],
    tag: "div",
    prepareValue: [],
    radioTickbox: {
      _recursiveOption: true,
      iconChecked: "checkmark3", // checkmark checkmark5 checkmark2 check-circle
      orientation: "horizontal",
      type: "tickbox",
    },
    methods: radioTickboxMethods,
  },

  //
  //                              RADIO

  radio: {
    description: "Choose a single option from the given ones.",
    specificOpts: ["radioTickbox", "choices"],
    tag: "div",
    radioTickbox: {
      _recursiveOption: true,
      iconChecked: "radio-checked",
      orientation: "horizontal",
      type: "radio",
    },
    methods: radioTickboxMethods,
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
