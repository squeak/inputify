var sliderMethods = require("../methods/sliders");


module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              SLIDER

  slider: {
    description: "An input with a handle sliding left and right to set a number value.",
    specificOpts: ["slider"],
    tag: "div",
    methods: sliderMethods,
    slider: {
      _recursiveOption: true,
      orientation: "horizontal",
      start: 0,
      tooltips: [true],
      connect: true, // color from start to slider position
      range: {
        "min": 0,
        "max": 1
      },
    },
  },

  //
  //                              RANGE

  range: {
    description: "An input with two handles sliding left and right to set two number values.",
    specificOpts: ["slider"],
    tag: "div",
    methods: sliderMethods,
    slider: {
      _recursiveOption: true,
      multiSlider: true,
      orientation: "horizontal",
      start: [0, 0], // "start" value defines the ammount of sliders there will be
      tooltips: true,
      connect: true, // color the range
      range: {
        "min": 0,
        "max": 1
      },
    },
  },

  //
  //                              VERTICAL SLIDER

  verticalSlider: {
    description: "An input with a handle sliding up and down to set a number value.",
    specificOpts: ["slider"],
    tag: "div",
    methods: sliderMethods,
    slider: {
      _recursiveOption: true,
      orientation: "vertical",
      direction: "rtl",
      start: 0,
      tooltips: [true],
      connect: [true, false], // color from start to slider position
      range: {
        "min": 0,
        "max": 1
      },
    },
  },

  //
  //                              VERTICAL RANGE

  verticalRange: {
    description: "An input with two handle sliding up and down to set two number values.",
    specificOpts: ["slider"],
    tag: "div",
    methods: sliderMethods,
    slider: {
      _recursiveOption: true,
      orientation: "vertical",
      direction: "rtl",
      multiSlider: true,
      start: [0, 0], // "start" value defines the ammount of sliders there will be
      connect: true, // color the range
      tooltips: true,
      range: {
        "min": 0,
        "max": 1
      },
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
