var timeMethods = require("../methods/time");
var objectMethods = require("../methods/object");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              TIME

  time: {
    description: "Set a time.",
    specificOpts: ["time"],
    tag: "div",
    methods: timeMethods,

    // convert from "hh:mm:ss" to { hours: "hh", minutes: "mm", seconds: "ss", }
    systemValueInProcess: function (value) {
      var inputified = this;
      var timeArray = (value || "").split(":");
      var timeObject = {};
      _.each(inputified.timeOptions.parts, function (partName, partIndex) {
        timeObject[partName] = timeArray[partIndex] || undefined;
      });
      return timeObject;
    },

    // convert from { hours: "hh", minutes: "mm", seconds: "ss", } to "hh:mm:ss"
    systemValueOutProcess: function (processedValue) {
      var inputified = this;
      if (_.isEmpty(processedValue)) return
      else {
        var resultArray = _.map(inputified.timeOptions.parts, function (part) { return processedValue[part] || "00"; });
        if (_.indexOf(resultArray, undefined) !== -1) return ""
        else return resultArray.join(":");
      };
    },

  },

  //
  //                              TIME RANGE

  timeRange: {
    description: "Set a time range.",
    specificOpts: ["time", "object"],
    tag: "div",
    methods: objectMethods,
    object: {
      _recursiveOption: true,
      shape: "object",
      passParentOptionsToChildrenModel: ["time"],
      structure: [
        { name: "start", type: "time", },
        { name: "end", type: "time", },
      ],
    },
    systemValueInProcess: function (value) {
      var valueArray = (value || "").split("#");
      return {
        start: valueArray[0] || undefined,
        end: valueArray[1] || undefined,
      };
    },
    systemValueOutProcess: function (processedValue) {
      var inputified = this;
      if (_.isObject(processedValue)) {
        var defaultTime = _.map(inputified.options.parts || ["hours", "minutes"], function () { return "00" }).join(":");
        return (processedValue.start || defaultTime) +"#"+ (processedValue.end || defaultTime);
      };
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
