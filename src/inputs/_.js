
/**
  DESCRIPTION: default input
  TYPES: {
    !tag: <string>,
    ?labelLayout: <"inline"|"stacked">,
    ?methods: <{
      make: <function(ø)>,
      get: <function(ø)>,
      set: <function( value <any> ),
    }>,
    ?ionic: <{
      tag: <string>,
    }>,
  }
*/
module.exports = {

  description: "MISSING DESCRIPTION",
  tag: "input",
  labelLayout: "inline",
  methods: require("../methods/_"),

}
