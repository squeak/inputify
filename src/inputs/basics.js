var _ = require("underscore");
var $$ = require("squeak");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              NORMAL

  normal: {
    description: "Simple standard input, without any further functionality.",
    specificOpts: ["input"],
    input: {
      _recursiveOption: true,
      type: "",
    },
  },

  //
  //                              PASSWORD

  password: {
    description: "An input for passwords, it's content is hidden.",
    specificOpts: ["input"],
    input: {
      _recursiveOption: true,
      type: "password",
    },
  },

  //
  //                              NUMBER

  number: {
    description: "An input restricted to numbers.",
    specificOpts: ["input"],
    systemValueOutVerify: function (processedValue) {
      if (_.isUndefined(processedValue)) return undefined
      else if (!_.isNaN(+processedValue)) return +processedValue
      else throw "Value is not a number";
    },
    // input: { type: "number", }, // NOTE: not using "number" type input because it makes it impossible to know in systemValueOutVerify what value is set when value is not a proper number
  },

  //
  //                              URL

  url: {
    description: "An url input.",
    specificOpts: ["input"],
    systemValueOutVerify: function (val) {
      // try to convert val to url, it will throw an error if it fails
      try {
        new URL(val);
        return val;
      }
      catch (e) {
        if (!val) return
        else throw "this is not a valid url";
      };
    },
    input: {
      _recursiveOption: true,
      type: "url",
    },
  },

  //
  //                              SEARCH

  search: {
    description: "Search something.",
    specificOpts: ["input"],
    input: {
      _recursiveOption: true,
      type: "search",
    },
  },

  //
  //                              EMAIL

  email: {
    description: "An input that content must be an email address.",
    specificOpts: ["input"],
    input: {
      _recursiveOption: true,
      type: "email",
    },
  },

  //
  //                              PHONE

  phone: {
    description: "An input that content must be a phone number.",
    specificOpts: ["input"],
    input: {
      _recursiveOption: true,
      type: "tel",
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
