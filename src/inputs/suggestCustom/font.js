var _ = require("underscore");
var $$ = require("squeak");
var suggestMethods = require("../../methods/suggest");

var fontDetector = require("../../lib/fontDetector");
var commonFontsList = require("../../lib/fontDetector/commonFontsList");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET FONTS LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var listOfAllFonts;
function getFontsList (done) {

  // GENERATE LIST OF FONTS IF FIRST TIME
  if (!listOfAllFonts) listOfAllFonts = _.filter(commonFontsList, function (fontName) {
    return fontDetector.detect(fontName);
  })

  // CALLBACK THE LIST OF FONTS
  var choices = _.clone(listOfAllFonts);
  done(choices);

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate inputify options to suggest fonts
  ARGUMENTS: (
    !description <string>,
    ?suggestOptions <object>,
  )
  RETURN: inputifyPreoptions
*/
module.exports = function (description, suggestOptions) {

  return {
    description: description,
    specificOpts: ["suggest", "choices"],
    tag: "div",
    methods: suggestMethods,
    choices: getFontsList,
    suggest: $$.defaults({
      _recursiveOption: true,
      addItems: false,
      valueToLabel: function (fontName) {
        return '<span style="font-family:'+ fontName +';">'+ fontName +'</span>';
      },
    }, suggestOptions),
  };

};
