var _ = require("underscore");
var $$ = require("squeak");
var suggestMethods = require("../../methods/suggest");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate inputify options to suggest paths
  ARGUMENTS: (
    !description <string>,
    ?suggestOptions <object>,
  )
  RETURN: inputifyPreoptions
*/
module.exports = function (description, suggestOptions) {

  return {
    description: description,
    specificOpts: ["suggest", "choices"],
    tag: "div",
    methods: suggestMethods,
    suggest: $$.defaults({
      _recursiveOption: true,
      addItems: true,
      labelToValue: function (path) {
        if ($$.isAbsoluteFolderPath(path)) return path
        else throw "The value you inputed is not a proper path";
      },
    }, suggestOptions),
  };

};
