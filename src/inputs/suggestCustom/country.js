var _ = require("underscore");
var $$ = require("squeak");
var suggestMethods = require("../../methods/suggest");

var flagify = require("uify/plugin/flagify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET COUNTRIES LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var listOfAllCountries;
function getCountriesList (done) {

  // GENERATE LIST OF COUNTRIES IF FIRST TIME
  if (!listOfAllCountries) listOfAllCountries = _.chain(flagify.list).filter(function (countryObject) {
      return countryObject.Country_name;
    }).map(function (countryObject) {
      return {
        _isChoice: true,
        label: makeIconChoiceHtml(countryObject),
        search: countryObject.search,
        value: countryObject.Country_name,
      };
    }).value()

  // CALLBACK THE LIST OF COUNTRIES
  var choices = _.clone(listOfAllCountries);
  done(choices);

};

function makeIconChoiceHtml (countryObject) {
  if (countryObject) return '<div class="inputify-country-item"><span class="avatar uify-flag uify-flag-'+ countryObject.Code +'"></span>'+ countryObject.Country_name +'</div>'
  else "";
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate inputify options to suggest countries
  ARGUMENTS: (
    !description <string>,
    ?suggestOptions <object>,
  )
  RETURN: inputifyPreoptions
*/
module.exports = function (description, suggestOptions) {

  return {
    description: description,
    specificOpts: ["suggest"],
    tag: "div",
    methods: suggestMethods,
    choices: getCountriesList,
    suggest: $$.defaults({
      _recursiveOption: true,
      addItems: false,
      valueToLabel: function (countryName) {
        return makeIconChoiceHtml(flagify.getCountryObject(countryName));
      },
      lazyLoad: true,
    }, suggestOptions),
  };

};
