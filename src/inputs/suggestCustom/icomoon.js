var _ = require("underscore");
var $$ = require("squeak");
var suggestMethods = require("../../methods/suggest");
var icomoonIconsList = require("stylectrode/icomoon/selection.json").icons;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET ICONS LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var listOfAllIcomoonIcons;
function getIcomoonsIconsList (done) {

  // GENERATE LIST OF ICOMOON ICONS IF FIRST TIME
  if (!listOfAllIcomoonIcons) {
    var allIconsChoices = [];
    _.each(icomoonIconsList, function (iconObject) {
      var iconNames = iconObject.properties.name.split(/\s*,\s*/);

      allIconsChoices.push({
        _isChoice: true,
        label: makeIconChoiceHtml(iconNames[0]),
        search: iconObject.properties.ligatures +" "+ iconObject.properties.name, // +" "+ iconObject.icon.tags.join(),
        value: iconNames[0],
      });

    });
    listOfAllIcomoonIcons = allIconsChoices;
  }

  // CALLBACK THE LIST OF ICOMOON ICONS
  var choices = _.clone(listOfAllIcomoonIcons);
  done(choices);

};

function makeIconChoiceHtml (name) {
  return '<div class="inputify-icon-item"><span class="avatar icon-'+ name +'"></span><span class="text">'+ name +'</span></div>';
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: generate inputify options to suggest icomoon icons
  ARGUMENTS: (
    !description <string>,
    ?suggestOptions <object>,
  )
  RETURN: inputifyPreoptions
*/
module.exports = function (description, suggestOptions) {

  return {
    description: description,
    specificOpts: ["suggest", "choices"],
    tag: "div",
    methods: suggestMethods,
    choices: getIcomoonsIconsList,
    suggest: $$.defaults({
      _recursiveOption: true,
      addItems: false,
      valueToLabel: makeIconChoiceHtml,
      lazyLoad: true,
    }, suggestOptions),
  };

};
