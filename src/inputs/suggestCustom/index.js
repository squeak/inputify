var _ = require("underscore");
var $$ = require("squeak");

var makeFontsInputifyOptions = require("./font");
var makePathsInputifyOptions = require("./path");
var makeCountriesInputifyOptions = require("./country");
var makeIcomoonInputifyOptions = require("./icomoon");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = {

  //
  //                              PATH

  path: makePathsInputifyOptions("Input allowing to set only path like value.", { maxItemCount: 1, }),
  paths: makePathsInputifyOptions("Input allowing to set multiple paths."),

  //
  //                              FONTS

  font: makeFontsInputifyOptions("An input with autosuggestion of fonts names.", { maxItemCount: 1, }),
  fonts: makeFontsInputifyOptions("An input with autosuggestion of fonts names allowing selecting multiple fonts."),

  //
  //                              COUNTRIES

  country: makeCountriesInputifyOptions("An input with autosuggestion of countries names.", { maxItemCount: 1, }),
  countries: makeCountriesInputifyOptions("An input with autosuggestion of countries names allowing selecting multiple countries."),

  //
  //                              ICOMOON ICONS

  icon: makeIcomoonInputifyOptions("Choose an icon.", { maxItemCount: 1, }),
  icons: makeIcomoonInputifyOptions("Choose multiple icons."),
  icon_names: makeIcomoonInputifyOptions("Choose an icon (in a list with icons and names).", { maxItemCount: 1, }),
  icons_names: makeIcomoonInputifyOptions("Choose multiple icons (in a list with icons and names)."),

  //                              ¬
  //

};
