var _ = require("underscore");
var $$ = require("squeak");
var textareaMethods = require("../methods/textarea");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  json: {
    description: "A long text input that will be interpreted as JSON object.",
    tag: "textarea",
    methods: textareaMethods,
    systemValueOutVerify: function (processedValue) {
      if (_.isUndefined(processedValue) || $$.isJson(processedValue)) return processedValue
      else throw "The string you've inputed is not a proper JSON string.";
    },
    systemValueInProcess: function (value) {
      return $$.json.pretty(value);
    },
    systemValueOutProcess: function (processedValue) {
      if (_.isUndefined(processedValue)) return processedValue
      else return JSON.parse(processedValue);
    },
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
