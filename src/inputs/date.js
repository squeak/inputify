var dateMethods = require("../methods/date");
var objectMethods = require("../methods/object");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              DATE OR RANGE

  dateOrRange: {
    description: "A date picker.",
    specificOpts: ["date"],
    methods: dateMethods,
    date: {
      _recursiveOption: true,
    },
  },

  //
  //                              DATE

  date: {
    description: "A date picker.",
    specificOpts: ["date"],
    methods: dateMethods,
    date: {
      _recursiveOption: true,
      precisionChoiceDisable: true,
      rangeChoiceDisable: true,
    },
  },

  //
  //                              DATE RANGE

  dateRange: {
    description: "Choose a date range.",
    specificOpts: ["date"],
    methods: dateMethods,
    date: {
      _recursiveOption: true,
      rangePicker: true,
      precisionChoiceDisable: true,
      rangeChoiceDisable: true,
    },
  },

  //
  //                              DATE AND TIME

  dateTime: {
    description: "Choose a date and time.",
    specificOpts: ["date"],
    methods: dateMethods,
    date: {
      _recursiveOption: true,
      precision: "time",
      precisionChoiceDisable: true,
      rangeChoiceDisable: true,
    },
  },

  //
  //                              DATE AND TIME RANGE

  dateTimeRange: {
    description: "Choose a date and time range.",
    specificOpts: ["date"],
    methods: dateMethods,
    date: {
      _recursiveOption: true,
      rangePicker: true,
      precision: "time",
      precisionChoiceDisable: true,
      rangeChoiceDisable: true,
    },
  },

  //
  //                              DATES

  dates: {
    description: "An input allowing to set multiple and complex dates/ranges in a reduced string format.",
    specificOpts: ["date", "object"],
    tag: "div",
    methods: objectMethods,
    object: {
      _recursiveOption: true,
      shape: "array",
      passParentOptionsToChildrenModel: ["date"],
      model: {
        type: "dateOrRange",
      },
    },
    systemValueInProcess: function (value) {
      var squeakDates = $$.squeakDates(value);
      if (squeakDates) return _.map(squeakDates.dates, function (squeakDate) { return squeakDate.getString(); });
    },
    systemValueOutProcess: function (processedValue) {
      var squeakDates = $$.squeakDates(processedValue);
      if (squeakDates) return squeakDates.getString();
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
