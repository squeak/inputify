var colorMethods = require("../methods/color");
var objectMethods = require("../methods/object");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              COLOR

  color: {
    description: "A color picker.",
    specificOpts: ["spectrum"],
    tag: "div",
    methods: colorMethods,
    spectrum: {
      _recursiveOption: true,
      clickoutFiresChange: false,
      showAlpha: true,
      allowEmpty: true,
    },
  },

  //
  //                              COLORS

  colors: {
    description: "A color picker to select multiple colors.",
    specificOpts: ["spectrum", "object"],
    tag: "div",
    methods: objectMethods,
    object: {
      shape: "array",
      passParentOptionsToChildrenModel: ["spectrum"],
      model: {
        type: "color",
      },
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
