var suggestMethods = require("../methods/suggest");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              SIMPLE

  simple: {
    description: "Simple input with autosuggestion, allowing to create new values as well.",
    specificOpts: ["suggest", "choices"],
    tag: "div",
    methods: suggestMethods,
    suggest: {
      maxItemCount: 1,
      addItems: true,
    },
  },

  //
  //                              MULTI

  multi: {
    description: "Multiple entries input with autosuggestion, allowing to create new values as well.",
    specificOpts: ["suggest", "choices"],
    tag: "div",
    suggest: {
      addItems: true,
    },
    methods: suggestMethods,
  },

  //
  //                              SELECT

  select: {
    description: "Simple input with autosuggestion, that doesn't allow to create new values.",
    specificOpts: ["suggest", "choices"],
    tag: "div",
    suggest: {
      maxItemCount: 1,
      addItems: false,
    },
    methods: suggestMethods,
  },

  //
  //                              SUGGEST

  suggest: {
    description: "Multiple entries input with autosuggestion, that doesn't allow to create new values.",
    specificOpts: ["suggest", "choices"],
    tag: "div",
    methods: suggestMethods,
    suggest: {
      addItems: false,
    },
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
