var objectMethods = require("../methods/object");
var entryMethods = require("../methods/entry");

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              ENTRY

  entry: {
    description: "An input containing other inputs organnized by key/value to edit in it's own space.",
    specificOpts: ["entry", "object"],
    tag: "div",
    methods: entryMethods,
  },

  //
  //                              OBJECT

  object: {
    description: "An input containing other inputs organnized by key/value.",
    specificOpts: ["object"],
    tag: "div",
    object: {
      _recursiveOption: true,
      shape: "object",
    },
    methods: objectMethods,
  },

  //
  //                              ARRAY

  array: {
    description: "An input containing a list of other inputs.",
    specificOpts: ["object"],
    tag: "div",
    object: {
      _recursiveOption: true,
      shape: "array",
    },
    methods: objectMethods,
  },

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
