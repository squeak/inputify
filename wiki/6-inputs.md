# All available inputs

The full demo/list of all available inputs is available [here](?demo#inputs).

## Default options

You can customize global options default values by passing your own custom defaults:
```js
inputify.defaults = { $container: $("#all-inputified-will-be-in-this-container") }
```
If you don't want to overwrite default options but extend them use:
```js
inputify.extendDefaults({ $container: $("#all-inputified-will-be-in-this-container") })
```

## Library Organization

##### Inputs:
Inputs are listed in files in the `src/inputs/` directory, every file contains a list of input types.
The default options (specified in `inputs/_.js`) are applied to all types.

##### Methods:
Custom methods are stored in the `src/methods/` directory.
Custom methods must be imported in inputs options (see default input options in `src/inputs/_.js` for an example).
Default methods stored in `src/methods/_.js` are applied to all inputs for which no custom method has been specified.

## How to make a new custom method

> TODO: write a full explanation

The following methods are used to define how to make, get and set values in inputs:
```javascript
/**
  {
    makeIsAsync: <boolean>@default=false « if true, make will be passed a callback function that it should run »,
    make: <function(?callback<function(ø)>){@this=inputified}> «
      function explaining how to create the input display
      callback will be defined and should be ran if makeIsAsync is set to true
    »,
    uiGet: <function(ø){@this=inputified}> « return the value as it's set in the UI »,
    uiSet: <function(
      processedValue <any>,
      verifiedValue <any>,
      opts<see utilities/general:set function opts argument>
    ){@this=inputified}> « set the processed value to the UI »,
    ?systemValueOutVerify: <function(processedValue){@this=inputified}:processedValueVerified> «
      verify value before processing it out,
      if this throws an error, will return undefined instead,
      the return of this function will be the processedValue passed to processing out function
    »,
    ?systemValueInVerify: <function(value){@this=inputified}:valueVerified> «
      verify value before processing it in, if this throws an error,
      will set undefined instead,
      the return of this function will be the value passed to processing in function
      »,
    ?valueSetVerify: <function(processedValue){@this=inputified}> «
      verify the processed value before setting it to UI,
      if this throws an error, will set undefined instead
    »,
  }
*/
```
In all those methods, `this` is inputified, the input object (the one that inputify main function returns).
