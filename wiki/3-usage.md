# Usage

Load and use inputify library with `inputify(options)`.  

Or in more details:
```javascript
  inputify({
    $container: $("body"),
    type: "slider",
    name: "volume",
    labelLayout: "hidden",
    changed: function (value) {
      console.log("Volume slider value was changed to: "+ value);
    },
    slider: {
      step: 2,
      range: {
        "min": 0,
        "max": 100
      },
    },
    // ... check out the whole list of options in 'All options' page
  })
```
