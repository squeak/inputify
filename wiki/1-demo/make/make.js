require("squeak/extension/string");
var inputify = require("../../../src");

window.demoStart = function (utilities) {

  utilities.section({
    title: "Make your own customized input",
    demo: function ($container) {

      //
      //                              CREATE TYPE AND OPTIONS INPUTS

      // create type chooser inputify
      var typeInputify = inputify({
        $container: $container,
        name: "input type",
        labelLayout: "intertitle",
        help: "Choose the type of input you want to create.",
        type: "select",
        choices: _.keys(inputify.inputsList),
        modified: function (value) {
          if (optionsInputify) createResultInputify(value, optionsInputify.get());
        },
      });

      var optionsInputify = inputify({
        $container: $container,
        name: "additional options",
        labelLayout: "intertitle",
        type: "json",
        help: "Choose any custom options for this input.\nFor details on possible options, see 'All options' page.",
        choices: _.keys(inputify.inputsList),
        modified: function (value) {
          createResultInputify(typeInputify.get(), value);
        },
      });

      //
      //                              RESULT INPUTIFY

      var $resultInputify = $container.div({ class: "result-inputify", });

      function createResultInputify (type, additionalOptions) {
        if ($resultValue) $resultValue.empty();
        $resultInputify.empty();
        inputify($$.defaults(
          additionalOptions,
          {
            $container: $resultInputify,
            name: "result input ("+ type +")",
            help: "This is the resulting input, created from the input type you chose and the additional options you entered.",
            labelLayout: "intertitle",
            type: type,
            changed: function (value) {
              $resultValue.htmlSanitized($$.string.htmlify($$.json.pretty(value)));
            },
          }
        ));
      };

      //
      //                              RESULT VALUE

      $container.div({ class: "result-title", text: "RESULT VALUE", });
      var $resultValue = $container.div({ class: "result-value", });

      //
      //                              SET A RANDOM TYPE ON START

      typeInputify.set($$.random.entry(_.keys(inputify.inputsList)));

      //                              ¬
      //

    },

  });

};
