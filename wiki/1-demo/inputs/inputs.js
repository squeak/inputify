var inputify = require("../../../src");

window.demoStart = function (utilities) {

  utilities.section({
    title: "List of possible inputs",
    demo: function ($container) {

      var $allInputs = $container.div({
        class: "all_inputs",
      });

      _.each(inputify.inputsList, function (input, inputName) {

        var $oneInput = $allInputs.div({
          class: "one_input",
        });
        $oneInput.div({
          class: "title",
          html: inputName,
        });

        $oneInput.div({
          class: "description",
          html: input.description,
        });

        inputify({
          $container: $oneInput,
          // help: input.description,
          // labelLayout: "hidden",
          name: inputName,
          type: inputName,
          changed: function (value) {
            console.log(value);
          },
        });

      });

    },

  });

};
