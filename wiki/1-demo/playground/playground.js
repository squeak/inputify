window.inputify = require("../../../src");

window.demoStart = function (utilities) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              TITLE

  $app.div({
    class: "main-title",
    text: "Some inputify inputs examples",
  });

  //
  //                              DEFAULT OPTIONS TO APPLY TO ALL INPUTS

  inputify.extendDefaults({
    // color: {
    //   primary: "purple",
    //   primaryContrast: "white",
    //   // hover: "",
    //   // hoverContrast: "",
    //   hover: "#0F0",
    //   hoverContrast: "black",
    //   // base: "rgba(50, 0, 0, 0.5)",
    //   base: "#444",
    //   baseContrast: "white",
    // },
    changed: function (value) { console.log(value); },
  });

  //
  //                              CREATE ALL INPUTS

  var inputsCategories = {
    basics: require("./inputs/basics"),
    boolean: require("./inputs/boolean"),
    color: require("./inputs/color"),
    date: require("./inputs/date"),
    files: require("./inputs/files"),
    json: require("./inputs/json"),
    number: require("./inputs/number"),
    objects: require("./inputs/objects"),
    radioTickbox: require("./inputs/radioTickbox"),
    sliders: require("./inputs/sliders"),
    suggest: require("./inputs/suggest"),
    suggestCustom: require("./inputs/suggestCustom"),
    sortableList: require("./inputs/sortableList"),
  };

  _.each(inputsCategories, function (inputsDemo, inputsCategoryName) {

    var $category = $app.div({
      class: "category",
    });

    $category.div({
      class: "title",
      html: inputsCategoryName,
    });

    inputsDemo($category);

  });

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
