
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "normal",
    type: "normal",
    value: "some value",
  });

  inputify({
    $container: $category,
    name: "password",
    type: "password",
    value: "some password",
  });

  inputify({
    $container: $category,
    name: "long",
    type: "long",
    value: "some \nlong text to edit in dialog",
  });

  inputify({
    $container: $category,
    name: "textarea",
    type: "textarea",
    value: "some \nlong text",
  });

  inputify({
    $container: $category,
    name: "any",
    type: "any",
    value: {
      yolo: 1,
      stringof: "json",
    },
    // value: '{ "yolo": 1, "stringof": "json" }',
  });

};
