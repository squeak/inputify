

module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "radio",
    type: "radio",
    value: "something else",
    choices: ["possibility 1", "possibility 2", "possibility 3", "something else"],
  });

  inputify({
    $container: $category,
    name: "tickbox",
    type: "tickbox",
    value: ["possibility 2", "possibility 3"],
    choices: ["possibility 1", "possibility 2", "possibility 3", "something else"],
  });

  inputify({
    $container: $category,
    name: "tickbox-sorted",
    type: "tickbox",
    radioTickbox: {
      sortBySelectOrder: true,
    },
    choices: ["possibility 1", "possibility 2", "possibility 3", "something else"],
  });

};
