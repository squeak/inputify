
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "date/range",
    type: "date",
    value: "2422-09-09",
  });

  inputify({
    $container: $category,
    name: "date!",
    type: "date",
    date: {
      drops: "up",
      opens: "center",
      precision: "date",
      rangePicker: false,
      rangeChoiceDisable: true,
    },
  });
  
  inputify({
    $container: $category,
    name: "range!",
    type: "date",
    value: "2000-10-10#23",
    date: {
      precision: "date",
      rangePicker: true,
      rangeChoiceDisable: true,
    },
  });

  inputify({
    $container: $category,
    name: "year!",
    type: "date",
    value: "2020#2050",
    date: {
      precision: "year",
      rangePicker: true,
      precisionChoiceDisable: true,
    },
  });

  inputify({
    $container: $category,
    name: "dateRange",
    type: "dateRange",
    value: "2000-10-10#12-23",
  });

  inputify({
    $container: $category,
    name: "dateTimeRange",
    type: "dateTimeRange",
    value: "2000-10-10_12:12#23_13:12",
  });

  inputify({
    $container: $category,
    name: "time",
    type: "time",
    value: "15:16",
  });

  inputify({
    $container: $category,
    name: "array of dates/ranges in reduced format",
    type: "dates",
  });

};
