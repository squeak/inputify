
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "object",
    type: "object",
    // value: 19,
  });

  inputify({
    $container: $category,
    name: "array",
    type: "array",
    object: {
      model: {
        type: "radio",
        choices: [ "a", "list", "of", "strings", ],
      },
    },
    value: [ "a", "list", "of", "strings", ],
  });

  inputify({
    $container: $category,
    name: "array of sliders",
    type: "array",
    object: {
      model: {
        type: "slider",
      },
    },
  });

  inputify({
    $container: $category,
    name: "object with structure",
    type: "object",
    object: {
      shape: "object",
      structure: [
        {
          name: "key1",
          type: "color",
        },
        {
          name: "key2",
          type: "slider",
        },
      ],
    },
    value: {
      key1: "val1",
      key2: "val2",
    },
  });

};
