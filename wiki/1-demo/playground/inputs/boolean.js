
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "boolean",
    type: "boolean",
    value: true,
  });

};
