
module.exports = function ($category) {

  inputify({
    $container: $category,
    type: "slider",
    name: "slider",
    value: 0.5,
    // color: "red",
  });

  inputify({
    $container: $category,
    type: "slider",
    name: "slider",
    value: 0.5,
    color: "red",
    colorContrast: "green",
    hoverColor: "purple",
    hoverColorContrast: "white",
  });

  inputify({
    $container: $category,
    type: "range",
    name: "range",
    value: [0.3, 0.9],
  });

};
