
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "country",
    type: "country",
  });

  inputify({
    $container: $category,
    name: "fonts",
    type: "fonts",
  });

  inputify({
    $container: $category,
    name: "icon",
    type: "icon",
    value: "rocket",
  });

  inputify({
    $container: $category,
    name: "icons",
    type: "icons",
    value: ["rocket"],
  });

};
