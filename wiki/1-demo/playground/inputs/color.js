
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "color",
    type: "color",
    value: "rgba(0, 255, 100, 0.8)",
    spectrum: {
      // showChoicesPalette: true,
    },
  });
  inputify({
    $container: $category,
    name: "colorRandomizable",
    type: "color",
    value: "rgba(100, 255, 10, 0.8)",
    spectrum: {
      showRandomButton: true,
    },
  });
  inputify({
    $container: $category,
    name: "colorRandomString",
    type: "color",
    value: "random",
    spectrum: {
      showRandomButton: "calculateLater",
    },
  });

  inputify({
    $container: $category,
    name: "palette with more options",
    type: "color",
    value: "#666666",
    spectrum: {
      showChoicesPalette: true,
    },
  });

};
