
module.exports = function ($category) {

  inputify({
    $container: $category,
    type: "simple",
    name: "simple",
    value: "A1",
    color: {
      primary: "red",
      hover: "blue",
    },
    choices: ["A1", "B2", "dedede", "ddd"]
  });

  inputify({
    $container: $category,
    type: "multi",
    name: "multi",
    color: {
      hover: "blue",
    },
    value: ["aadedee"],
    valueInProcess: function (value) {
      return ")"+ value
    },
    choices: function () {
      return [1,2,3,4]
    },
  });
  inputify({
    $container: $category,
    type: "multi",
    name: "multi",
    value: ["aadedee"],
    // valueInProcess: function (value) {
    //   return ")"+ value
    // },
    valueOutProcess: function (value) {
      if (value.match("@")) return "("+ value +")";
    },
    choices: function () {
      return [1,2,3,4]
    },
  });

  inputify({
    $container: $category,
    type: "multi",
    name: "multi",
    value: [{ key: "this is the val of key", val: "value"}, {a:11}],
    suggest: {
      labelToValue: function (label) {
        return _.chain(label.split(/\s*,\s*/)).map(function (d, i) {
          return d.split(/\s*:\s*/);
        }).object().value();
      },
      valueToLabel: function (value) {
        if (!_.isObject(value)) throw "Value should be an object"
        else return JSON.stringify(value);
      },
    },

    choices: function () {
      return [{a: "B2"}, { yo: "test", }]
    },
  });

  inputify({
    $container: $category,
    type: "select",
    name: "select",
    value: "A1",
    choices: function () {
      return ["A1", "B2", "dedede", "ddd"]
    },
  });

  inputify({
    $container: $category,
    type: "suggest",
    name: "suggest",
    choices: [
      {
        _isChoice: true,
        value: "AA",
        label: "yoa",
      },
      {
        _isChoice: true,
        value: "BB",
        label: "yob",
      },
      {
        _isChoice: true,
        value: "CC",
        label: "yoc",
      },
    ],
  });

};
