var uify = require("uify");
require("uify/plugin/crop");

module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "file",
    type: "file",
    file: {
      preview: "couch",
      process: [
        function (currentBlob, callback) {
          uify.crop({
            image: currentBlob,
            callback: callback,
          });
        },
      ]
    },
  });

  inputify({
    $container: $category,
    name: "files",
    type: "files",
  });

};
