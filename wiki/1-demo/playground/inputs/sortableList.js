
module.exports = function ($category) {

  inputify({
    $container: $category,
    name: "sortableList",
    type: "sortableList",
    value: [ "aa", "bb", "cde", "az", ],
  });

  inputify({
    $container: $category,
    name: "sortableList with handle",
    type: "sortableList",
    sortableList: {
      handle: ".handle",
    },
    value: [ "aa", "bb", "cde", "az", ],
  });

};
