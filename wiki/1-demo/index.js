
module.exports = {
  type: "demo",
  title: "Demo",
  demo: "playground",
  demos: [
    {
      name: "make",
      title: "Make an input",
    },
    {
      name: "inputs",
      title: "List of all available inputs",
    },
  ],
};
