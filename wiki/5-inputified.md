# The inputified object
(the one returned by `inputify()`)

🛈 This page is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈

```javascript
inputified = <{

  isInputified: true,
  name: <string>,
  deepKey: <string> « if the inputify is a nested one, in another this will be the chain of all parents name/keys, including this inputified »,
  $inputContainer: <yquerjObject>,
  $input: <yquerjObject>,
  $label: <yquerjObject>,
  options: <inputify·options> « full list of options, from type and passed »,

  //
  //                              METHODS: all those methods' context (=this) is inputified

  // GET/SET/MAKE
  get: <function(?opts<getOptions>):<inputify·inputValue>>,
  set: <function(value, ?opts<getOptions>):<inputify·inputValue>>,
  remake: <function(callback <function(ø)> « executed when remake done »):<void>> «
    remake the input, this will recalculate the input according to changes in it's options,
    another use case, is to retest if an input should be displayed, reevaluating it's condition
    WARNING:
      be careful with this method, no guaranty that it'll work fine, should be heavily tested,
      if type is modified, this type's options will be re-gotten, but the previous options will still be stored, since this stored option have priority over the others, there could be some issues with some type options not properly applied, or combined with some other that don't work together
  »,

  // DISPLAY
  show: <function(ø)> « set input display visible »,
  hide: <function(ø)> « set input display hidden »,
  // NOTE: to show/hide/toggle label and others, you can just call for example: inputified.$label.show()

  // COLOR STYLING
  setColor: <function(
    !colorDefiner <"base"|"baseContrast"|"primary"|"primaryContrast"|"hover"|"hoverContrast">,
    !value <colorString|"">,
  )> « sets input coloring »,

  // FOR INTERNAL USAGE
  makeIsAsync: <boolean>@default=false « if true, make will be passed a callback function that it should run »,
  make: <function(?callback<function(ø))> «
    generates the input
    callback is defined and should be ran if makeIsAsync is set to true
  »,
  changed: <function(ø)> « function executed when an input is changed, if inputified.options.changed is defined, it will execute it »,
  getSpecificOptions: <function(
    !key <string> « the key where are stored options in inputified.options object »,
    ?customProcessing <
      |function(specificOptions):specificOptions « any custom processing you want to apply to get options, this function must return the final options »,
      |object « this object will be used as default options for those specific options »
    >
    ?mandatoryOptions <object> « list of mandatory key/values to verify validity »,
  )> « get options specific to an input type »,
  initializeColor: <function(
    ?options <{
      // TYPE: colorNamer = "base"|"baseContrast"|"primary"|"primaryContrast"|"hover"|"hoverContrast" \\
      ?[colorNamer]:    <{ [any css property]: <string[]> «
        list of selector, elements matching those selectors will have the previous css properties applied
        string can be any kind of selector that jquery supports, with the following customizations:
        if "&": selects the input
        if "&&": selects the input container
        if starts with "&&": find the matching selector in the input container
        else: find the matching selector in the input
      » }>,
      ?selectedColor: <colorNamer> « color to go back to when unhovering if this element was selected »,
      ?unselectedColor: <colorNamer> « color to go back to when unhovering if this element was not selected »,
    }> « NOTE: if initializeColor has already been ran, you can omit passing options »
  )> « set input color and contrast »,
  interpretChoices: <function(
    callback <function(
      choices <inputify·choiceObject[]>
    )>
  )> « process the list of choices for usage in suggest-like and radioTickbox inputs »,
  ?systemValueOutVerify: <function(processedValue)> « used internally on some inputs to make sure the value is valid before processing it out »,
  ?systemValueInVerify:  <function(value)> « used internally on some inputs to make sure the value is valid before processing it in »,
  ?valueSetVerify: <function(processedValue)> « used internally on some inputs to make sure the value is valid before setting it »,

  //
  //                              STORAGE

  storage: <function(
    deepKey <string> « the key in storage on which you want to perform an action, if undefined, will return the whole storage »,
    customAction <"setValue"|"hasValue"|"removeValue"|"getValueOrResult"> « see the corresponding $$ methods with the same names, they will be called with the storage as object and the deepKey you passed here as deepKey »,
    ... any additional arguments that should be passed to the called method
  ):<any>> «
    get options from input storage (see inputify·options.storage for details)
    for object/array/entry subentries, inputified.options.storage will contain by default (in addition to the things you passed):
      parentInputified: <inputified> « the parent inputified object »,
  »,
  storageRecursive: <function(
    deepKey <string> « the key in storage on which you want to perform an action, if undefined, will return the whole storage »,
    customAction <"setValue"|"hasValue"|"removeValue"|"getValueOrResult"> « see the corresponding $$ methods with the same names, they will be called with the storage as object and the deepKey you passed here as deepKey »,
    ... any additional arguments that should be passed to the called method
  ):<any>> «
    get options from input storageRecursive (see inputify·options.storageRecursive for details)
    for object/array/entry subentries, inputified.options.storageRecursive will contain by default (in addition to the things you passed):
      rootInputified: <inputified> « the top parent inputified object »,
  »,

  //
  //                              SPECIFIC TO SUGGEST-LIKE INPUTS
  // the following keys/methods are only available in suggest-like inputs

  refreshChoices: <function(?callback<function(ø)>)> « this just reload the list of choices for inputs that support it (for now only suggest inputs) »,

  //
  //                              SPECIFIC TO OBJECT-LIKE INPUTS
  // the following keys/methods are only available in object-like inputs

  // ADD REMOVE KEY
  addSubKey: <function(?subinputifyOptions <inputify·options>){@this=inputified}:<inputified>> « create a new key in this object-like input »,
  removeSubKey: <function(!key<string|number>){@this=inputified}:<void>> « remove a key in this object-like input »,

  // GET SUB INPUTIFIED
  getSubInputified: <function(?key<string|number|<string|number>[]>):<inputified|undefined|<inputified|undefined>[]>> « get the inputified of a child key of this object (you may ask multiple inputified at once) »,

  // CHILD EVENTS
  childEventsSubscriptions: <{
    [childName <string|number> « key of the child that fired the event »]: <{
      subscriber: <inputified> « the child that subscribe to this event firing »,
      event: <inputify·subscribableChildEvent>,
      action: <inputify·actionOrMethodKey>,
    }[]>,
  }> « list of subscriptions from children towards other children »,
  childEventFired: <function(
    !eventName <inputify·modifEvent> « the name of the event that was fired »,
    !childName <string> « key of the child that fired this event »,
  )> « this is the method that a child fires on it's parent when an event happen, so the parent is aware of it and will transmit it eventually to other inputs that subscribed to it »,

  // SUBSCRIPTIONS
  subscriptions: <{
    changed: <inputified.subscribe·action[]>,
    setCallback: <inputified.subscribe·action[]>,
    modified: <inputified.subscribe·action[]>,
  }> « list of callbacks to execute on events on this input »,
  subscribe: <function(
    eventName <"changed"|"setCallback"|"modified"|"removed"> « the event you want to subscribe to »,
    action <function(ø){@this=inputified}> « what to execute when this event is triggered »,
  )> « subscribe to an event on this input »,

  //
  //                              SPECIFIC TO CHILDREN INPUTS OF OBJECT-LIKE INPUTS

  // GET SIBLING INPUT
  getSibling: <function(?siblingName<string|number>):<inputified|undefined>> « a method that returns an input with the given name that is a child of the same parent »,

  // GET SIBLING INPUT VALUE
  getSiblingValue: <function(
    ?siblingKey <string>
    ?logErrorIfSiblingNotFound <boolean>@default=false « if true, will log an error if sibling not found »,
  ):<any>> «
    get the value of a sibling input
    if can't find sibling, will simply return nothing, same as if sibling has no value
  »,

  // DO ON SIBLINGS INPUTS
  doOnSiblings: <function(
    !actionName <string> « the name of the action to execute on siblings (should be a valid inputified method name) »,
    ?nameOfSiblings <string|string[]> «
      list of names/keys of the siblings to execute the action on
      if not defined will do it on all (except this one ;)
    »
  ):<void>> « execute an action on some or all sibling inputs »,

  //
  //                              SPECIFIC TO ENTRY INPUTS

  // OPEN EDIT DIALOG
  entry_openEditDialog: <function(ø)> « open edit dialog for this entry »,

  // EDIT DIALOG OBJECT
  entry_editDialog: {
    dialog: <any>@default=<uify.dialog·return> «
      the type of this will depend on your custom dialog method, pass here what is useful to be able to manipulate the dialog easily
      this key is not necessary for inputified internal functioning
    »,
    destroy: <function(ø)> « close the edit dialog if it's open »,
  },

  // EDIT DIALOG INPUT
  entry_dialogInputified: <inputified> « if an edit dialog is currently open, this refers to the input it contains »,

  // REVERSE CHANGES MADE BY EDIT DIALOG
  entry_reverseEditDialogChanges: <function(ø)> « reverse modifications made by the currently open edit dialog »,
  // SAVE CHANGES MADE BY EDIT DIALOG
  entry_writeDialogInputValueToOriginInput: <function(ø)> « save changes in edit dialog to main entry »,

  // CLOSE EDIT DIALOG
  entry_saveAndCloseEditDialog: <function(ø)> « save changes to main entry and close edit dialog »,
  entry_discardAndCloseEditDialog: <function(ø)> « discard changes to main entry and close edit dialog »,

  //                              ¬
  //

}>
```
