# All options

🛈 This page is written using squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈

```javascript
inputify·options = <{

  //
  //                              GENERAL OPTIONS

  // GENERAL
  !$container <yquerjProcessable>,
  !type|inputifyType: <keyof inputs>,
  !name|key: <string> « input name (= key) »,

  // DETAILS
  ?help: <string|string[]|function(ø){@this=inputified}:<string|string[]>> « a help message to display when label is hovered »,
  ?helpPlacement: <"left"|"right"|"top"|"bottom"... see tippy.js placement options https://atomiks.github.io/tippyjs/#placement> « where to display tooltip, by default it is on the left if width screen, else bottom »,
  ?mandatory: <boolean|function(ø){@this=inputified}:<boolean>> « if true or returns true, an error will be thrown if no value has been chosen »,
  ?locked: <boolean|function(ø){@this=inputified}:<boolean>> « if true or returns true, this input will not be editable »,
  ?condition: <boolean|function(ø){@this=inputified}:<boolean>> « if this input should be displayed or not »,
  ?conditionIsImperative: <boolean> « even if input has a value, will display it only if condition is fulfilled (by default, if condition is not validated, input is still displayed if defined) »,
  ?isAdvanced: <boolean>@default=false « if true, the input will be treated according to the rule set in in advancedInputsDisplay »,
  ?hideAdvancedEvenIfDefined: <boolean>@default=false « if this is true and advanced input is defined, it will still be hidden by default »,
  ?advancedInputDisplay: <"hidden"|"decoration"|"normal">@default="hidden" « how to display advanced inputs »,
  ?protectionBanner: <string|function(ø){@this=inputified}:<string>> « display a banner with the given text to prevent users from editing this input too easily »,
  ?objectResult: <boolean>@default=false « if true, get, set, and changed methods will return value in the shape: { name: <string>, value: <any>, } (useful if you want to get inputify name with its value) »,
  ?monospace: <boolean>@default=false « make the input use monospace font »,
  ?focus: <boolean>@default=false « focus an input at its creation (not supported by all input types) »,

  // LABEL
  ?label: <string> « the label displayed on the left of the input, if label is not defined, will use inputify·options.name »,
  ?labelLayout: <"inline"|"stacked"|"hidden"|"intertitle">@default="inline" « how to display the label »,
  ?labelAdditionalButtons: <uify.button·options[]> « a list of custom buttons to add to label »,

  // VALUE
  ?value: <any(value type depends on the input type)|function(ø){{@this=inputified}}:<any>> « the initially set value »,
  ?defaultValue: <any(value type depends on the input type)|function(ø){@this=inputified}:<any>> « if value is undefined, will use this value to initialize input »,
  ?prepareValue: <any(value type depends on the input type)|function(ø){@this=inputified}:<any>> « if value and defaultValue are undefined, will use this value to initialize input, but if this value is not modified, it will not be returned when saving »,
  ?defaultOnlyForNew: <boolean> «
    if true, will not set defaultValue if the entry this input belongs to is not new
    the newness of the entry is determined by checking a custom option: storageRecursive.isNewEntry, so you must specify this value as well for defaultOnlyForNew to be of some use
  »

  // VALUE VERIFICATION AND PROCESSING
  ?valueInVerify: <function(value){@this=inputified}:valueVerified> « if defined, this function must return something, use this to discard a value that wouldn't be appropriate and prevent setting it, or to filter it and make it appropriate, for real value modification, use valueInProcess »,
  ?valueOutVerify: <function(processedValue){@this=inputified}:processedValueVerified> «
    - if defined, this function must return something, use this to discard a value that wouldn't be appropriate and prevent getting it (get will return undefined), or to filter it and make it appropriate (for real value modification, use valueOutProcess)
    - to display that the value is invalid, throw an error here
    - suggest-like inputs don't support this option, use suggest.labelToValue instead
  »,
  ?valueInProcess: <function(value){@this=inputified}:processedValue> « the given value will be processed with this function »,
  ?valueOutProcess: <function(processedValue){@this=inputified}:value> « the chosen value will be processed with this function »,
  ?unvalidatedThrowError: <boolean>@default=false « if set to true and there is an error validating value while processing in or out, an error will be thrown »,
  ?unvalidatedAlertError: <boolean>@default=true « if set to false will not alert when error validating value in or out »,
  ?unvalidatedOnChangedSilent: <boolean>@default=true « if true, will not alert or throw error if not validated on changed event »,

  // EVENTS
  ?changed: <function(inputify·inputValue){@this=inputified}:void> « executed when value is modified by the user »,
  ?setCallback: <function(inputify·inputValue){@this=inputified}> « executed when value has been set programatically »,
  ?modified: <function(inputify·inputValue){@this=inputified}:void> «
    executed when value is modified by the user or has been set programatically
    (modified is executed before changed or setCallback)
  »,
  ?created: <function(ø){@this=inputified}:void> « executed when the input has been created (only on first creation, not on remakes) »,

  // STYLING AND COLORING
  ?containerClass: <string> « this class will be added to the inputify container dom element »,
  ?inputClass: <string> « this class will be added to the inputify input dom element »,
  ?color: <{
    ?base: <colorString> « base color of the backgrounds and neutral elements in the input »,
    ?baseContrast: <colorString> « contrast color for base »,
    ?primary: <colorString> « this will color the input »,
    ?primaryContrast: <colorString> « this will be the color contrast for primary, for example if primary colors an element background, primaryContrast would be applied to this element text »,
    ?hover: <colorString|""> « if primary is set, but not hover, will remove any hovering effect, to use the default app hovering color, pass "" »,
    ?hoverContrast: <colorString|""> « just like primaryContrast but for hovered elements »,
  }>,
  // NOTE: coloring is done via javascript, because it's too complex to make css rules to apply to all different types of elements, colors have to be applied to background, to foreground, to border... in very various ways depending on the input

  // METHODS
  ?inputMethods: <<function(){@this=inputified}>[]> «
    a list of methods to attach to the inputied object,
    this can be useful to create some methods that will be attached to it on its creation, before it has its first set, get, modified... events/actions
    ⚠️ if you set a method with the same name than an existing one in this inputified, your method will be ignored
    ⚠️ you can override existing method by setting the next "allowOverridingExistingMethods" option to true but be careful with that as it can break inputify
  »
  ?allowOverridingExistingMethods: <boolean>@default=false « allow to override system methods by your custom ones, see previous option "inputMethods" for details »,

  // CUSTOM CSS RULES FOR THE INPUT
  ?cssInput: <object> « any custom css to apply to the input »,
  ?cssContainer: <object> « any custom css to apply to the input container »,
  ?cssLabel: <object> « any custom css to apply to the input label »,

  //
  //                              OPTIONS USED ONLY IN SOME SPECIFIC INPUT TYPES

  // for suggest, radiotickbox inputs
  ?choices: <
    | any[] // if value is not a string list, use valueInProcess to make sure label is a string to the input
    | inputify·choiceObject[]
    | function(
        callback <function(
          choices <string[]|inputify·choiceObject[]>
        )>
      )
    > «
    for some inputs, the list of item that are suggested or chosen from
    if it's a function, it can be asynchronous an should execute a callback with the choices list passed as argument
  »,

  // for input using the default html input tag
  ?input: {
    ?type: <string> « input type: e.g. password, number, color... »,
    ?placeholder: <string> « placeholder value before the user types in anything »,
  },

  // for color inputs
  ?spectrum: {
    ?showChoicesPalette: <boolean> « if true, will display a palette of colors to choose and more button to choose any custom color »,
    ?showRandomButton: <boolean|"randomTransparency"|"calculateLater"> «
      if true, will display a button allowing to let the input randomly choose a color (with the chosen transparency)
      if "randomTransparency", same as true, but with random transparency
      if "calculateLater", will display a button that if clicked set the value of the input to "random"
    »
    // ... see all spectrum options at: https://bgrins.github.io/spectrum/#methods-option
    // NOTE: returned color value will be a hex string (e.g. #FF00FF) unless there is transparency, it will be returned as rgba string (e.g. rgba(255, 0, 255, 0.6))
  },

  // for date inputs
  ?date: {
    ?precision: <"year"|"month"|"date"|"time">@default="date" « precision of the date to choose »,
    ?precisionChoiceDisable: <boolean>@default=false « disable the possibility for the user to choose the precision »,
    ?rangePicker: <boolean>@default=false « initial state of the date picker, date or range »,
    ?rangeChoiceDisable: <boolean>@default=false « disable the possibility for the user to choose to input range or date »,
    ?onDateChange: <function(<{ start: <momentObject>, end: <momentObject> }>){@this=inputified}> « a function to execute when the date has been modified by the user »,
    ?onRangeSwitch: <function(rangeOrNotRange<boolean>){@this=inputified}> « a function to execute when the range switcher have been triggered »,
    ?onPrecisionPick: <function(pickedPrecision<"year"|"month"|"date"|"time">){@this=inputified}> « a function to execute when the precision picker has been triggered »,
  },

  // for time inputs
  ?time: {
    ?parts: <<"hours"|"minutes"|"seconds">[]>@default=["hours", "minutes"] « which time parts can be set »,
  },

  // for slider inputs
  ?slider: {
    ?orientation: <"horizontal"|"vertical">@default="horizontal" « orientation of the input »,
    ?min: <number> « minimum slider value »,
    ?max: <number> « maximum slider value »,
    ?step: <number> « steps between values »,
    ?showNumberOnHandle: <boolean>@default=false « if true, will put the tooltip displaying chosen number directly on the slider handle »
    // ... see all nouislider options at: https://refreshless.com/nouislider/examples/
  },

  // for sortable inputs
  ?sortableList: {
    ?handle: <".handle"|string> «
      if not defined the whole entry can be grabbed,
      if you want to use the default handle, pass ".handle",
      if you want to use a custom handle, pass any other selector,
    »,
    ?display: <string|function($li, entry)> «
      if this is specified, it will be used to map the list for custom displaying,
      so this can be a <function> or a <key> to pluck list with,
      if display is undefined, the list will be displayed raw
    »,
    ?liClass: <string|function(listEntry)> « to specify a class for the li elements »,
    // ... see all sortablejs options at : https://sortablejs.github.io/Sortable/
  },

  // for all suggest inputs
  ?suggest: {
    ?addItems: <boolean>@default=true « whether a user can add items »,
    ?maxItemCount: <number>@default=-1 « how many items the user can input, if -1 or 0 == no limit »,
    ?valueToLabel: <function(value){@this=inputified}:label<string>> « how to display a value label »,
    ?labelToValue: <function(label){@this=inputified}:value<any>> «
      this function allow you to define custom ways to interpret the inputed value to convert it into a choice label
      throw an error in this function to prevent the choice from being created (in other words to discard the currently inputed value)
      suggest-like inputs don't support valueOutVerify, use this option instead
    »,
    ?invalidChoiceError: <"alert"|"log"|null>@default="log" «
      what to do it asked to set an invalid choice
      log the error, alert the error, or do nothing
    »,
    ?noResultsText: <htmlString>@default="No results found" « message to display when no matching result can be found (only displayed if addItems is set to false) »,
    ?tooManyItemsText: <htmlString>@default="You have chosen the maximum number of items" « message displayed when max items count has been reached »,
    ?errorResultText: <function(e<error>):<string>>@default=function(e){return '<span><b>This value is invalid:</b><br><span class="error-text">'+ e +'</span></span>'; } « message displayed if the item the user tries to add has an error »,
  },

  // for radio and tickbox inputs
  ?radioTickbox: {
    ?orientation: <"horizontal"|"vertical">@default="horizontal",
    ?valueToLabel: <function(value){@this=inputified}:label<string>> « how to display a value label »,
    ?customItemDisplay: <function(item<{ $element: <yquerjObject>, name: <string>, label: <string> }>)> « if this is defined, on top of the previous valueToLabel option, you can create your own customized display for the item's text/content (icons and sortOrder display are still automatically displayed) »,
    ?noText: <boolean>@default=false « if true, will not display choices text »,
    ?iconChecked: <icomoonString|false>@default="checkmark3"|"radio-checked" « if false, will display no checked icon »,
    ?iconUnchecked: <icomoonString|false>@default=undefined « if undefined/false, will display no unchecked icon »,
    ?sortBySelectOrder: <boolean>@default=false « if true, will order tickbox selected value in the order they have been selected »,
    øtype: <"tickbox"|"radio"> « this is defined internally in inputs config »,
  },

  // for boolean inputs
  ?boolean: {
    ?orientation: <"horizontal"|"vertical">@default="horizontal",
    ?iconOn: <icomoonIconString>@default:"toggle-on",
    ?iconOff: <icomoonIconString>@default:"toggle-off",
  },

  // for number inputs
  ?number: {
    ?orientation: <"horizontal"|"vertical">@default="horizontal",
    ?min: <number>@default=0 « set to null to remove any minimum limit »,
    ?max: <number>@default=null « set to null to remove any maximum limit »,
    ?step: <number>@default=1 « customize how much will be added/subtracted to the value when +- buttons are clicked »,
  },

  // for file inputs
  ?file: {
    ?title: <boolean>@default=false «
      if true: display an input to choose the title of the file
      if false: no input displayed, no title will be set
    »,
    ?additionalInputs: <inputify.options[]> « a list of additional input to add more data associated to this file »,
    ?fileTypes: <<"image"|"video"|"sound"|string>[]> «
      list of allowed type of files, or extensions
      you can set here any extension (like "jpg", "eps") or preset file types
      available preset file types are:
      "image" which unfolds into: jpg, jpeg, png, svg
      "video" which unfolds into: mp4, avi, mkv, flv, mov, m4v
      "audio" which unfolds into: mp3, m4a, ogg, wav, aif, aiff, flac, wma, mov
      you can mix up file types and extensions, for example: ["jpg", "png", "video", "audio"]
    »,
    ?process: <inputify·fileProcessFunc[]> «
      a list of functions to process the selected file before uploading,
      they can be asynchronous and must all call they're callback function when they're done
    »,
    ?showFileInfos: <boolean>@default=true « show some infos about the selected file »,
    ?preview: <null|"couch"|function(
      entry,
      previewFile <function(inputified, file<string|blob> « if string, will display this string, if blob and an image will display a tumbnail preview »)>,
    ){@this:inputified}> « define here how to get the file to preview from your entry »,
    ?attachmentSave: <null|"couch"|inputify·fileSaveDeleteFunction> « the return of this function will be the return of inputified.get/inputified·changed methods »,
    ?attachmentDelete: <null|"couch"|inputify·fileSaveDeleteFunction>,
    ?couchAttachment: <boolean> « setting this to true this will bypass anything you have set to "preview", "attachmentSave" and "attachmentDelete" options and set them to "couch" »,
    ?doNotSubscribeToParentChangedEvent: <boolean>@default=false « if true, will not try to identify if an attachment should be removed based on changes in parent inputs (like removal of an object containing file inputs) »,
    // see inputified·fileValue for information on the type of data that is returned when running inputified.get on a file input
  },

  // for "entry", "array", "object" inputs (and also "date", "files", "colors" and probably some others)
  ?object: <
    |inputify·objectOptions
    |function( callback<function(inputify·objectOptions)> ):<void>
  > «
    pass either the object options
    or a function that has for unique argument a callback with object options as argument
  »,

  // for "entry" input
  ?entry: <{
    ?saveToParentOnChange: <boolean>@default=false « if true, changes in subentry will be automatically applied to parent, without needing to save them »,
    ?useCustomDialog: <function(<{
      contentMaker: <function($container):<void>> « execute this callback when you're ready to display the inputs, pass the $container where inputs should be created and inputify will do the rest »,
      save: <function(ø):<void>> « execute this callback when you want to save the value to the parent entry (if you set saveToParentOnChange to true, you never need to run this, it's done automatically on every change) »,
      discard: <function(ø):<void>> « if you set saveToParentOnChange to true, use this to cancel modifications that have been saved to the parent entry »,
    }>){@this=inputified}:<{
      ?dialog: <any> «
        the type of this will depend on your custom dialog method, pass here what is useful to be able to manipulate the dialog easily
        this key is not necessary for inputified internal functioning
      »,
      !destroy: <function(ø)> « destroy the dialog if it's opened »,
    }>> « customize the way a subentry is edited, create your own personalized dialog »,
    ?customizedOptions: <inputify·options> «
      if this is set, the entry input opened in the dialog will use these custom settings,
      on the contrary, when this is not set, the dialog input is an object and gets options.object passed by its maker
    »,
  }>,

  // for geo inputs
  ?geo: <{
    ?shape: <<"point"|"polyline"|"polygon"|"circle">[]>@default=["point", "polyline", "polygon", "circle"] « the types of shape that this input allow to create »,
  }>,

  // for subinputs (means they are inside an object, entry or array)
  ?siblingsSubscriptions: <siblingSubscription[]|function(ø){@this=inputified}:siblingSubscription[]> « subscribe to some events in sibling inputs »

  //
  //                              ADDITIONAL INFOS TO STORE IN INPUTIFIED

  ?storage: <object> «
    this is just to pass any values you want to attach them to the inputified object, it can be useful to set some context that you then could use to determine the value, defaultValue of an input depending on the context
    (postify library make use of this for example to store general information of the edited entry in each inputs)
    to access to an input "storage" or "storageRecursive" use the inputified.storage and inputified.recursiveStorage methods described below
  »,
  ?storageRecursive: <object> «
    same as the previous one, but this will also be passed to children inputified (if necessary, merged with the one set in children)
    additionally, if storageRecursive contain a key named "options", those options will be passed to all children inputifies recursively (having priority over system options of an input, but not over options specifically passed for the creation of this new inputified)
  »,

  //
  //                              OPTIONS USED INTERNALLY
  // NOTE: THOSE OPTIONS ARE SET FROM WITHIN INPUTIFY, CHANGE THEM ONLY IF YOU REALLY KNOW WHAT YOU ARE DOING

  // GENERAL
  ?tag: <string>, « tag used in the dom for this input »
  ?description: <string>, « description of the input »,
  ?specificOpts: <string[]> «
    the list of specific options that this type of input uses
    (the values available to put in this list are the keys in the previous section "OPTIONS USED ONLY IN SOME SPECIFIC INPUT TYPES")
  »,
  ?ignoreInDeepKeyChain: <boolean> « if true, it this is a subinput, it will be ignored to form the deepKey (this is used internally for entry inputs) »,
  ?methods: <{
    ?make: <function(ø)> « defines how to generate the input »,
    ?get: <function(ø)> « defines how to get a value from the input »,
    ?set: <function(value, opts)> «
      defines how to set a value to the input
      you may pass some custom options (supported by this input type) for this run of set with the opts argument for example, if you pass { isPartialSet: true } to an object input, keys present in what you set will be added to the current value, the value wont be totally replaced
    »,
    ?uiGet: <function()> « get value from the input UI »,
    ?uiGetRaw: <function()> « get value from the input but should return not a processedValue but directly the real value that doesn't need to be processed back (this is useful if the initial value is stored and doesn't need to be processed back, see radioTickbox and suggest inputs types for examples) »,
    ?uiSet: <function(value):<void>> « set the value to the UI »,
  }>,

  // PROCESSING
  ¡systemValueInProcess: <function(value){@this=inputified}:processedValue> « is executed on value just before valueInProcess »,
  ¡systemValueOutProcess: <function(processedValue){@this=inputified}:value> « is executed on value just before valueOutProcess »,
  ¡systemValueInVerify: <see valueInVerify>,
  ¡systemValueOutVerify: <see valueOutVerify>,
  // NOTE: systemValue(in/out)Process work the same as value(in/out)Process but are meant to be used internally for creating custom inputs without removing the user the possibility to add it's own processing function, same for systemValue(in/out)Verify

  //                              ¬
  //

}>
```

## inputValue
```javascript
inputify·inputValue = <
  |<any> « if inputify·options.objectResult == false »,
  |<{ name: <string>, value: <any>, }> « if inputify·options.objectResult == true »,
>
```

## subOptions
```javascript
inputify·subOptions = <{
  ... same as inputify·options but $container is optional
}>
inputify·subOptionsWithoutKey = <{
  ... same as inputify·subOptions but name will be ignored, it is not necessary
}>
```

## objectOptions
```javascript
inputify·objectOptions = <{
  ?shape: <"object"|"array">@default="object" « is this object an "array" or a js "object" »,
  ?structure: <inputify·subOptions[]> « defines the list of inputs that lets you edit the object/array (if shape == "array" structure type is <inputify·subOptionsWithoutKey[]>) »,
  ?model: <inputify·subOptionsWithoutKey>@default={type: "normal"} « if no structure defined, every children value will be an input defined with this provided config, if structure is defined, this input config will be used for new custom keys »,
  ?allowNewKeyCreation: <boolean>@default=(inputify·options.object.model ? true : false),
  ?newKeyChoices: <string[]> « a list of keys to suggest »,
  ?newKeyConstraint: <function(newKey<string>)> « this function should throw an error if the inputed key should be refused »,
  ?sortDisplay: <see options.sortableList.display> « the custom processing to display entries when the user clicked on the reorder entries button »,
  ?passParentOptionsToChildrenModel: <string[]> « if you set something here, the object options with the keys specified in this array will passed to the children model »,
  ?keepUndefinedEntries: <boolean> « if true, will not compact result to strip undefined values »,
  ?help: <
    |string
    |function($helpMessage<yquerjObject>){@this=<inputified>}:<string>
  > «
    help message displayed on top of the entry window
    ⚠️ this is option is only used for entry inputs
    if you use a function, you can either return with it the html content you want to display, or manipulate the $helpMessage yourself
  »,
  // you may pass the following options to inputified.set method for object inputs:
  // inputified.set·opts = {
  //   ?isPartialSet: <boolean> « if true will merge new entries with previous one replacing only keys defined in newly passed value »,
  // }
}>
```

## choices
```javascript
inputify·choiceObject = <{
  !_isChoice: true,
  !value: <any>,
  ?label: <string>,
  ?search: <string>,
}> «
  "_isChoice" key should be defined, inputify will use passed object directly as choiceObject to create system list of choices
  if "label" is not defined, it will be generated internally (by interpretChoices/makeChoice method)
  "search" is an optional additional string to search into when typing (for suggest inputs)
»
```

## fileProcessFunc
```javascript
inputify·fileProcessFunc = <function(
  currentBlob <Blob> « the file to modify »,
  next <function(modifiedBlob<Blob>) « execute this callback somewhere in your code (async is fine), and pass the modified version of the file to this callback »,
)>
```

## getOptions
```javascript
getOptions = <{ ?objectResult: <boolean> « if set, will use this value instead of the default one in options », }>
```

## subscribableChildEvent
```javascript
inputify·subscribableChildEvent: <"modified"|"changed"|"setCallback">,
```

## actionOrMethodKey
```javascript
inputify·actionOrMethodKey: <
  |<"remake"|"refreshChoices"|"show"|"hide"|"toggle">
  |function(inputThatFiredTheEvent<inputified>){@this=<inputified> « the input that subscribed to the event firing »}
> «
  the action to execute when the event was fired
  if string will use the method named like this in inputified, it will also pass the value of the input that fired the event to the gotten method
»
```

## siblingSubscription
```javascript
siblingSubscription: <{
  !key: <string> « key of the sibling input you want to subscribe event to »,
  !event: <inputify·subscribableChildEvent> « the event you want to subsribe to »,
  !action: <inputify·actionOrMethodKey>,
}>
```

## file types: fileSaveDeleteFunction, fileObject, fileValue
```javascript
inputify·fileSaveDeleteFunction = function(
  fileObject<inputify·fileObject,
  entry <couchEntry> « the root entry (toppest parent of this inputified), if there is one »
)> «
  a custom function that should be executed when a file is saved/removed
  if nothing is set, nothing will happen
  if "couch" will save/remove the file to entry's "_attachments" section
»

inputify·fileObject = <{
  isFile: <true>,
  id: <string> « a unique uuid to identify this attachment »,
  title: <string> « the title/name of this file »,
  ?remove: <boolean> « this is true if the file should be removed »,
  ?blob: <File|Blob> « the blob of this file »,
}>

inputified·fileValue = <
  | inputified.options.attachmentSave·return « if inputified.options.attachmentSave is a custom function »
  | {
    attachmentId: <string>,
    title: <string>,
  } « if inputified.options.attachmentSave == "couch" »
  | inputify·fileObject « if inputified.options.attachmentSave is undefined »
> « this is what the return of inputified.get will look like on a file inputified »
```
