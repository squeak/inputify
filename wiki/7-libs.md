# Used libraries

Inputify exists thanks to the awesome tools provided by those great libraries:

[underscore](https://underscorejs.org/)
[squeak](https://squeak.eauchat.org/libs/squeak/)
[async](https://caolan.github.io/async/)
[yquerj](https://squeak.eauchat.org/libs/yquerj/) (an extended version of [jquery](https://jquery.com/))
[stylectrode](https://squeak.eauchat.org/libs/stylectrode/)
[keyboardify](https://squeak.eauchat.org/libs/keyboardify/)
[uify](https://squeak.eauchat.org/libs/uify/)
[tippy.js](https://atomiks.github.io/tippyjs/)
[spectrum](https://bgrins.github.io/spectrum/#methods-option)
[nouislider](https://refreshless.com/nouislider/examples/)
[sortablejs](https://sortablejs.github.io/Sortable/)
[leaflet](https://leafletjs.com/)
